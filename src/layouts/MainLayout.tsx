import React, { createContext, useEffect } from 'react';
import Sidebar from '@/common/components/SideBar';
import useTheme from '@/hooks/useTheme';
import useSidebar, { useMultipleSidebarHookProps } from '@/hooks/useSidebar';
import { messaging } from '@/common/utils/firebase';
import { onMessage } from 'firebase/messaging';
import { useAppDispatch } from '@/hooks';
import {
  addNotification,
  setUnreadNotification,
} from '@/store/slice/notificationSlice';
import { INotification } from '@/common/utils/interface';
import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import { NotificationStatus, NotificationType } from '@/common/utils/enums';
import { getAPI } from '@/common/utils/customAPI';

export const SidebarContext = createContext<useMultipleSidebarHookProps>({
  primarySidebar: {
    isShowSidebar: false,
    openSidebar: () => undefined,
    closeSidebar: () => undefined,
  },
  secondarySidebar: {
    isShowSidebar: false,
    openSidebar: () => undefined,
    closeSidebar: () => undefined,
  },
});

interface Props {
  children?: React.ReactNode;
}

const MainLayout: React.FC<Props> = ({ children }) => {
  const dispatch = useAppDispatch();
  useTheme();
  const primarySidebarHook = useSidebar();
  const secondarySidebarHook = useSidebar();
  const fetchCountUnreadNotifications = async () => {
    const res = await getAPI('/v1/notification/unread');
    if (res.code === 200) {
      dispatch(setUnreadNotification(res.data as number));
    }
  };

  useEffect(() => {
    fetchCountUnreadNotifications();
    if (messaging) {
      onMessage(messaging, (payload) => {
        const notification: INotification = {
          title: payload.notification?.title,
          body: payload.notification?.body,
          data: {
            senderImage: payload.data?.senderImage || DEFAULT_AVATAR_PATH,
            type:
              (payload.data?.type as NotificationType) ||
              NotificationType.DEFAULT,
            time: payload.data?.time || new Date().toISOString(),
            username: payload.data?.username || '',
            code: payload.data?.code,
            noti_id: payload.data?.noti_id || '',
          },
          status: NotificationStatus.UNREAD,
        };
        dispatch(addNotification(notification));
      });
    }
  }, []);

  useEffect(() => {
    const channel = new BroadcastChannel('notifications');
    const messageEvent = (event: MessageEvent) => {
      const payload = event.data;
      const notification: INotification = {
        title: payload.notification?.title,
        body: payload.notification?.body,
        data: {
          senderImage: payload.data?.senderImage || DEFAULT_AVATAR_PATH,
          type: payload.data?.type,
          time: payload.data?.time,
          username: payload.data?.username || '',
          code: payload.data?.code,
          noti_id: payload.data?.noti_id || '',
        },
        status: NotificationStatus.UNREAD,
      };
      dispatch(addNotification(notification));
    };
    channel.addEventListener('message', messageEvent);
    return () => {
      channel.removeEventListener('message', messageEvent);
    };
  }, []);

  return (
    <SidebarContext.Provider
      value={{
        primarySidebar: primarySidebarHook,
        secondarySidebar: secondarySidebarHook,
      }}
    >
      <div className="app-layout">
        <Sidebar />
        <div className="app-content">{children}</div>
      </div>
    </SidebarContext.Provider>
  );
};

export default MainLayout;
