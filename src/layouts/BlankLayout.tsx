import useTheme from '@/hooks/useTheme';
import React from 'react';

interface Props {
  children?: React.ReactNode;
}
const BlankLayout: React.FC<Props> = ({ children }) => {
  useTheme();

  return <>{children}</>;
};

export default BlankLayout;
