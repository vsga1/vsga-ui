import React from 'react';
import Image from 'next/image';
import Link from 'next/link';

interface Props {
  children?: React.ReactNode;
}

const Authenticate: React.FC<Props> = ({ children }) => {
  return (
    <>
      <div id="authenticate-layout" className="d-flex">
        <div className="content">
          <div className="logo">
            <Link href="/">
              <Image
                src="/static/images/logo.png"
                width="24"
                height="26"
                alt="logo"
                className="mr-2"
              />
              VStudy
            </Link>
          </div>
          <div className="auth-form">{children}</div>
        </div>
        <div className="banner align-items-center justify-content-center">
          <div className="image d-flex justify-content-center align-items-center">
            <img src="/static/images/auth_banner.png" alt="banner" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Authenticate;
