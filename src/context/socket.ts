import React from 'react';
import { io } from 'socket.io-client';

export const SocketContext = React.createContext(
  io(process.env.NEXT_PUBLIC_SOCKET_DOMAIN || '', {
    transports: ['websocket'],
  })
);
