import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../index';
import { TaskStatus, TaskType } from '@/common/utils/enums';

export interface Task {
  code: string | null;
  description: string;
  room_code: string;
  status: TaskStatus;
  type: TaskType;
  scheme?: string;
  users?: Array<{
    username: string;
    avatar_img_url: string;
  }>;
}

interface TaskSlice {
  data: Task[];
}

const initialState: TaskSlice = {
  data: [] as Task[],
};

export const taskSlice = createSlice({
  name: 'task',
  initialState,
  reducers: {
    initTask(state, action: PayloadAction<Task[]>) {
      state.data = action.payload;
    },
    addTask(state, action: PayloadAction<Task>) {
      const newTask = action.payload;
      state.data = [newTask, ...state.data];
    },
    doneTask(state, action: PayloadAction<string | null>) {
      if (action.payload !== null) {
        const index = state.data.findIndex(
          (task) => task.code === action.payload
        );
        state.data[index].status = TaskStatus.DONE;
      }
    },
    removeTask(state, action: PayloadAction<string | null>) {
      if (action.payload !== null) {
        const index = state.data.findIndex(
          (task) => task.code === action.payload
        );
        state.data.splice(index, 1);
      }
    },
    editTask(
      state,
      action: PayloadAction<{ code: string | null; description: string }>
    ) {
      if (action.payload.code !== null) {
        const index = state.data.findIndex(
          (task) => task.code === action.payload.code
        );
        state.data[index].description = action.payload.description;
      }
    },
    userDoneTask(
      state,
      action: PayloadAction<{
        scheme: string;
        username: string;
        avatar_img_url: string;
      }>
    ) {
      const index = state.data.findIndex(
        (t) => t.scheme === action.payload.scheme
      );
      if (index !== -1) {
        if (!state.data[index].users) {
          state.data[index].users = [
            {
              username: action.payload.username,
              avatar_img_url: action.payload.avatar_img_url,
            },
          ];
        } else {
          state.data[index].users?.push({
            username: action.payload.username,
            avatar_img_url: action.payload.avatar_img_url,
          });
        }
      }
    },
    editRoomTask(state, action: PayloadAction<Task>) {
      const index = state.data.findIndex(
        (t) => t.scheme === action.payload.scheme
      );
      if (index !== -1) {
        state.data[index].description = action.payload.description;
      }
    },
    removeRoomTask(state, action: PayloadAction<string>) {
      const index = state.data.findIndex((t) => t.scheme === action.payload);
      if (index !== -1) {
        state.data.splice(index, 1);
      }
    },
  },
});

export const {
  initTask,
  addTask,
  removeTask,
  doneTask,
  editTask,
  userDoneTask,
  editRoomTask,
  removeRoomTask,
} = taskSlice.actions;

export const selectAllTask = (state: RootState) => state.task.data;
export const selectUnremovedTasks = (state: RootState) =>
  state.task.data.filter((item) => item.status !== TaskStatus.DELETED);
export const selectRoomTasks = (state: RootState) => {
  const doneTasks = state.task.data.filter(
    (item) => item.type === TaskType.ROOM && item.status === TaskStatus.DONE
  );
  const undoneTasks = state.task.data.filter(
    (item) => item.type === TaskType.ROOM && item.status === TaskStatus.UNDONE
  );
  return [...undoneTasks, ...doneTasks];
};
export const selectPersonalTasks = (state: RootState) => {
  const doneTasks = state.task.data.filter(
    (item) => item.type === TaskType.PERSONAL && item.status === TaskStatus.DONE
  );
  const undoneTasks = state.task.data.filter(
    (item) =>
      item.type === TaskType.PERSONAL && item.status === TaskStatus.UNDONE
  );
  return [...undoneTasks, ...doneTasks];
};

export default taskSlice.reducer;
