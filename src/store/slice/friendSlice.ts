import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Info } from './userSlice';
import { RootState } from '..';

interface FriendState {
  friendList: Info[];
}

const initialState: FriendState = {
  friendList: [],
};

export const friendSlice = createSlice({
  name: 'friend',
  initialState,
  reducers: {
    setFriendList(state, action: PayloadAction<Info[]>) {
      state.friendList = action.payload;
    },
    removeFriend(state, action: PayloadAction<string>) {
      state.friendList = state.friendList.filter(
        (user) => user.username !== action.payload
      );
    },
  },
});

export const { setFriendList, removeFriend } = friendSlice.actions;
export const selectFriendList = (state: RootState) => state.friend.friendList;
export default friendSlice.reducer;
