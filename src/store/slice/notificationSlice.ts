import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { INotification } from '@/common/utils/interface';
import { RootState } from '..';
import { NotificationStatus } from '@/common/utils/enums';

interface PageInfo {
  next: number;
  limit: number;
  total: number;
}

interface NotificationState {
  data: INotification[];
  new: number;
  page: PageInfo;
}

const initialState: NotificationState = {
  data: [],
  new: 0,
  page: {
    next: 0,
    limit: 10,
    total: 0,
  },
};

export const notificationSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    setFirstOfListNotification(state, action: PayloadAction<INotification[]>) {
      state.data = [...action.payload];
    },
    addNotification(state, action: PayloadAction<INotification>) {
      state.data = [action.payload, ...state.data];
      state.new++;
    },
    setUnreadNotification(state, action: PayloadAction<number>) {
      if (action.payload > 0) {
        state.new = action.payload;
      }
    },
    decreaseUnreadNotification(state) {
      if (state.new > 0) {
        state.new--;
      }
    },
    addListNotification(state, action: PayloadAction<INotification[]>) {
      state.data = [...state.data, ...action.payload];
    },
    readAllNotifications(state) {
      state.new = 0;
      state.data.forEach((item) => {
        if (item.status !== NotificationStatus.READ) {
          item.status = NotificationStatus.READ;
        }
      });
    },
    updateNotificationPageInfo(state, action: PayloadAction<PageInfo>) {
      state.page = { ...action.payload };
    },
    resetNotifications() {
      return initialState;
    },
  },
});

export const {
  setFirstOfListNotification,
  addNotification,
  readAllNotifications,
  setUnreadNotification,
  updateNotificationPageInfo,
  addListNotification,
  decreaseUnreadNotification,
  resetNotifications,
} = notificationSlice.actions;
export const selectNotificationList = (state: RootState) =>
  state.notification.data;
export const selectNumberNewNotifications = (state: RootState) =>
  state.notification.new;
export const selectNotificationPageInfo = (state: RootState) =>
  state.notification.page;
export default notificationSlice.reducer;
