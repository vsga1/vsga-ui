import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../index';

export interface Note {
  code: string | null;
  header: string;
  description: string;
  status: 'ACTIVE' | 'INACTIVE';
}

export interface RoomNote {
  header: string;
  description: string;
  status: 'OPEN' | 'EDIT';
}

interface NoteSlice {
  personal: Note[];
  room: RoomNote;
}

const initialState: NoteSlice = {
  personal: [],
  room: {
    header: '',
    description: '',
    status: 'OPEN',
  },
};

export const noteSlice = createSlice({
  name: 'note',
  initialState,
  reducers: {
    initNote(state, action: PayloadAction<Note[]>) {
      state.personal = action.payload;
    },
    addNote(state, action: PayloadAction<Note>) {
      state.personal.push(action.payload);
    },
    updateNote(state, action: PayloadAction<Omit<Note, 'status'>>) {
      if (action.payload.code !== null) {
        const index = state.personal.findIndex(
          (note) => note.code === action.payload.code
        );
        state.personal[index].description = action.payload.description;
        state.personal[index].header = action.payload.header;
      }
    },
    updateRoomNote(state, action: PayloadAction<Partial<RoomNote>>) {
      const { header, description, status } = state.room;
      state.room.description = action.payload.description || description;
      state.room.header = action.payload.header || header;
      state.room.status = action.payload.status || status;
    },
    removeNote(state, action: PayloadAction<string | null>) {
      if (action.payload !== null) {
        const index = state.personal.findIndex(
          (note) => note.code === action.payload
        );
        state.personal.splice(index, 1);
      }
    },
  },
});

export const { initNote, addNote, updateNote, updateRoomNote, removeNote } = noteSlice.actions;
export const selectAllNote = (state: RootState) => state.note.personal;
export const selectRoomNote = (state: RootState) => state.note.room;
export default noteSlice.reducer;
