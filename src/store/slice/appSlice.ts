import { RoomDefaultBackgrounds } from '@/common/utils/constants/room';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../index';

interface NotificationType {
  msg: string;
  show: boolean;
  status: 'info' | 'error' | 'warn';
}

interface AppState {
  loading: boolean;
  theme: 'dark' | 'light';
  noti: NotificationType;
  background: {
    src: string;
    volume: number;
  };
}

const initialState: AppState = {
  loading: false,
  theme: 'light',
  noti: {
    msg: '',
    show: false,
    status: 'error',
  },
  background: {
    src: RoomDefaultBackgrounds[3],
    volume: 0,
  },
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setAppLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
    setAppTheme(state, action: PayloadAction<'light' | 'dark'>) {
      state.theme = action.payload;
    },
    showNotification(
      state,
      action: PayloadAction<Omit<NotificationType, 'show'>>
    ) {
      state.noti.msg = action.payload.msg;
      state.noti.status = action.payload.status;
      state.noti.show = true;
    },
    hideNotification(state) {
      state.noti.msg = '';
      state.noti.show = false;
    },
    setBackgroundSrc(state, action: PayloadAction<string>) {
      state.background.src = action.payload;
    },
    setBackgroundVol(state, action: PayloadAction<number>) {
      state.background.volume = action.payload;
    },
  },
});

export const {
  setAppLoading,
  setAppTheme,
  showNotification,
  hideNotification,
  setBackgroundSrc,
  setBackgroundVol,
} = appSlice.actions;
export const selectAppLoading = (state: RootState) => state.app.loading;
export const selectAppTheme = (state: RootState) => state.app.theme;
export const selectNotification = (state: RootState) => state.app.noti;
export const selectBackground = (state: RootState) => state.app.background;
export default appSlice.reducer;
