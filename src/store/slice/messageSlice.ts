import { ChatType } from '@/common/utils/enums';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { uniqueId } from 'lodash';
import moment from 'moment';
import type { RootState } from '../index';

export interface Message {
  id: string;
  text: string;
  sender: string;
  receiver: string;
  sentAt: string;
  type: ChatType;
}

interface MessageSlice {
  data: Message[];
}

const initialState: MessageSlice = {
  data: [],
};

export const messageSlice = createSlice({
  name: 'message',
  initialState,
  reducers: {
    addMessage(state, action: PayloadAction<{ text: string, sender: string, receiver: string, type: ChatType }>) {
      const { text, sender, receiver, type } = action.payload;
      const newMessage: Message = {
        id: uniqueId(),
        text,
        sender,
        receiver,
        type,
        sentAt: moment().format('LT'), 
      };
      state.data = [...state.data, newMessage];
    },
    clearMessageList(state) {
      state.data = [];
    },
  },
});

export const { addMessage, clearMessageList } = messageSlice.actions;
export const selectAllMessage = (state: RootState) => state.message.data;
export default messageSlice.reducer;
