import { Role } from '@/common/utils/enums';
import { Room } from '@/common/utils/interface';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '..';

export interface UserInRoom {
  id: string;
  username: string;
  avatar_img_url: string;
  role: Role;
}

export interface RoomData {
  room_info?: Room;
  role?: Role;
  users: Array<UserInRoom>;
}

const initialState: RoomData = {
  users: [],
};

export const roomSlice = createSlice({
  name: 'room',
  initialState,
  reducers: {
    initRoom(state, action: PayloadAction<RoomData>) {
      return { ...state, ...action.payload };
    },
    setRoomInfo(state, action: PayloadAction<Room>) {
      state.room_info = action.payload;
    },
    setUsers(state, action: PayloadAction<Array<UserInRoom>>) {
      state.users = action.payload;
    },
    setRoleUsers(
      state,
      action: PayloadAction<Pick<UserInRoom, 'username' | 'role'>>
    ) {
      const index = state.users?.findIndex(
        (item) => item.username === action.payload.username
      );
      if (index !== -1) {
        state.users[index].role = action.payload.role;
      }
    },
    removeUser(state, action: PayloadAction<string>) {
      if (action.payload !== '') {
        state.users = state.users?.filter((u) => u.id !== action.payload);
      }
    },
    setRole(state, action: PayloadAction<Role>) {
      state.role = action.payload;
    },
    outRoom() {
      return {
        ...initialState,
      };
    },
  },
});

export const { initRoom, setUsers, setRoomInfo, removeUser, outRoom, setRole, setRoleUsers } =
  roomSlice.actions;

export const selectRoom = (state: RootState) => state.room;
export const selectRole = (state: RootState) => state.room.role;
export const selectRoomInfo = (state: RootState) => state.room.room_info;
export const selectUsers = (state: RootState) => state.room.users;

export default roomSlice.reducer;
