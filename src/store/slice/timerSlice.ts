import { TimerStatus } from '@/common/utils/enums';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../index';

export type TimerType = 'PERSONAL' | 'ROOM';

export interface Timer {
  status: TimerStatus;
  endAt?: string;
}

interface TimerSlice {
  personal: Timer;
  group: Timer;
}

const initialState: TimerSlice = {
  personal: {
    status: TimerStatus.IDLE,
  },
  group: {
    status: TimerStatus.IDLE,
  },
};

export const timerSlice = createSlice({
  name: 'timer',
  initialState,
  reducers: {
    setEndAt(state, action: PayloadAction<{ value: string; type: TimerType }>) {
      if (action.payload.type === 'PERSONAL') {
        state.personal.endAt = action.payload.value;
      } else {
        state.group.endAt = action.payload.value;
      }
    },
    setStatus(
      state,
      action: PayloadAction<{ value: TimerStatus; type: TimerType }>
    ) {
      if (action.payload.type === 'PERSONAL') {
        state.personal.status = action.payload.value;
      } else {
        state.group.status = action.payload.value;
      }
    },
  },
});

export const { setEndAt, setStatus } = timerSlice.actions;
export const selectPersonalTimer = (state: RootState) => state.timer.personal;
export const selectGroupTimer = (state: RootState) => state.timer.group;

export default timerSlice.reducer;
