import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../index';

export const SET_USER = 'USER/SET_USER';

export interface Info {
  username: string;
  name: string;
  email: string;
  avatar_img_url: string;
  description: string;
  display_name: string;
  full_name: string;
  address: string;
  gender: 'MALE' | 'FEMALE' | '';
  age: number;
  code: string;
  is_friend?: boolean;
}

interface UserState {
  data: Info;
  status: 'existed' | 'non_existed';
}

const initialState: UserState = {
  data: {
    username: '',
    name: '',
    email: '',
    avatar_img_url: '',
    description: '',
    display_name: '',
    full_name: '',
    address: '',
    gender: '',
    age: 1,
    code: '',
  },
  status: 'non_existed',
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<Info>) {
      return {
        ...state,
        status: 'existed',
        data: {
          ...action.payload,
        },
      };
    },
    setAvatar(state, action: PayloadAction<string>) {
      state.data.avatar_img_url = action.payload;
    },
    removeUser(state) {
      return {
        ...state,
        ...initialState,
      };
    },
  },
});

export const { setUser, removeUser, setAvatar } = userSlice.actions;
export const selectUser = (state: RootState) => state.user;
export const selectUserInfo = (state: RootState) => state.user.data;
export default userSlice.reducer;
