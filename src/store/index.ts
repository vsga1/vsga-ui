import { configureStore } from '@reduxjs/toolkit';
// Reducer
import UserReducer from './slice/userSlice';
import AppReducer from './slice/appSlice';
import TaskReducer from './slice/taskSlice';
import NoteReducer from './slice/noteSlice';
import MessageReducer from './slice/messageSlice';
import RoomReducer from './slice/roomSlice';
import TimerReducer from './slice/timerSlice';
import FriendReducer from './slice/friendSlice';
import NotificationReducer from './slice/notificationSlice';

export const store = configureStore({
  reducer: {
    app: AppReducer,
    user: UserReducer,
    task: TaskReducer,
    note: NoteReducer,
    message: MessageReducer,
    room: RoomReducer,
    timer: TimerReducer,
    friend: FriendReducer,
    notification: NotificationReducer,
  },
  devTools: false,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
