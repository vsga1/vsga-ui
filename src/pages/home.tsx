import React, { useCallback, useEffect, useState } from 'react';
import Head from 'next/head';
import MainLayout from '@/layouts/MainLayout';
import AppBar from '@/common/components/AppBar';
import RoomCard from '@/common/components/Card/Room';
import Search from '@/common/components/Search';
import { Button, Col, Container, Modal, Pagination, Row } from 'react-bootstrap';
import CreateRoomForm from '@/common/components/Form/CreateRoom';
import { getAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import Loading from '@/common/components/Loading';
import useModal from '@/hooks/useModal';
import { MdSearchOff } from 'react-icons/md';
import { useIsFirstRender } from 'usehooks-ts';
import FilterTopic from '@/common/components/Search/FilterTopic';
import FriendList from '@/common/components/FriendList';
import usePagination from '@/hooks/usePagination';
import useTheme from '@/hooks/useTheme';

interface SearchParams {
  limit: number;
  offset?: number;
  name?: string;
  topics?: string;
}

const Home = () => {
  const dispatch = useAppDispatch();
  const [searchText, setSearchText] = useState<string>('');
  const [list, setList] = useState([]);
  const [pagination, setPagination] = useState({
    total: 1,
    current: 1,
    perPage: 12,
  });
  const [loading, setLoading] = useState<boolean>(false);
  const [selectedFilterOption, setSelectedFilterOption] = useState<string[]>(
    []
  );
  const showCreateRoom = useModal();
  const isMount = useIsFirstRender();
  const paginateItems = usePagination(pagination.total, pagination.current);
  const theme = useTheme();

  const searchRoomCallback = useCallback(
    async (offset = 0, searchText?: string, topics?: string[]) => {
      setLoading(true);
      const params: SearchParams = {
        limit: pagination.perPage,
      };
      if (searchText) {
        params.name = searchText;
      }
      if (offset) {
        params.offset = offset;
      }
      if (topics?.length) {
        params.topics = topics.join(',');
      }
      const { code, data, meta } = await getAPI('/v1/room/search', params);
      if (code === 200) {
        const total = Math.ceil(meta.total / meta.limit);
        const current = Math.floor(meta.offset / meta.limit) + 1;
        setPagination((prev) => ({
          ...prev,
          total,
          current,
        }));
        setList(data);
      } else {
        dispatch(
          showNotification({
            msg: data as string,
            status: 'error',
          })
        );
      }
      setLoading(false);
    },
    [dispatch, pagination.perPage]
  );

  useEffect(() => {
    searchRoomCallback();
  }, [searchRoomCallback]);

  const onChangeSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const onKeyDownSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      searchRoomCallback(0, searchText);
      setSelectedFilterOption([]);
    }
  };

  useEffect(() => {
    if (isMount) return;
    const delayDebounceFn = setTimeout(() => {
      searchRoomCallback(0, searchText.trim());
      setSelectedFilterOption([]);
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [dispatch, searchText]);

  useEffect(() => {
    searchRoomCallback(0, searchText.trim(), selectedFilterOption);
  }, [selectedFilterOption]);

  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      <Modal
        centered
        show={showCreateRoom.isShowModal}
        onHide={showCreateRoom.closeModal}
        scrollable
      >
        <Modal.Header closeButton {...(theme === 'dark' ? {closeVariant: 'white'} : {})}>
          <Modal.Title>Create new room</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <CreateRoomForm />
        </Modal.Body>
      </Modal>
      <MainLayout>
        <AppBar />
        <div id="room-page" className="page-content">
          <div className="rooms">
            <div
              className="head d-flex align-items-center justify-content-between flex-wrap pt-3 pb-2"
              style={{ position: 'sticky', top: '52px' }}
            >
              <div className="d-flex align-items-center mb-2 mb-md-0">
                <Search
                  placeholder="Search room"
                  id="search-room"
                  value={searchText}
                  onChange={onChangeSearch}
                  onKeyDown={onKeyDownSearch}
                />
                <FilterTopic
                  options={selectedFilterOption}
                  setOptions={setSelectedFilterOption}
                />
              </div>
              <Button
                variant="secondary"
                onClick={showCreateRoom.openModal}
                className="text-uppercase mb-2 mb-md-0 fw-bold"
              >
                Create
              </Button>
            </div>
            <Container>
              <Row className="gy-4 mt-0 mx-0">
                {loading ? (
                  <div className="d-flex justify-content-center align-items-center">
                    <Loading />
                  </div>
                ) : list.length ? (
                  list.map((room, index) => (
                    <Col md={12} lg={6} xl={4} key={index}>
                      <RoomCard room={room} />
                    </Col>
                  ))
                ) : (
                  <p className="not-found">
                    <MdSearchOff />
                    No room found
                  </p>
                )}
              </Row>
            </Container>
            {!!list.length && (
              <Pagination className="mt-3 justify-content-center">
                <Pagination.Prev
                  disabled={pagination.current === 1}
                  onClick={(e) => {
                    e.currentTarget.blur();
                    if (pagination.current > 1) {
                      searchRoomCallback(
                        (pagination.current - 2) * pagination.perPage
                      );
                    }
                  }}
                />
                {paginateItems.map((item, index) => {
                  const cur = pagination.current;
                  return (
                    <Pagination.Item
                      key={index}
                      active={item === cur}
                      disabled={item === '...'}
                      onClick={(e) => {
                        e.currentTarget.blur();
                        if (item !== cur && typeof item === 'number') {
                          searchRoomCallback(
                            (item - 1) * pagination.perPage,
                            searchText
                          );
                        }
                      }}
                    >
                      {item}
                    </Pagination.Item>
                  );
                })}
                <Pagination.Next
                  disabled={pagination.current === pagination.total}
                  onClick={(e) => {
                    e.currentTarget.blur();
                    if (pagination.current < pagination.total) {
                      searchRoomCallback(
                        pagination.current * pagination.perPage
                      );
                    }
                  }}
                />
              </Pagination>
            )}
          </div>
          <FriendList />
        </div>
      </MainLayout>
    </>
  );
};

export default Home;
