import PageSection from '@/common/components/PageSection';
import { STATIC_IMAGES_PATH } from '@/common/utils/constants/path';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import { Button, Col, Container, Nav, Navbar, Row } from 'react-bootstrap';
import { RiMenu3Line } from 'react-icons/ri';
import Slider, { Settings }  from 'react-slick';

const settings: Settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  autoplay: true,
  responsive: [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
      }
    }
  ]
};

const teamData = [
  {
    name: 'Trần Mạnh Đức',
    avatar: 'tranmanhduc.jpg',
    role: 'Back-end Tech Lead',
  }, 
  {
    name: 'Đoàn Thế Huy',
    avatar: 'doanthehuy.jpg',
    role: 'Front-end Developer',
  },
  {
    name: 'Nguyễn Gia Huy',
    avatar: 'nguyengiahuy.jpg',
    role: 'Team Leader',
  },
  {
    name: 'Cao Hải Síl',
    avatar: 'caohaisil.jpg',
    role: 'Front-end Tech Lead',
  },
  {
    name: 'Nguyễn Việt Minh Tâm',
    avatar: 'nguyenvietminhtam.jpg',
    role: 'Mobile Developer',
  }
];

const LandingPage = () => {

  return (
    <>
      <Head>
        <title>VStudy | Meet, chat, and study</title>
      </Head>
      <Navbar fixed="top" variant="secondary" bg="white" className="shadow-sm" expand="md">
        <Container>
          <Navbar.Brand href="/">
            <Image
              src="/static/images/logo.png"
              alt="logo"
              width="45"
              height="47"
              priority
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbar-nav" className="border-0">
            <RiMenu3Line />
          </Navbar.Toggle>
          <Navbar.Collapse className="justify-content-end mb-2 mb-md-0" id="navbar-nav">
            <Nav className="ms-auto">
              <Nav.Item>
                <Nav.Link href="#home" className="p-3 fs-5">
                  Home
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link href="#about" className="p-3 fs-5">
                  About
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link href="#team"  className="p-3 fs-5">
                  Team
                </Nav.Link>
              </Nav.Item>
            </Nav>
            <Button 
              as="a" 
              href="/login" 
              variant="secondary" 
              className="ms-auto fw-bold"
              target="_blank"
              size="lg"
            >
              Get started
            </Button>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <main>
        <PageSection id="home">
          <Row>
            <Col>
              <div className="text-center pt-3">
                <div className="mb-4">
                  <img className="lp-img" src="static/images/auth_banner.png" alt="Auth Banner" />
                </div>
                <h1 className="fw-bold fs-1 mb-4">
                  Meet, chat, and study with students from all over the world 
                </h1>
                <p className="fs-4 mb-4">
                  Join the largest global student community online and say goodbye to lack of motivation.
                </p>
                <Button 
                  as="a" 
                  href="/home" 
                  variant="outline-secondary" 
                  target="_blank"
                  className="fw-bold"
                  size="lg"
                >
                  Browse Rooms
                </Button>
              </div>
            </Col>
          </Row>
        </PageSection>
        <PageSection id="about">
          <Row className="align-items-center text-center text-lg-start">
            <Col xs={12} lg={6}>
              <div className="mb-4 mb-lg-0">
                <img className="lp-img" src="static/images/group_study.png" alt="Group Study" />
              </div>
            </Col>
            <Col xs={12} lg={6}>
              <div>
                <h2 className="fw-bold fs-1 mb-4">Group Study</h2>
                <p className="fs-4 mb-4">
                  Join with your team and boost your productivity with {' '} 
                  <span className="fw-bold">white board</span>
                  , {' '}
                  <span className="fw-bold">room timer</span>
                  {' '} and {' '}
                  <span className="fw-bold">tasks</span>
                  .
                </p>
              </div>
            </Col>
          </Row>
        </PageSection>
        <PageSection>
          <Row className="align-items-center text-center text-lg-start">
            <Col xs={12} lg={{ span: 6, order: 2 }}>
              <div  className="mb-4 mb-lg-0">
                <img className="lp-img" src="static/images/study_universe.png" alt="Study Universe" />
              </div>
            </Col>
            <Col xs={12} lg={{ span: 6, order: 1 }}>
              <div>
                <h2 className="fw-bold fs-1 mb-4">Own Study Universe</h2>
                <p className="fs-4 mb-4">
                  Custom your very own study room with {' '} 
                  <span className="fw-bold">backgrounds</span>
                  , {' '}
                  <span className="fw-bold">personal timers</span>
                  {' '} and {' '}
                  <span className="fw-bold">tasks</span>
                  .
                </p>
                <Button 
                  as="a" 
                  href="/login" 
                  variant="outline-secondary" 
                  target="_blank"
                  className="fw-bold"
                  size="lg"
                >
                  Discover now
                </Button>
              </div>
            </Col>
          </Row>
        </PageSection>
        <PageSection id="team">
          <div className="text-center" style={{ maxWidth: 'calc(100% - 1.5rem)' }}>
            <h2 className="fw-bold fs-1 mb-4">
              Meet our team
            </h2>
            <Slider {...settings}>
              {
                teamData.map((member, index) => (
                  <div key={index} className="p-3">
                    <div className="mb-3 rounded-4 overflow-hidden">
                      <img className="lp-img" src={`${STATIC_IMAGES_PATH}${member.avatar}`} alt={member.name} />
                    </div>
                    <p className="fs-5 fw-bold text-secondary">{member.name}</p>
                    <p className="text-muted fs-6">{member.role}</p>
                  </div>
                ))
              }
            </Slider>
          </div>
        </PageSection>
        <PageSection background="primary">
          <div className="text-center">
            <h2 className="text-white fw-bold fs-1 mb-4">
              What are you waiting for?
              <br></br>
              Join the VStudy!
            </h2>
            <Button 
              as="a" 
              href="/login" 
              variant="primary" 
              target="_blank"
              className="fw-bold bg-white text-primary border-primary px-5"
              size="lg"
            >
              Join with us
            </Button>
          </div>
        </PageSection>
      </main>
      <footer>
        <div className="container py-4 text-center d-flex flex-column align-items-center">
          <Link href="/" className="fw-bold mb-3 text-decoration-none d-flex align-items-center">
            <Image
              src="/static/images/logo.png"
              alt="logo"
              width="45"
              height="47"
              priority
            />
            <span className="ms-2 fw-bold fs-2">
              VStudy
            </span>
          </Link>
          <small className="text-muted">Copyright © 2023</small>
        </div>
      </footer>
    </>
  );
};

export default LandingPage;
