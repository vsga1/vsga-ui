import Head from 'next/head';
import { NextPage, NextPageContext } from 'next';
import { useRouter } from 'next/router';
import { FaLongArrowAltLeft } from 'react-icons/fa';

interface Props {
  statusCode?: number;
}

const Error: NextPage<Props> = ({ statusCode }) => {
  const router = useRouter();

  return (
    <div
      id="error"
      className="d-flex flex-column align-items-center justify-content-center"
    >
      <Head>
        <title>Error</title>
      </Head>
      <div className="status-code">{statusCode}</div>
      <div className="sub-title">Something&apos;s missing</div>
      <div className="description">
        This page is missing or your assembled the link incorrectly
      </div>
      <span
        className="go-back mt-4 d-flex align-items-center"
        onClick={() => router.back()}
      >
        <FaLongArrowAltLeft className="me-2" />
        Go back
      </span>
    </div>
  );
};

Error.getInitialProps = ({ res, err }: NextPageContext) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
