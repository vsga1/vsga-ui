import Head from 'next/head';
import React, { useState } from 'react';
import AuthenticateLayout from '@/layouts/Authenticate';
import { Button, Form, InputGroup, Spinner } from 'react-bootstrap';
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import schema from '@/common/utils/yupSchema/resetPasswordForm';
import { useRouter } from 'next/router';
import { FiEye, FiEyeOff } from 'react-icons/fi';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { putAPI } from '@/common/utils/customAPI';

interface FormData {
  password: string;
  confirmPassword: string;
}

const ResetPassword = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<FormData>({
    resolver: yupResolver(schema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });

  const router = useRouter();

  const dispatch = useAppDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [loading, setLoading] = useState(false);

  const onSubmit: SubmitHandler<FormData> = async ({ password }) => {
    setLoading(true);
    const token = router.query.verifyToken;
    if (token) {
      const res = await putAPI('/v1/account/password', {
        token,
        password,
      });
      if (res.code === 200) {
        router.push('/login');
      } else {
        dispatch(
          showNotification({
            msg: res.data as string,
            status: 'error',
          })
        );
      }
    } else {
      dispatch(
        showNotification({
          msg: 'Token is invalid!',
          status: 'error',
        })
      );
    }
    setLoading(false);
  };

  return (
    <>
      <Head>
        <title>Reset Password</title>
      </Head>
      <AuthenticateLayout>
        <div id="ResetPassword">
          <div className="header mb-3">
            <h1 className="mb-2">Reset Password</h1>
            <p>Choose your new password</p>
          </div>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group className="mb-3">
              <Form.Label>Password</Form.Label>
              <InputGroup>
                <Form.Control
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Password"
                  {...register('password', {
                    onChange: () => {
                      clearErrors('password');
                    },
                  })}
                  isInvalid={!!errors.password}
                />
                <InputGroup.Text
                  className="toggle-password rounded-end"
                  onClick={() => setShowPassword((prev) => !prev)}
                >
                  {showPassword ? <FiEyeOff /> : <FiEye />}
                </InputGroup.Text>
                <Form.Control.Feedback type="invalid">
                  {errors.password?.message}
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Confirm Password</Form.Label>
              <InputGroup>
                <Form.Control
                  type={showConfirmPassword ? 'text' : 'password'}
                  placeholder="Confirm Password"
                  {...register('confirmPassword', {
                    onChange: () => {
                      clearErrors('confirmPassword');
                    },
                  })}
                  isInvalid={!!errors.confirmPassword}
                />
                <InputGroup.Text
                  className="toggle-password rounded-end"
                  onClick={() => setShowConfirmPassword((prev) => !prev)}
                >
                  {showConfirmPassword ? <FiEyeOff /> : <FiEye />}
                </InputGroup.Text>
                <Form.Control.Feedback type="invalid">
                  {errors.confirmPassword?.message}
                </Form.Control.Feedback>
              </InputGroup>
              <Form.Text>
                At least 8 characters, one uppercase letter, lowercase letter,
                number and special letter.
              </Form.Text>
            </Form.Group>
            <Button
              variant="secondary"
              type="submit"
              className="w-100 text-uppercase"
              disabled={loading}
            >
              <Spinner
                animation="border"
                size="sm"
                className={!loading ? 'd-none' : 'me-2'}
              />
              Reset Password
            </Button>
          </Form>
        </div>
      </AuthenticateLayout>
    </>
  );
};

export default ResetPassword;
