import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { Room } from '@/common/utils/interface';
import MainLayout from '@/layouts/MainLayout';
import AppBar from '@/common/components/AppBar';
import { getAPI } from '@/common/utils/customAPI';
import { showNotification } from '@/store/slice/appSlice';
import { useAppDispatch } from '@/hooks';
import RoomSlider from '@/common/components/RoomSlider';
import Loading from '@/common/components/Loading';
import FriendList from '@/common/components/FriendList';

const Rooms = () => {
  const dispatch = useAppDispatch();
  const [latestList, setLatestList] = useState<Room[]>([]);
  const [favoredList, setFavoredList] = useState<Room[]>([]);
  const [createdList, setCreatedList] = useState<Room[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchRooms = async () => {
      setLoading(true);
      const res = await Promise.all([
        getAPI('/v1/room/latest'),
        getAPI('/v1/room/search?is_my_room=true'),
        getAPI('/v1/room/search?is_favored=true'),
      ]);
      const errorMsg = res.find((item) => item.code !== 200);
      if (errorMsg) {
        dispatch(
          showNotification({
            msg: errorMsg.data as string,
            status: 'error',
          })
        );
      }
      if (res[0].code === 200) {
        setLatestList(res[0].data);
      }
      if (res[1].code === 200) {
        setCreatedList(res[1].data);
      }
      if (res[2].code === 200) {
        setFavoredList(res[2].data);
      }
      setLoading(false);
    };
    fetchRooms();
  }, [dispatch]);

  return (
    <>
      <Head>
        <title>Rooms</title>
      </Head>
      <MainLayout>
        <AppBar />
        <div className="page-content" id="room-page">
          <div className="rooms">
            {loading ? (
              <div className="d-flex justify-content-center align-items-center">
                <Loading />
              </div>
            ) : (
              <>
                <RoomSlider list={latestList} title="Last Visited Rooms" />
                <RoomSlider list={createdList} title="My Rooms" />
                <RoomSlider list={favoredList} title="My Favorite Rooms" />
              </>
            )}
          </div>
          <FriendList />
        </div>
      </MainLayout>
    </>
  );
};

export default Rooms;
