import React, { useState } from 'react';
import Link from 'next/link';
import Head from 'next/head';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { Form, Spinner, Button } from 'react-bootstrap';
import schema from '@/common/utils/yupSchema/forgotPasswordForm';
import AuthenticateLayout from '@/layouts/Authenticate';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { getAPI } from '@/common/utils/customAPI';

interface FormData {
  email: string;
}

const ForgotPassword = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<FormData>({
    resolver: yupResolver(schema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit'
  });

  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);

  const onSubmit = handleSubmit(async ({ email }) => {
    setLoading(true);
    const res = await getAPI(`/v1/account/reset-password?email=${email}`);
    if (res.code === 200) {
      dispatch(
        showNotification({ msg: 'The email has been sent.', status: 'info' })
      );
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    setLoading(false);
  });

  return (
    <>
      <Head>
        <title>Forgot password</title>
      </Head>
      <AuthenticateLayout>
        <div id="forgot-password">
          <div className="header mb-3">
            <h1 className="mb-2">Forgot password?</h1>
            <p>Fill your email to continue</p>
          </div>
          <Form onSubmit={onSubmit} className="d-flex flex-column d-lg-block">
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter email"
                {...register('email', {
                  onChange: () => {
                    clearErrors('email')
                  },
                })}
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email?.message}
              </Form.Control.Feedback>
            </Form.Group>
            <Button
              variant="secondary"
              type="submit"
              className="text-uppercase full-width mt-2"
              disabled={loading}
            >
              <Spinner
                animation="border"
                size="sm"
                className={!loading ? 'd-none' : 'me-2'}
              />
              Send mail
            </Button>
          </Form>
          <div className="link-to mt-4 mt-lg-0">
            Already have an account?&nbsp;
            <Link href="/login">Login</Link>
          </div>
        </div>
      </AuthenticateLayout>
    </>
  );
};

export default ForgotPassword;
