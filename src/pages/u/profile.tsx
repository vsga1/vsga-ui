import React from 'react';
import Head from 'next/head';
import MainLayout from '@/layouts/MainLayout';
import AppBar from '@/common/components/AppBar';
import FormProfile from '@/common/components/Form/Profile';
import FormSecurity from '@/common/components/Form/Security';
import { Tab, Tabs } from 'react-bootstrap';

const Profile = () => {
  return (
    <>
      <Head>
        <title>Profile</title>
      </Head>
      <MainLayout>
        <AppBar />
        <div id="profile-page" className="page-content">
          <Tabs
            defaultActiveKey="profile"
            id="uncontrolled-tab-example"
            className="mb-3"
          >
            <Tab eventKey="profile" title="Profile">
              <FormProfile />
            </Tab>
            <Tab eventKey="security" title="Security">
              <FormSecurity />
            </Tab>
          </Tabs>
        </div>
      </MainLayout>
    </>
  );
};

export default Profile;
