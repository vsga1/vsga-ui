import AppBar from '@/common/components/AppBar';
import FriendList from '@/common/components/FriendList';
import Loading from '@/common/components/Loading';
import Search from '@/common/components/Search';
import UserSearchItem from '@/common/components/UserSearchItem';
import { getAPI, postAPI } from '@/common/utils/customAPI';
import MainLayout from '@/layouts/MainLayout';
import { showNotification } from '@/store/slice/appSlice';
import { removeFriend } from '@/store/slice/friendSlice';
import { Info, selectUserInfo } from '@/store/slice/userSlice';
import { debounce } from 'lodash';
import Head from 'next/head';
import React, { useCallback, useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { MdSearchOff } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFirstRender } from 'usehooks-ts';

const FindUser = () => {
  const [searchText, setSearchText] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [userList, setUserList] = useState<Info[]>([]);
  const isMount = useIsFirstRender();
  const dispatch = useDispatch();
  const currentUserInfo = useSelector(selectUserInfo);

  const onChangeSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const onKeyDownSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      searchUserCallback(searchText.trim());
    }
  };

  const handleUnfriend = async (username: string) => {
    const res = await postAPI('/v1/friend/unfriend', { username });
    if (res.code === 200) {
      const updatedUserList = userList.map((user) =>
        user.username === username ? { ...user, is_friend: false } : user
      );
      setUserList(updatedUserList);
      dispatch(removeFriend(username));
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
  };

  const handleAddFriend = debounce(async (username: string) => {
    const res = await postAPI('/v1/notification/add-friend', {
      title: 'Friend request',
      body: `${currentUserInfo.username} sent you a friend request.`,
      data: {
        type: 'FRIEND_REQUEST',
        time: new Date().toISOString(),
        senderImage: currentUserInfo.avatar_img_url || null,
        username: currentUserInfo.username,
      },
      username,
    });
    if (res.code === 200) {
      dispatch(
        showNotification({
          msg: 'Friend request sent successfully!',
          status: 'info',
        })
      );
    } else {
      dispatch(
        showNotification({
          msg: 'Failed to send friend request!',
          status: 'error',
        })
      );
    }
  }, 500);

  useEffect(() => {
    if (isMount) return;
    const delayDebounceFn = setTimeout(() => {
      searchUserCallback(searchText.trim());
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [dispatch, searchText]);

  const searchUserCallback = useCallback(
    async (searchText?: string) => {
      if (!searchText) return;
      setLoading(true);
      const { code, data } = await getAPI('/v1/account/search-friend', {
        name: searchText,
      });
      if (code === 200) {
        setUserList(data);
      } else {
        dispatch(
          showNotification({
            msg: data as string,
            status: 'error',
          })
        );
      }
      setLoading(false);
    },
    [dispatch]
  );

  useEffect(() => {
    searchUserCallback();
  }, [searchUserCallback]);

  return (
    <>
      <Head>
        <title>Users</title>
      </Head>
      <MainLayout>
        <AppBar />
        <div id="room-page" className="page-content">
          <div className="rooms">
            <div
              className="head d-flex align-items-center justify-content-between flex-wrap pt-3 pb-2"
              style={{ position: 'sticky', top: '52px' }}
            >
              <div className="d-flex align-items-center mb-2 mb-md-0">
                <Search
                  placeholder="Search user"
                  id="find-user"
                  value={searchText}
                  onChange={onChangeSearch}
                  onKeyDown={onKeyDownSearch}
                />
              </div>
            </div>
            <Row className="gy-2 gx-2 mt-0 mx-0">
              {loading ? (
                <div className="d-flex justify-content-center align-items-center">
                  <Loading />
                </div>
              ) : !searchText ? (
                <p className="text-primary text-center fw-bold fs-5">
                  Find some users.
                </p>
              ) : userList.length ? (
                userList.map((user) => (
                  <Col sm={12} xl={6} key={user.username}>
                    <UserSearchItem
                      user={user}
                      isCurrentUser={user.username === currentUserInfo.username}
                      handleUnfriend={handleUnfriend}
                      handleAddFriend={handleAddFriend}
                    />
                  </Col>
                ))
              ) : (
                <p className="not-found">
                  <MdSearchOff />
                  No users found.
                </p>
              )}
            </Row>
          </div>
          <FriendList />
        </div>
      </MainLayout>
    </>
  );
};

export default FindUser;
