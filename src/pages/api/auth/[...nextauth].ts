import NextAuth, { Account, Session, User } from 'next-auth';
import GoogleProvider from 'next-auth/providers/google';
import { JWT } from 'next-auth/jwt';
import DiscordProvider from 'next-auth/providers/discord';
import FacebookProvider from 'next-auth/providers/facebook';
import Credentials from 'next-auth/providers/credentials';
import axios from 'axios';

export const authOptions = {
  providers: [
    GoogleProvider({
      clientId: `${process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID}`,
      clientSecret: `${process.env.NEXT_PUBLIC_GOOGLE_CLIENT_SECRET}`,
    }),
    FacebookProvider({
      clientId: `${process.env.NEXT_PUBLIC_FACEBOOK_APP_ID}`,
      clientSecret: `${process.env.NEXT_PUBLIC_FACEBOOK_CLIENT_SECRET}`,
    }),
    DiscordProvider({
      clientId: `${process.env.NEXT_PUBLIC_DISCORD_CLIENT_ID}`,
      clientSecret: `${process.env.NEXT_PUBLIC_DISCORD_CLIENT_SECRET}`,
    }),
    Credentials({
      name: 'credentials',
      credentials: {
        username: {},
        password: {},
      },
      async authorize(credentials) {
        try {
          if (!credentials?.username || !credentials.password) {
            throw new Error('Missing Information!');
          }
          const { username, password } = credentials;
          const res = await axios.post(
            `${process.env.NEXT_PUBLIC_API_DOMAIN}/vstudy/v1/account/login`,
            {
              username,
              password,
            }
          );
          if (res.data.meta.code === 200) {
            const profile = await axios.get(
              `${process.env.NEXT_PUBLIC_API_DOMAIN}/vstudy/v1/account/profile`,
              {
                headers: {
                  Authorization: `Bearer ${res.data.data?.access_token}`,
                  'Content-Type': 'application/json',
                },
              }
            );
            return {
              access_token: res.data.data?.access_token,
              name: credentials.username,
              email: profile.data.data.email,
              image: profile.data.data.avatar_img_url,
            };
          } else {
            throw new Error(res.data.meta.message);
          }
        } catch (error) {
          if (axios.isAxiosError(error)) {
            throw new Error(
              error?.response?.data?.meta?.message || error.message
            );
          } else {
            throw error;
          }
        }
      },
    }),
  ],
  callbacks: {
    async jwt({
      token,
      user,
      account,
    }: {
      token: JWT;
      user?: User;
      account?: Account;
    }) {
      let returnedToken: unknown;
      if (account?.id_token) {
        const { data } = await axios.post(
          `${process.env.NEXT_PUBLIC_API_DOMAIN}/vstudy/v1/account/google/login`,
          {
            token: account.id_token,
          }
        );
        returnedToken = data.data.access_token;
      }
      returnedToken = returnedToken ? returnedToken : user?.access_token;
      if (returnedToken) {
        token.accessToken = returnedToken;
      }
      return token;
    },
    session({ session, token }: { session: Session; token: JWT }) {
      session.accessToken = token.accessToken;
      return session;
    },
  },
};

export default NextAuth(authOptions);
