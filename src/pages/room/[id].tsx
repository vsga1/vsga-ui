import React, { useCallback, useEffect, useState } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Button, Dropdown, Modal } from 'react-bootstrap';
import {
  MdImage,
  MdLightMode,
  MdDarkMode,
  MdStickyNote2,
  MdSettings,
  MdPersonAddAlt1,
} from 'react-icons/md';
import {
  RiTimerFill,
  RiArtboardFill,
  RiPencilRuler2Fill,
} from 'react-icons/ri';
import { BiExit } from 'react-icons/bi';
import { useAppDispatch, useAppSelector } from '@/hooks';
import {
  selectAppTheme,
  setAppTheme,
  showNotification,
} from '@/store/slice/appSlice';
import ItemCard from '@/common/components/Card/Popup';
import { useRouter } from 'next/router';
import RoomSideBar from '@/common/components/RoomSideBar';
import BlankLayout from '@/layouts/BlankLayout';
import { selectUser } from '@/store/slice/userSlice';
import { SocketContext } from '@/context/socket';
import {
  DONE_TASK,
  EDIT_TASK,
  GET_NEW_TASK,
  JOIN_ROOM,
  JOIN_ROOM_ERROR,
  KICK_USER,
  OUT_ROOM,
  REMOVE_TASK,
  ROOM_DATA,
  ROOM_END,
  SOCKET_ERROR,
  UPDATE_ROLE,
  UPDATE_ROOM_NAME,
} from '@/common/utils/constants/event';
import {
  selectRole,
  selectRoomInfo,
  setRole,
  setRoleUsers,
  setRoomInfo,
  setUsers,
} from '@/store/slice/roomSlice';
import Timer from '@/common/components/Timer';
import { Tooltip } from 'react-tooltip';
import { getAPI, postAPI } from '@/common/utils/customAPI';
import { clearMessageList } from '@/store/slice/messageSlice';
import {
  addTask,
  doneTask,
  editRoomTask,
  removeRoomTask,
  userDoneTask,
} from '@/store/slice/taskSlice';
import RoomTimer from '@/common/components/Timer/RoomTimer';
import RoomNote from '@/common/components/Note/RoomNote';
import {
  settingAuthorization,
  timerAuthorization,
} from '@/common/utils/roomAuthorization';
import RoomBackground from '@/common/components/RoomBackground';
import BackgroundSetting from '@/common/components/RoomBackground/BackgroundSetting';
import useModal from '@/hooks/useModal';
import RoomName from '@/common/components/Form/RoomName';
import RoomPassword from '@/common/components/Form/RoomPassword';
import { FaUserClock } from 'react-icons/fa';
import useWhiteboard from '@/hooks/useWhiteboard';
import useAuthorizeSocket from '@/hooks/useAuthorizeSocket';
import Loading from '@/common/components/Loading';
import JoinRoom from '@/common/components/Form/EnterRoomPassword';
import { Role } from '@/common/utils/enums';
import { Room } from '@/common/utils/interface';
import { useWindowSize } from 'usehooks-ts';
import { WINDOW_SIZE } from '@/common/utils/constants';
import InviteModal from '@/common/components/InviteModal';
import _ from 'lodash';
import { resetNotifications } from '@/store/slice/notificationSlice';

const Agora = dynamic(
  () => {
    return import('../../common/components/Agora');
  },
  {
    ssr: false,
  }
);

const Whiteboard = dynamic(
  () => {
    return import('../../common/components/Whiteboard');
  },
  {
    ssr: false,
  }
);

type PopupStackShow = 'left' | 'right' | 'both';

const Room = (props: { prevPath: string | null }) => {
  const { socket, isAuth } = useAuthorizeSocket();
  const theme = useAppSelector(selectAppTheme);
  const [timerShow, setTimerShow] = useState(true);
  const [roomTimerShow, setRoomTimerShow] = useState(false);
  const [roomNoteShow, setRoomNoteShow] = useState(true);
  const [backgroundShow, setBackgroundShow] = useState(true);
  const [showContent, setShowContent] = useState({
    waiting: true,
    main: false,
    whiteboard: false,
  });
  const [popupStackShow, setPopupStackShow] = useState<PopupStackShow>('both');
  const [roomName, setRoomName] = useState('');
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUser);
  const room = useAppSelector(selectRoomInfo);
  const role = useAppSelector(selectRole);
  const changeNameModal = useModal();
  const changePassModal = useModal();
  const joinRoomModal = useModal();
  const inviteModal = useModal();
  const router = useRouter();
  const { id } = router.query;
  const windowSize = useWindowSize();
  const isMobile = windowSize.width < WINDOW_SIZE.TABLET_SIZE;
  const { prevPath } = { ...props };

  const whiteboardParams = useWhiteboard(room?.whiteboard_id);

  const returnToPreviousPage = useCallback(() => {
    !prevPath
      ? router.replace('/home')
      : router.replace(`/${prevPath.split('/').pop()}`);
  }, []);

  const toggleWhiteboardClick = () => {
    if (!showContent.whiteboard) {
      setShowContent({
        waiting: false,
        main: false,
        whiteboard: true,
      });
    } else {
      setShowContent({
        waiting: false,
        main: true,
        whiteboard: false,
      });
    }
  };

  const joinRoomCallback = useCallback(
    async (role: Role) => {
      if (!user.data.username) return;

      socket.emit(JOIN_ROOM, {
        roomCode: id,
        username: user.data.username,
        avatar_img_url: user.data.avatar_img_url,
        role: role,
      });

      setShowContent({
        main: true,
        waiting: false,
        whiteboard: false,
      });

      joinRoomModal.closeModal();

      dispatch(setRole(role));
    },
    [dispatch, id, socket, user.data]
  );

  const handleToggleTheme = () => {
    theme === 'light'
      ? dispatch(setAppTheme('dark'))
      : dispatch(setAppTheme('light'));
  };

  const handleSwitchTimer = () => {
    handleTogglePopupStack('left');
    if (timerShow === roomTimerShow) {
      setTimerShow(true);
    } else {
      setRoomTimerShow((prev) => !prev);
      setTimerShow((prev) => !prev);
    }
  };

  const handleTogglePopupStack = (popup: PopupStackShow) => {
    if (isMobile) {
      if (popup === 'left') {
        setBackgroundShow(false);
      } else {
        setTimerShow(false);
        setRoomTimerShow(false);
        setRoomNoteShow(false);
      }
      setPopupStackShow(popup);
    } else {
      setPopupStackShow('both');
    }
  };
  useEffect(() => {
    setShowContent({ main: false, waiting: true, whiteboard: false });
    if (!id || !isAuth) return;
    const getRoomInfo = async () => {
      const res = await getAPI(`/v1/room/detail/${id}`);
      if (res.code === 200) {
        dispatch(setRoomInfo(res.data));
      } else {
        dispatch(
          showNotification({
            msg: res.data,
            status: 'error',
          })
        );
        returnToPreviousPage();
      }
    };
    if (!room || _.isEmpty(room)) {
      getRoomInfo();
      return;
    }
    const joinRoom = async () => {
      const { data, code } = await postAPI('/v1/event/join-room', {
        room_code: id,
      });

      if (code !== 200) {
        dispatch(
          showNotification({
            msg: data,
            status: 'error',
          })
        );
        returnToPreviousPage();
        return;
      }

      joinRoomCallback(data.role);
    };

    if (room?.configuration.is_protected) {
      joinRoomModal.openModal();
    } else {
      joinRoom();
    }

    const cleanUp = async () => {
      socket.emit(OUT_ROOM);
      dispatch(setRoomInfo({} as Room));
      dispatch(clearMessageList());
      dispatch(resetNotifications());
      socket.disconnect();
    };

    window.addEventListener('beforeunload', cleanUp);

    return () => {
      cleanUp();
      window.removeEventListener('beforeunload', cleanUp);
    };
  }, [dispatch, id, isAuth, room, socket]);

  useEffect(() => {
    setRoomName(room?.name as string);
  }, [room?.name]);

  useEffect(() => {
    socket.on(JOIN_ROOM, (msg) => {
      dispatch(
        showNotification({
          msg,
          status: 'info',
        })
      );
    });

    socket.on(ROOM_DATA, ({ users }) => {
      dispatch(setUsers(users));
    });

    socket.on(ROOM_END, () => {
      dispatch(
        showNotification({
          msg: 'Your room has ended!',
          status: 'info',
        })
      );
      returnToPreviousPage();
    });

    socket.on(UPDATE_ROLE, ({ username, role }) => {
      if (user.data.username === username) {
        dispatch(setRole(role));
      }
      dispatch(setRoleUsers({ username, role }));
    });

    socket.on(UPDATE_ROOM_NAME, ({ name }) => {
      if (room) {
        setRoomName(name);
        dispatch(
          showNotification({
            status: 'info',
            msg: 'Room name has been updated!',
          })
        );
      }
    });

    socket.on(JOIN_ROOM_ERROR, ({ msg }) => {
      dispatch(
        showNotification({
          msg,
          status: 'error',
        })
      );
      returnToPreviousPage();
    });

    socket.on(SOCKET_ERROR, ({ msg }) => {
      dispatch(
        showNotification({
          msg,
          status: 'error',
        })
      );
    });

    socket.on(KICK_USER, () => {
      dispatch(
        showNotification({
          msg: 'You have been kick!',
          status: 'info',
        })
      );
      returnToPreviousPage();
    });

    socket.on(GET_NEW_TASK, async ({ task }) => {
      const res = await getAPI(
        `/v1/task?room_code=${task.room_code}&type=${task.type}&scheme=${task.scheme}`
      );
      if (res.code !== 200) {
        dispatch(
          showNotification({
            msg: res.data,
            status: 'info',
          })
        );
        return;
      }
      dispatch(
        showNotification({
          status: 'info',
          msg: 'New task has been created!',
        })
      );
      dispatch(addTask(res.data[0]));
    });

    socket.on(DONE_TASK, ({ code, scheme, username, avatar }) => {
      if (username === user.data.username) {
        dispatch(doneTask(code));
      }
      dispatch(userDoneTask({ scheme, username, avatar_img_url: avatar }));
    });

    socket.on(EDIT_TASK, ({ task }) => {
      dispatch(editRoomTask(task));
      dispatch(
        showNotification({
          status: 'info',
          msg: `Task ${task?.description ?? ''} has been updated!`,
        })
      );
    });

    socket.on(REMOVE_TASK, ({ scheme }) => {
      dispatch(removeRoomTask(scheme));
    });

    return () => {
      socket.off(JOIN_ROOM);
      socket.off(ROOM_DATA);
      socket.off(ROOM_END);
      socket.off(UPDATE_ROLE);
      socket.off(UPDATE_ROOM_NAME);
      socket.off(JOIN_ROOM_ERROR);
      socket.off(SOCKET_ERROR);
      socket.off(KICK_USER);
      socket.off(GET_NEW_TASK);
      socket.off(EDIT_TASK);
      socket.off(REMOVE_TASK);
      socket.off(DONE_TASK);
    };
  }, [dispatch, room, router, socket, user.data.username]);

  useEffect(() => {
    if (isMobile) {
      setTimerShow(false);
      setRoomTimerShow(false);
      setRoomNoteShow(false);
      setPopupStackShow('right');
    } else {
      setPopupStackShow('both');
    }
  }, [isMobile]);

  return (
    <SocketContext.Provider value={socket}>
      <Head>
        <title>{`Room ${!!roomName ? ` - ${roomName}` : ''}`}</title>
      </Head>
      <Modal
        centered
        show={changeNameModal.isShowModal}
        onHide={changeNameModal.closeModal}
      >
        <Modal.Header
          closeButton
          {...(theme === 'dark' ? { closeVariant: 'white' } : {})}
        >
          <Modal.Title>Change Room&apos;s Name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <RoomName
            roomName={roomName}
            closeModal={changeNameModal.closeModal}
          />
        </Modal.Body>
      </Modal>
      <Modal
        centered
        show={changePassModal.isShowModal}
        onHide={changePassModal.closeModal}
      >
        <Modal.Header
          closeButton
          {...(theme === 'dark' ? { closeVariant: 'white' } : {})}
        >
          <Modal.Title>Change Room&apos;s Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <RoomPassword closeModal={changePassModal.closeModal} />
        </Modal.Body>
      </Modal>
      <Modal centered show={joinRoomModal.isShowModal} backdrop="static">
        <Modal.Header>
          <Modal.Title>
            You&apos;re joining to &quot;{room?.name}&quot;
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <JoinRoom code={id as string} callback={joinRoomCallback} />
        </Modal.Body>
      </Modal>
      <InviteModal
        isShowModal={inviteModal.isShowModal}
        closeModal={inviteModal.closeModal}
      />
      {!showContent.waiting && (
        <BlankLayout>
          {showContent.main && <RoomBackground />}
          <div
            id="room-bar"
            className="d-flex justify-content-between align-items-center"
          >
            {isMobile ? (
              <div className="buttons-group align-items-center">
                <Dropdown className="feature-dropdown">
                  <Dropdown.Toggle
                    variant="grey"
                    className="d-flex align-items-center"
                  >
                    <RiPencilRuler2Fill />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item onClick={inviteModal.openModal}>
                      <MdPersonAddAlt1 /> Invite
                    </Dropdown.Item>
                    <Dropdown.Item onClick={handleSwitchTimer}>
                      {timerShow ? (
                        <>
                          <RiTimerFill /> Switch to room timer
                        </>
                      ) : (
                        <>
                          <FaUserClock /> Switch to Personal timer
                        </>
                      )}
                    </Dropdown.Item>
                    <Dropdown.Item
                      onClick={() => {
                        handleTogglePopupStack('left');
                        setRoomNoteShow((prev) => !prev);
                      }}
                    >
                      <MdStickyNote2 /> Room note
                    </Dropdown.Item>
                    <Dropdown.Item
                      onClick={() => {
                        handleTogglePopupStack('right');
                        setBackgroundShow((prev) => !prev);
                      }}
                    >
                      <MdImage /> Background
                    </Dropdown.Item>
                    <Dropdown.Item onClick={toggleWhiteboardClick}>
                      <RiArtboardFill />
                      {showContent.whiteboard
                        ? 'Switch to main screen'
                        : 'Switch to whiteboard'}
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            ) : (
              <div className="buttons-group">
                <Button
                  data-tooltip-id="btn-feature-timer"
                  data-tooltip-content={
                    timerShow
                      ? 'Switch to Room Timer'
                      : 'Switch to Personal Timer'
                  }
                  data-tooltip-place="bottom"
                  variant="grey"
                  onClick={handleSwitchTimer}
                >
                  {timerShow ? <RiTimerFill /> : <FaUserClock />}
                </Button>
                <Tooltip id="btn-feature-note" className="py-1 px-2 fs-7" />
                <Button
                  data-tooltip-id="btn-feature-note"
                  data-tooltip-content="Room note"
                  data-tooltip-place="bottom"
                  variant="grey"
                  onClick={() => setRoomNoteShow((prev) => !prev)}
                >
                  <MdStickyNote2 />
                </Button>
                <Tooltip id="btn-feature-timer" className="py-1 px-2 fs-7" />
                <Button
                  variant="grey"
                  data-tooltip-id="btn-feature-background"
                  data-tooltip-content="Background"
                  data-tooltip-place="bottom"
                  onClick={() => setBackgroundShow((prev) => !prev)}
                >
                  <MdImage />
                </Button>
                <Tooltip
                  id="btn-feature-background"
                  className="py-1 px-2 fs-7"
                />
                <Button
                  variant="grey"
                  data-tooltip-id="btn-feature-whiteboard"
                  data-tooltip-content={
                    showContent.whiteboard
                      ? 'Switch to main screen'
                      : 'Switch to whiteboard'
                  }
                  data-tooltip-place="bottom"
                  onClick={toggleWhiteboardClick}
                >
                  <RiArtboardFill />
                </Button>
                <Tooltip
                  id="btn-feature-whiteboard"
                  className="py-1 px-2 fs-7"
                />
              </div>
            )}

            <div className="name-group">
              <h1 className="room-name mx-2 mr-md-3">{roomName}</h1>
              <Button
                variant="secondary d-none d-md-block"
                onClick={inviteModal.openModal}
              >
                Invite
              </Button>
            </div>
            <div className="buttons-group">
              {isMobile ? (
                <Dropdown>
                  <Dropdown.Toggle
                    variant="grey"
                    className="d-flex align-items-center"
                  >
                    <MdSettings />
                  </Dropdown.Toggle>

                  <Dropdown.Menu align="end">
                    {settingAuthorization(role) && (
                      <>
                        <Dropdown.Item
                          onClick={() => changeNameModal.openModal()}
                        >
                          Change Room&apos;s Name
                        </Dropdown.Item>
                        <Dropdown.Item
                          onClick={() => changePassModal.openModal()}
                        >
                          Change Password
                        </Dropdown.Item>
                      </>
                    )}
                    <Dropdown.Item onClick={handleToggleTheme}>
                      {theme === 'light' ? 'Dark theme' : 'Light theme'}
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              ) : (
                <>
                  {settingAuthorization(role) && (
                    <Dropdown>
                      <Dropdown.Toggle
                        variant="grey"
                        className="d-flex align-items-center"
                      >
                        <MdSettings />
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <>
                          <Dropdown.Item
                            onClick={() => changeNameModal.openModal()}
                          >
                            Change Room&apos;s Name
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => changePassModal.openModal()}
                          >
                            Change Password
                          </Dropdown.Item>
                        </>
                      </Dropdown.Menu>
                    </Dropdown>
                  )}
                  <Button variant="grey" onClick={handleToggleTheme}>
                    {theme === 'light' ? <MdDarkMode /> : <MdLightMode />}
                  </Button>
                </>
              )}
              <Button variant="grey" onClick={() => returnToPreviousPage()}>
                <BiExit />
              </Button>
            </div>
          </div>
          <div id="room">
            <Agora
              id={typeof id === 'string' ? id : ''}
              role={role}
              uid={user.data.code}
              show={showContent.main}
            />
            {showContent.whiteboard && (
              <Whiteboard
                id={typeof id === 'string' ? id : ''}
                uuid={whiteboardParams.uuid}
                roomToken={whiteboardParams.roomToken}
              />
            )}

            <div
              className={`popup-stack left ${
                (!showContent.main || popupStackShow === 'right') && 'hide'
              }`}
            >
              <ItemCard show={roomNoteShow} setShow={setRoomNoteShow} header="">
                <RoomNote />
              </ItemCard>
              <ItemCard
                show={timerShow}
                setShow={setTimerShow}
                header="Personal Timer"
              >
                <Timer />
              </ItemCard>
              <ItemCard
                show={roomTimerShow}
                setShow={setRoomTimerShow}
                header="Group Timer"
              >
                <RoomTimer editable={timerAuthorization(role)} />
              </ItemCard>
            </div>
            <div
              className={`popup-stack right ${
                (!showContent.main || popupStackShow === 'left') && 'hide'
              }`}
            >
              <ItemCard
                show={backgroundShow}
                setShow={setBackgroundShow}
                header="Background"
              >
                <BackgroundSetting />
              </ItemCard>
            </div>
            <RoomSideBar />
          </div>
        </BlankLayout>
      )}
      {showContent.waiting && (
        <div className="d-flex justify-content-center align-items-center vh-100">
          <Loading />
        </div>
      )}
    </SocketContext.Provider>
  );
};

export default Room;

export async function getServerSideProps(context: {
  req: { headers: { referer: string | null } };
}) {
  return {
    props: {
      prevPath: context.req.headers.referer || null,
    },
  };
}
