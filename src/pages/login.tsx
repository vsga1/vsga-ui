import Head from 'next/head';
import React, { useState } from 'react';
import AuthenticateLayout from '@/layouts/Authenticate';
import { signIn } from 'next-auth/react';
import Image from 'next/image';
import { Button, Form, InputGroup, Spinner } from 'react-bootstrap';
import { FiEye, FiEyeOff } from 'react-icons/fi';
import Link from 'next/link';
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import schema from '@/common/utils/yupSchema/loginForm';
import { RedirectableProviderType } from 'next-auth/providers';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';

interface FormData {
  username: string;
  password: string;
}

const Login = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<FormData>({
    resolver: yupResolver(schema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });

  const dispatch = useAppDispatch();
  const [isShow, setIsShow] = useState(false);
  const [loading, setLoading] = useState(false);

  const onSubmit: SubmitHandler<FormData> = async (data) => {
    setLoading(true);
    const res = await signIn<RedirectableProviderType>('credentials', {
      redirect: false,
      username: data.username,
      password: data.password,
      callbackUrl: '/',
    });
    setLoading(false);
    if (res?.error) {
      dispatch(
        showNotification({
          msg: res.error,
          status: 'error',
        })
      );
    }
  };

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <AuthenticateLayout>
        <div id="login">
          <div className="header mb-3">
            <h1 className="mb-2">
              Hi !<br />
              Welcome back
            </h1>
            <p>Login to your account</p>
          </div>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="username"
                placeholder="Username"
                {...register('username', {
                  onChange: () => {
                    clearErrors('username');
                  },
                })}
                isInvalid={!!errors.username}
              />
              <Form.Control.Feedback className="error" type="invalid">
                {errors.username?.message}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <InputGroup>
                <Form.Control
                  type={isShow ? 'text' : 'password'}
                  placeholder="Password"
                  {...register('password', {
                    onChange: () => {
                      clearErrors('password');
                    },
                  })}
                  isInvalid={!!errors.password}
                />
                <InputGroup.Text
                  className="toggle-password rounded-end"
                  onClick={() => setIsShow((prev) => !prev)}
                >
                  {isShow ? <FiEyeOff /> : <FiEye />}
                </InputGroup.Text>
                <Form.Control.Feedback className="error" type="invalid">
                  {errors.password?.message}
                </Form.Control.Feedback>
              </InputGroup>
              <Link href="/forgot-password">
                <Form.Text>
                  <u>Forgot Password?</u>
                </Form.Text>
              </Link>
            </Form.Group>
            <Button
              variant="secondary"
              type="submit"
              className="mt-3 w-100 text-uppercase"
              disabled={loading}
            >
              <Spinner
                animation="border"
                size="sm"
                className={!loading ? 'd-none' : 'me-2'}
              />
              Login
            </Button>
          </Form>
          <div className="or w-100 d-flex align-items-center justify-content-between my-3 text-center">
            or login with
          </div>
          <div className="third-party-login d-flex justify-content-center">
            <button
              className="icon outline secondary md"
              onClick={() => signIn('google')}
            >
              <Image
                className="rounded-circle"
                src="/static/icons/google.png"
                alt="facebook"
                width="20"
                height="20"
              />
            </button>
            {/* <button
              className="icon outline secondary md"
              onClick={() => signIn('facebook')}
            >
              <Image
                className="rounded-circle"
                src="/static/icons/facebook.png"
                alt="facebook"
                width="20"
                height="20"
              />
            </button>
            <button
              className="icon outline secondary md"
              onClick={() => signIn('discord')}
            >
              <Image
                className="rounded-circle"
                src="/static/icons/discord.png"
                alt="facebook"
                width="20"
                height="20"
              />
            </button> */}
          </div>
          <div className="link-to mt-4 mt-lg-0">
            <p>
              Don&apos;t have an account? <Link href="/sign-up">Sign up</Link>
            </p>
          </div>
        </div>
      </AuthenticateLayout>
    </>
  );
};

export default Login;
