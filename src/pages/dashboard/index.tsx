import React from 'react';
import { useSession, signOut } from 'next-auth/react';
import { NextPage } from 'next';
import Button from 'react-bootstrap/Button';
import Link from 'next/link';

const Dashboard: NextPage = () => {
  const { data: session } = useSession();

  if (session) {
    return (
      <>
        <Button variant="primary" type="button" onClick={() => signOut()}>
          Sign out
        </Button>
      </>
    );
  } else {
    return <Link href="/sign-up">Sign up</Link>;
  }
};

export default Dashboard;
