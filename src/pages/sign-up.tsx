import React, { useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { signIn } from 'next-auth/react';
import { useForm } from 'react-hook-form';
import { Button, Form, InputGroup, Spinner } from 'react-bootstrap';
import { FiEye, FiEyeOff } from 'react-icons/fi';
import { yupResolver } from '@hookform/resolvers/yup';
import schema from '@/common/utils/yupSchema/signUpForm';
import AuthenticateLayout from '@/layouts/Authenticate';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { postAPI } from '@/common/utils/customAPI';

interface FormData {
  username: string;
  email: string;
  password: string;
}

const SignUp = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<FormData>({
    resolver: yupResolver(schema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });

  const router = useRouter();

  const dispatch = useAppDispatch();
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);

  const toggleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const onSubmit = handleSubmit(async (data) => {
    setLoading(true);
    const res = await postAPI('/v1/account/registration', data);
    if (res.code === 200) {
      router.push('/login');
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    setLoading(false);
  });

  return (
    <>
      <Head>
        <title>Sign up</title>
      </Head>
      <AuthenticateLayout>
        <div id="sign-up">
          <div className="header mb-3">
            <h1 className="mb-2">Sign up</h1>
            <p>Fill your information to create account</p>
          </div>
          <Form className="form" onSubmit={onSubmit}>
            <Form.Group>
              <Form.Label>Username*</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter username"
                {...register('username', {
                  onChange: () => {
                    clearErrors('username');
                  },
                })}
                isInvalid={!!errors.username}
              />
              <Form.Control.Feedback type="invalid" className="error">
                {errors.username?.message}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Email*</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter email"
                {...register('email', {
                  onChange: () => {
                    clearErrors('email');
                  },
                })}
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid" className="error">
                {errors.email?.message}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
              <Form.Label>Password*</Form.Label>
              <InputGroup>
                <Form.Control
                  type={showPassword ? 'text' : 'password'}
                  placeholder="Password"
                  {...register('password', {
                    onChange: () => {
                      clearErrors('password');
                    },
                  })}
                  isInvalid={!!errors.password}
                />
                <InputGroup.Text
                  className="toggle-password rounded-end"
                  onClick={toggleShowPassword}
                >
                  {showPassword ? <FiEyeOff /> : <FiEye />}
                </InputGroup.Text>
                <Form.Control.Feedback type="invalid" className="error">
                  {errors.password?.message}
                </Form.Control.Feedback>
                <Form.Text>
                  At least 8 characters, one uppercase letter, lowercase letter,
                  number and special letter.
                </Form.Text>
              </InputGroup>
            </Form.Group>
            <Button
              variant="secondary"
              type="submit"
              className="text-uppercase w-100 mt-2"
              disabled={loading}
            >
              <Spinner
                animation="border"
                size="sm"
                className={!loading ? 'd-none' : 'me-2'}
              />
              REGISTER
            </Button>
          </Form>
          <div className="or w-100 d-flex align-items-center justify-content-between my-3 text-center">
            or login with
          </div>
          <div className="third-party-login d-flex justify-content-center">
            <button
              className="icon outline secondary md"
              onClick={() => signIn('google')}
            >
              <Image
                className="rounded-circle"
                src="/static/icons/google.png"
                alt="facebook"
                width="20"
                height="20"
              />
            </button>
            {/* <button
              className="icon outline secondary md"
              onClick={() => signIn('facebook')}
            >
              <Image
                className="rounded-circle"
                src="/static/icons/facebook.png"
                alt="facebook"
                width="20"
                height="20"
              />
            </button>
            <button
              className="icon outline secondary md"
              onClick={() => signIn('discord')}
            >
              <Image
                className="rounded-circle"
                src="/static/icons/discord.png"
                alt="facebook"
                width="20"
                height="20"
              />
            </button> */}
          </div>
          <div className="link-to mt-4 mt-lg-0">
            Already have an account?&nbsp;
            <Link href="/login">Login</Link>
          </div>
        </div>
      </AuthenticateLayout>
    </>
  );
};

export default SignUp;
