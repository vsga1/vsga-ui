import 'bootstrap/dist/css/bootstrap.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '@/styles/globals.scss';
import type { AppProps } from 'next/app';
import { store } from '@/store';
import { Provider } from 'react-redux';
import { SessionProvider } from 'next-auth/react';
import RouteGuard from '@/common/components/RouteGuard';
import Notification from '@/common/components/Notification';
import { useRouter } from 'next/router';
import { SSRProvider } from 'react-bootstrap';
import Head from 'next/head';
import '@/common/utils/firebase';

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}: AppProps) {
  const router = useRouter();

  return (
    <SSRProvider>
      <Head>
        <meta
          httpEquiv="Content-Security-Policy"
          content="upgrade-insecure-requests"
        />
      </Head>
      <Provider store={store}>
        <SessionProvider session={session}>
          <Notification />
          <RouteGuard>
            <Component key={router.asPath} {...pageProps} />
          </RouteGuard>
        </SessionProvider>
      </Provider>
    </SSRProvider>
  );
}
