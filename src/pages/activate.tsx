import Loading from '@/common/components/Loading';
import { getAPI } from '@/common/utils/customAPI';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { BsShieldFillCheck, BsShieldFillExclamation } from 'react-icons/bs';

const Activate = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const router = useRouter();

  useEffect(() => {
    setLoading(true);
    async function activateAccount() {
      const token = router.query.token;
      if (token) {
        const res = await getAPI(`/v1/account/activate?token=${token}`);
        if (res.code !== 200) {
          setError(true);
        }
      } else {
        setError(true);
      }
      setLoading(false);
    }
    activateAccount();
  }, [router]);

  const errorBlock = (
    <>
      <div className="icon error">
        <BsShieldFillExclamation />
      </div>
      <h1>Your link is invalid!</h1>
      <p>Please try again later!</p>
      <Link href="/home">
        <Button>Return to home</Button>
      </Link>
    </>
  );

  const successBlock = (
    <>
      <div className="icon success">
        <BsShieldFillCheck />
      </div>
      <h1>Your account has been successfully activated!</h1>
      <p>You can log in to your account and enjoy!</p>
      <Link href="/home">
        <Button>Get started</Button>
      </Link>
    </>
  );
  return (
    <div id="activate">
      <div className="wrapper">
        {loading ? <Loading /> : error ? errorBlock : successBlock}
      </div>
    </div>
  );
};

export default Activate;
