import { useState } from 'react';

interface useSidebarHookProps {
  isShowSidebar: boolean;
  openSidebar: () => void;
  closeSidebar: () => void;
}

export interface useMultipleSidebarHookProps {
  primarySidebar: useSidebarHookProps;
  secondarySidebar: useSidebarHookProps;
}

const useSidebar = () => {
  const [isShowSidebar, setIsShowSidebar] = useState<boolean>(false);

  const openSidebar = () => {
    setIsShowSidebar(true);
  }

  const closeSidebar = () => {
    setIsShowSidebar(false);
  };

  return {
    openSidebar,
    closeSidebar,
    isShowSidebar,
  };
}

export default useSidebar;
