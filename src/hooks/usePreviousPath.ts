/**
 * Can refer from this link
 * https://stackoverflow.com/questions/56857880/how-to-get-history-and-match-in-this-props-in-nextjs
 * I Follow with this but cannot get the exact id from /room/[id]
 */
import { useRef, useEffect } from 'react';
import { useRouter } from 'next/router';
const EXCEPTIONS = ['/login'];

const usePreviousPath = () => {
  const { asPath } = useRouter();

  const ref = useRef<string | null>(null);

  useEffect(() => {
    if (!EXCEPTIONS.includes(asPath)) {
      ref.current = asPath;
    }
  }, [asPath]);

  return ref.current;
};

export default usePreviousPath;
