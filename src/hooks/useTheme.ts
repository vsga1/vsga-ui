import { useEffect } from 'react';
import { selectAppTheme } from '@/store/slice/appSlice';
import { useAppSelector } from '.';

function useTheme() {
  const theme = useAppSelector(selectAppTheme);

  useEffect(() => {
    if (theme === 'dark') {
      document.documentElement.setAttribute('data-theme', 'dark');
      window.localStorage.setItem('theme', 'dark');
    } else {
      document.documentElement.setAttribute('data-theme', 'light');
      window.localStorage.setItem('theme', 'light');
    }
  }, [theme]);

  return theme;
}

export default useTheme;
