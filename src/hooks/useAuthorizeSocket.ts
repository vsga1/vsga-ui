import { useSession } from 'next-auth/react';
import { useEffect, useRef, useState } from 'react';
import { Socket, io } from 'socket.io-client';

function useAuthorizeSocket() {
  const socketRef = useRef<Socket>(
    io(process.env.NEXT_PUBLIC_SOCKET_DOMAIN || '', {
      transports: ['websocket'],
    })
  );
  const [isAuth, setIsAuth] = useState(false);
  const session = useSession();

  useEffect(() => {
    if (session.data?.accessToken) {
      socketRef.current = io(process.env.NEXT_PUBLIC_SOCKET_DOMAIN || '', {
        transports: ['websocket'],
        auth: {
          token: session.data.accessToken,
        },
      });
      setIsAuth(true);
    }
  }, [session.data?.accessToken]);

  return {
    socket: socketRef.current,
    isAuth,
  };
}

export default useAuthorizeSocket;
