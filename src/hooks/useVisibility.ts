import { useState, useEffect, useRef } from 'react';

export default function useVisibility() {
  const [visibilityStatus, setVisibilityStatus] = useState(document.hidden);
  const prevVisibility = useRef<boolean>(document.hidden);

  useEffect(() => {
    function handleVisibilityChange() {
      prevVisibility.current = visibilityStatus;
      setVisibilityStatus(document.hidden);
    }

    document.addEventListener('visibilitychange', handleVisibilityChange);

    return () => {
      document.removeEventListener('visibilitychange', handleVisibilityChange);
    };
  }, [visibilityStatus]);

  return [visibilityStatus, prevVisibility.current];
}
