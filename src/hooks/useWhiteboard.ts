import { whiteboardSdkToken } from '@/common/utils/constants/agora';
import axios from 'axios';
import { useEffect, useState } from 'react';

const useWhiteboard = (whiteboardId?: string) => {
  const id = whiteboardId || '';
  const [roomToken, setRoomToken] = useState('');
  const fetch = async () => {
    const roomToken = await axios.post(
      `https://api.netless.link/v5/tokens/rooms/${whiteboardId}`,
      JSON.stringify({
        lifespan: 3600000,
        role: 'admin',
      }),
      {
        headers: {
          token: whiteboardSdkToken,
          'Content-Type': 'application/json',
          region: 'us-sv',
        },
      }
    );
    setRoomToken(roomToken.data);
  };

  useEffect(() => {
    if (!!whiteboardId) {
      fetch();
    }
  }, [whiteboardId]);

  return {
    uuid: id,
    roomToken,
  };
};

export default useWhiteboard;
