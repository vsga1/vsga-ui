export const appId = `${process.env.NEXT_PUBLIC_AGORA_APP_ID}`;
export const appCertificate = `${process.env.NEXT_PUBLIC_AGORA_PRIMARY_CERTIFICATE}`;
export const tokenExpirationTime = 60 * 60 * 24 * 180;

export const whiteboardAppIdentifier = `${process.env.NEXT_PUBLIC_AGORA_WHITEBOARD_APP_IDENTIFIER}`;
export const whiteboardSdkToken = `${process.env.NEXT_PUBLIC_AGORA_WHITEBOARD_SDK_TOKEN}`;
