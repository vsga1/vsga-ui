export const ROOM_TYPE: Array<{ value: string; label: string }> = [
  { value: 'PUBLIC', label: 'Public' },
  { value: 'PRIVATE', label: 'Private' },
];

export const RoomDefaultBackgrounds = [
  '5wRWniH7rt8',
  '6sBig5kccbE',
  'uDNr-y7xdiQ',
  'cLOP0Kr36ZA',
  'Ab0DhO2gYf0',
  'y-JtsmxGZnU',
  'CiGHT4eS-oU',
  'xNN7iTA57jM',
  'acsLxmnjMho',
];
