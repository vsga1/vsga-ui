export const JOIN_ROOM = 'join room';
export const OUT_ROOM = 'out room';
export const SEND_MESSAGE = 'send message';
export const RECEIVE_MESSAGE = 'receive message';

export const ROOM_END = 'room end';
export const ROOM_DATA = 'room data';
export const UPDATE_ROLE = 'update role';
export const UPDATE_ROOM_NAME = 'update room name';
export const KICK_USER = 'kick user';

export const CREATE_TASK = 'create task';
export const EDIT_TASK = 'edit task';
export const REMOVE_TASK = 'remove task';
export const DONE_TASK = 'done task';
export const GET_NEW_TASK = 'get new task';

export const UPDATE_RESOURCE = 'update resource';

export const SET_TIMER = 'set timer';
export const PAUSE_TIMER = 'pause timer';
export const STOP_TIMER = 'stop timer';

export const OPEN_EDIT_NOTE = 'open edit note';
export const CLOSE_EDIT_NOTE = 'close edit note';

export const SOCKET_ERROR = 'error';
export const JOIN_ROOM_ERROR = 'join room error';
