export const EMAIL = /^[\w-.]+@([\w-]+.)+[\w-]{2,4}$/;
export const PASSWORD =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
export const YOUTUBE =
  /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
export const GOOGLE_DOCS =
  /https:\/\/docs\.google\.com\/document\/d\/(.*?)\/.*?/;
