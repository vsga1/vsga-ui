export const authPaths = [
  '/login',
  '/sign-up',
  '/forgot-password',
  '/reset-password',
  '/activate',
];

export const publicPaths = ['/'];

export const STATIC_IMAGES_PATH = '/static/images/';
export const DEFAULT_AVATAR_PATH = STATIC_IMAGES_PATH + 'default_avatar.jpg';
