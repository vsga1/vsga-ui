export default function formatNumber(num: number, digit = 1) {
  return num.toLocaleString('en-US', {
    minimumIntegerDigits: digit,
  });
}
