import * as yup from 'yup';

const schema = yup.object({
  current_password: yup.string(),
  new_password: yup.string(),
});

export default schema;
