import * as yup from 'yup';

const schema = yup.object({
  name: yup
    .string()
    .required('Please enter the room\'s name.')
    .max(60, 'Room\'s name must not be longer than 60 characters.'),
});

export default schema;
