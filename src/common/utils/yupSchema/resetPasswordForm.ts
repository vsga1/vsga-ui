import * as yup from 'yup';
import { REGEX } from '../constants';
const schema = yup
  .object({
    password: yup
      .string()
      .required('Please enter your password.')
      .matches(REGEX.PASSWORD, 'Password is invalid.'),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref('password'), null], 'Password does not match'),
  })
  .required();

export default schema;
