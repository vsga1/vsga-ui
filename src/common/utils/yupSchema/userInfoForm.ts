import * as yup from 'yup';

const schema = yup.object({
  full_name: yup.string().nullable(),
  address: yup.string().nullable(),
  gender: yup.string().nullable(),
  description: yup.string().nullable(),
  display_name: yup.string().nullable(),
  age: yup
    .number()
    .typeError('Age must be a number.')
    .positive('Age must be positive.')
    .max(100, 'Age must not be bigger than 100.')
    .nullable(),
});

export default schema;
