import * as yup from 'yup';
import { REGEX } from '../constants';

const schema = yup.object({
  current_password: yup.string().required('Required!'),
  new_password: yup
    .string()
    .required('Required!')
    .matches(REGEX.PASSWORD, 'New password wrong format!'),
});

export default schema;
