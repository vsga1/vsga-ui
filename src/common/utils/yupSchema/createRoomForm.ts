import * as yup from 'yup';

const schema = yup.object({
  name: yup
    .string()
    .required('Please enter the room\'s name.')
    .max(60, 'Room\'s name must not be longer than 60 characters.'),
  description: yup
    .string()
    .max(120, 'Description must not be longer than 120 characters.'),
  capacity: yup
    .number()
    .required('Please set the capacity.')
    .typeError('Please enter a number.')
    .min(2, 'The room must have at least 2 users at a time.')
    .max(10, 'The room must have at most 10 users at a time.'),
  password: yup.string(),
  room_type: yup.string().required(),
  topics: yup.array(),
});

export default schema;
