import * as yup from 'yup';
import { REGEX } from '../constants';

const schema = yup.object({
  username: yup.string().required('Required!'),
  email: yup
    .string()
    .required('Required!')
    .matches(REGEX.EMAIL, 'Email wrong format!'),
  password: yup
    .string()
    .required('Required!')
    .matches(REGEX.PASSWORD, 'Password wrong format!'),
});

export default schema;
