import * as yup from 'yup';
import { REGEX } from '../constants';

const schema = yup.object({
  email: yup
    .string()
    .required('Required!')
    .matches(REGEX.EMAIL, 'Email wrong format!'),
});

export default schema;
