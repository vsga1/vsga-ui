import { object, string } from 'yup';

const schema = object({
  password: string().required('Required!'),
});

export default schema;
