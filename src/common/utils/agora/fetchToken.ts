import { RtcTokenBuilder, RtcRole } from 'agora-token';
import { AGORA } from '../constants';
export const fetchToken = async (
  channel: string,
  userRole: string,
  uid: string | number
) => {
  const appId = AGORA.appId;
  const appCertificate = AGORA.appCertificate;
  const role = userRole === 'host' ? RtcRole.PUBLISHER : RtcRole.SUBSCRIBER;
  const expirationTime = AGORA.tokenExpirationTime;
  const currentTimestamp = Math.floor(Date.now() / 1000);
  const privilegeExpiredTs = currentTimestamp + expirationTime;
  return RtcTokenBuilder.buildTokenWithUid(
    appId,
    appCertificate,
    channel,
    uid,
    role,
    expirationTime,
    privilegeExpiredTs
  );
};
