import { Role } from './enums';

export const settingAuthorization = (role?: Role) => {
  if (!role) {
    return false;
  }
  if (role !== Role.MEMBER) {
    return true;
  }
  return false;
};

export const taskAuthorization = (role?: Role) => {
  if (!role) {
    return false;
  }
  if (role !== Role.MEMBER) {
    return true;
  }
  return false;
};

export const timerAuthorization = (role?: Role) => {
  if (!role) {
    return false;
  }
  if (role !== Role.MEMBER) {
    return true;
  }
  return false;
};

export const canPromote = (sourceRole?: Role, targetRole?: Role) => {
  if (!sourceRole || !targetRole) {
    return false;
  }
  if (sourceRole === Role.MEMBER) {
    return false;
  }
  if (targetRole === Role.MEMBER) {
    return true;
  }
  return false;
};

export const canDemote = (sourceRole?: Role, targetRole?: Role) => {
  if (!sourceRole || !targetRole) {
    return false;
  }
  if (sourceRole === Role.MEMBER || sourceRole === Role.CO_HOST) {
    return false;
  }
  if (targetRole === Role.CO_HOST) {
    return true;
  }
  return false;
};

export const canKick = (sourceRole?: Role, targetRole?: Role) => {
  if (!sourceRole || !targetRole) {
    return false;
  }
  if (sourceRole === Role.MEMBER) {
    return false;
  }
  if (sourceRole === Role.CO_HOST) {
    if (targetRole === Role.MEMBER) {
      return true;
    } else {
      return false;
    }
  } else {
    if (targetRole === Role.HOST) {
      return false;
    } else {
      return true;
    }
  }
};
