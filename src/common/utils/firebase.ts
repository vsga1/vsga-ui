import { initializeApp, getApps, FirebaseApp } from 'firebase/app';
import { getMessaging, getToken, Messaging } from 'firebase/messaging';
import { FIREBASE_VAR } from './constants';
import { postAPI } from './customAPI';

const {
  apiKey,
  authDomain,
  projectId,
  storageBucket,
  messagingSenderId,
  appId,
  measurementId,
  vapidKey,
} = FIREBASE_VAR;

const firebaseConfig = {
  apiKey,
  authDomain,
  projectId,
  storageBucket,
  messagingSenderId,
  appId,
  measurementId,
};

let app: FirebaseApp;
let messaging: Messaging;

function requestPermission(messaging: Messaging) {
  if(!messaging) return;
  Notification.requestPermission().then((permission) => {
    if (permission === 'granted') {
      getToken(messaging, { vapidKey }).then((currentToken) => {
        if (currentToken) {
          try {
            postAPI(`/v1/account/device-token?token=${currentToken}`, {});
          } catch (err) {}
        }
      });
    }
  });
}

function iOS() {
  if (typeof window === 'undefined' || typeof navigator === 'undefined')
    return false;

  return /iPad|iPhone|iPod/.test(navigator.userAgent);
}

if (typeof window !== 'undefined' && !getApps().length && !iOS()) {
  app = initializeApp(firebaseConfig);
  messaging = getMessaging(app);
}

export { messaging, requestPermission };
