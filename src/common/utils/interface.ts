import { NotificationStatus, NotificationType, TaskType } from './enums';

export interface Room {
  code: string;
  capacity: number;
  name: string;
  room_type: 'PUBLIC' | 'PRIVATE';
  description: string | null;
  total_member: number;
  is_favored: boolean;
  host_avatar_url: string;
  topics: TopicResponse[];
  configuration: {
    background_url: string | null;
    is_protected?: boolean;
  };
  whiteboard_id: string;
}

export interface TaskResponse {
  code: string;
  description: string;
  room_code: string;
  scheme: string | null;
  status: 'DONE' | 'UNDONE' | 'DELETED';
  type: TaskType;
  users: [];
}

export interface Topic {
  value: string;
  label: string;
  color: string;
}

export interface TopicResponse {
  name: string;
  color: string;
}

export interface INotification {
  id?: string;
  title?: string;
  body?: string;
  data?: {
    noti_id: string;
    type: NotificationType;
    time: string;
    senderImage: string;
    username: string;
    code?: string;
  };
  status?: NotificationStatus;
  sender?: {
    username: string;
    avatar_img_url: string;
  };
}
