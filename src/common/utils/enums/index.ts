export enum TaskStatus {
  DONE = 'DONE',
  UNDONE = 'UNDONE',
  DELETED = 'DELETED',
}

export enum TaskType {
  PERSONAL = 'PERSONAL_TASK',
  ROOM = 'ROOM_TASK',
}

export enum RoomType {
  'PUBLIC',
  'PRIVATE',
}

export enum TimerStatus {
  IDLE = 'IDLE',
  RUN = 'RUN',
  PAUSE = 'PAUSE',
}

export enum ChatType {
  ALL = 'ALL',
  DIRECTIVE = 'DIRECTIVE',
}

export enum Role {
  HOST = 'HOST',
  CO_HOST = 'CO_HOST',
  MEMBER = 'MEMBER',
}

export enum NotificationType {
  DEFAULT = 'DEFAULT',
  FRIEND_REQUEST = 'FRIEND_REQUEST',
  JOIN_ROOM_REQUEST = 'JOIN_ROOM_REQUEST',
}

export enum NotificationStatus {
  READ = 'READ',
  UNREAD = 'UNREAD',
}
