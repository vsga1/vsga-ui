import axios, { AxiosHeaders } from 'axios';
import { getSession } from 'next-auth/react';

const axiosInstance = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_DOMAIN}/vstudy`,
});

axiosInstance.interceptors.request.use(async (config) => {
  const session = await getSession();
  if (session && session?.accessToken) {
    (config.headers as AxiosHeaders).set(
      'Authorization',
      `Bearer ${session.accessToken}`
    );
    (config.headers as AxiosHeaders).set('Content-Type', 'application/json');
  }
  return config;
});

export default axiosInstance;
