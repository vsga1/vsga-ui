import { REGEX } from './constants';

export default function youtubeParser(url: string) {
  const match = url.match(REGEX.YOUTUBE);
  const idPosition = 7,
    idLength =11;
  return match && match[idPosition].length == idLength ? match[idPosition] : false;
}
