import axiosInstance from './customAxios';
import { MESSAGE } from './constants';
import axios from 'axios';
import { getSession } from 'next-auth/react';

export const getAPI = async (url: string, params?: unknown) => {
  return await axiosInstance
    .get(url, { params })
    .then((res) => res.data)
    .then((res) => {
      const { meta, data } = res;
      const resData = data || meta.message;
      return {
        meta,
        code: meta.code as number,
        data: resData,
      };
    })
    .catch((error) => {
      return {
        meta: error.response?.data?.meta,
        code: error.response?.data?.meta?.code as number,
        data: error.response?.data?.meta?.message || MESSAGE.SERVER_ERROR,
      };
    });
};

export const postAPI = async (url: string, data: unknown) => {
  return await axiosInstance
    .post(url, data)
    .then((res) => res.data)
    .then((res) => {
      const { meta, data } = res;
      const resData = data || meta.message;
      return {
        meta,
        code: meta.code as number,
        data: resData,
      };
    })
    .catch((error) => {
      return {
        meta: error.response?.data?.meta,
        code: error.response?.data?.meta?.code as number,
        data: error.response?.data?.meta?.message || MESSAGE.SERVER_ERROR,
      };
    });
};

export const putAPI = async (url: string, data: unknown) => {
  return await axiosInstance
    .put(url, data)
    .then((res) => res.data)
    .then((res) => {
      const { meta, data } = res;
      const resData = data || meta.message;
      return {
        meta,
        code: meta.code as number,
        data: resData,
      };
    })
    .catch((error) => {
      return {
        meta: error.response?.data?.meta,
        code: error.response?.data?.meta?.code as number,
        data: error.response?.data?.meta?.message || MESSAGE.SERVER_ERROR,
      };
    });
};

export const deleteAPI = async (url: string) => {
  return await axiosInstance
    .delete(url)
    .then((res) => res.data)
    .then((res) => {
      const { meta, data } = res;
      const resData = data || meta.message;
      return {
        code: meta.code as number,
        data: resData,
      };
    })
    .catch((error) => {
      return {
        code: error?.response?.data?.meta?.code as number,
        data: error?.response?.data?.meta?.message || MESSAGE.SERVER_ERROR,
      };
    });
};

export const uploadFileAPI = async (url: string, formData: unknown) => {
  const session = await getSession();
  if (!session || !session.accessToken) {
    return {
      code: 401,
      data: 'Not authorized',
    };
  }
  return await axios
    .post(`${process.env.NEXT_PUBLIC_API_DOMAIN}/vstudy${url}`, formData, {
      headers: {
        Authorization: `Bearer ${session.accessToken}`,
        'Content-Type': 'multipart/form-data',
      },
    })
    .then((res) => res.data)
    .then((res) => {
      const { meta, data } = res;
      const resData = data || meta.message;
      return {
        code: meta.code as number,
        data: resData,
      };
    })
    .catch((error) => {
      return {
        code: error?.response?.data?.meta?.code as number,
        data: error?.response?.data?.meta?.message || MESSAGE.SERVER_ERROR,
      };
    });
};

export const cloudinaryUpload = async (data: FormData) => {
  return await axios
    .post('https://api.cloudinary.com/v1_1/huydoan1908/image/upload', data)
    .then((res) => res.data)
    .then((res) => {
      return {
        code: 200,
        data: res,
      };
    })
    .catch((error) => {
      return {
        code: error?.response?.status as number,
        data: error?.response?.data?.error?.message || MESSAGE.SERVER_ERROR,
      };
    });
};
