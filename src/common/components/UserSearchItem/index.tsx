import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import { Info } from '@/store/slice/userSlice';
import React, { useState } from 'react';
import { Button, ButtonGroup, Dropdown, DropdownButton } from 'react-bootstrap';
import { BsCheckLg, BsFillPersonPlusFill } from 'react-icons/bs';
import ConfirmationModal from '../ConfirmationModal';
import useModal from '@/hooks/useModal';
import useTheme from '@/hooks/useTheme';

interface UserSearchItemProps {
  user: Info;
  isCurrentUser?: boolean;
  handleUnfriend: (username: string) => void;
  handleAddFriend: (username: string) => void;
}

const UserSearchItem: React.FC<UserSearchItemProps> = ({
  user,
  isCurrentUser = false,
  handleUnfriend,
  handleAddFriend,
}) => {
  const [avatar, setAvatar] = useState<string>(user.avatar_img_url);
  const confirmationUnfriendModal = useModal();
  const theme = useTheme();

  return (
    <div className="user user-search-item border rounded-4 h-100">
      <div className="d-flex algin-items-flex-start align-items-sm-center w-100 gap-3 flex-column flex-sm-row">
        <div className="d-flex align-items-center gap-2 flex-grow-1 overflow-hidden">
          <div className="avatar avatar-medium">
            <img
              src={avatar ? avatar : DEFAULT_AVATAR_PATH}
              alt={user.display_name || user.username}
              onError={() => setAvatar(DEFAULT_AVATAR_PATH)}
            />
          </div>
          <div className="flex-grow-1 overflow-hidden w-0">
            <h5 className="text-truncate text-primary fw-bold mb-0">
              {user.display_name || user.username}
            </h5>
            <p className="text-truncate text-primary mb-1">{user.username}</p>
            <p
              className={`text-truncate ${
                theme === 'light' ? 'text-dark' : 'text-light'
              }`}
            >
              {user.description}
            </p>
          </div>
        </div>
        <div className="flex-shrink-0">
          {!isCurrentUser && (
            <div>
              {user.is_friend ? (
                <DropdownButton
                  align={{ sm: 'end' }}
                  size="sm"
                  as={ButtonGroup}
                  variant="secondary"
                  title={
                    <>
                      <BsCheckLg className="me-2" />
                      <strong>Friends</strong>
                    </>
                  }
                >
                  <Dropdown.Item
                    onClick={() => confirmationUnfriendModal.openModal()}
                  >
                    Unfriend
                  </Dropdown.Item>
                </DropdownButton>
              ) : (
                <Button
                  size="sm"
                  variant="secondary"
                  className="d-flex align-items-center"
                  onClick={() => {
                    handleAddFriend(user.username);
                  }}
                >
                  <BsFillPersonPlusFill className="me-2" />
                  <strong>Add Friend</strong>
                </Button>
              )}
              <ConfirmationModal
                title={`Unfriend ${user.display_name || user.username}`}
                confirmationText="Remove"
                isShowModal={confirmationUnfriendModal.isShowModal}
                closeModal={confirmationUnfriendModal.closeModal}
                handleConfirm={() => {
                  handleUnfriend(user.username);
                  confirmationUnfriendModal.closeModal();
                }}
              >
                Are you sure you want to remove{' '}
                <strong className="text-primary">
                  {user.display_name || user.username}{' '}
                </strong>
                from the list?
              </ConfirmationModal>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default UserSearchItem;
