import React, { useEffect, useRef, useState } from 'react';
import {
  Fastboard,
  createFastboard,
  apps,
  FastboardApp,
} from '@netless/fastboard-react';
import { whiteboardAppIdentifier } from '@/common/utils/constants/agora';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import GoogleDocSrcModal from './GoogleDocsSrcModal';
import useModal from '@/hooks/useModal';
import { ICON_LINK } from '@/common/utils/constants';

type Props = {
  id: string;
  uuid: string;
  roomToken: string;
};

const Whiteboard = ({ id, uuid, roomToken }: Props) => {
  const dispatch = useAppDispatch();
  const sdkConfig = {
    appIdentifier: whiteboardAppIdentifier,
    region: 'us-sv',
  };
  const [app, setApp] = useState<FastboardApp | null>(null);
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const screenshotRef = useRef<HTMLAnchorElement | null>(null);

  const showGoogleDocModal = useModal();

  const submitGoogleDocSrc = (src: string) => {
    if (!app) {
      return;
    }
    app.manager.addApp({
      kind: 'EmbeddedPage',
      options: { title: 'Google Docs' },
      attributes: {
        src,
      },
    });
    showGoogleDocModal.closeModal();
  };

  const screenShot = (app: FastboardApp) => {
    const view = app.manager.mainView;
    const canvas = canvasRef.current;
    if (!canvas) {
      return;
    }
    canvas.width = view.size.width;
    canvas.height = view.size.height;
    const c = canvas.getContext('2d');
    if (!c) {
      dispatch(
        showNotification({
          msg: 'Cannot get photo from whiteboard!',
          status: 'error',
        })
      );
      return;
    }
    view.screenshotToCanvas(
      c,
      view.focusScenePath || '/init',
      view.size.width,
      view.size.height,
      view.camera
    );
    try {
      canvas.toBlob((blob) => {
        if (!blob) {
          dispatch(
            showNotification({
              msg: 'Cannot get photo from whiteboard!',
              status: 'error',
            })
          );
          return;
        }
        if (screenshotRef.current) {
          screenshotRef.current!.href = URL.createObjectURL(blob);
          screenshotRef.current.click();
        }
      });
    } catch (err) {
      dispatch(
        showNotification({
          msg: 'Cannot get photo from whiteboard!',
          status: 'error',
        })
      );
    }
  };
  useEffect(() => {
    if (!roomToken) {
      return;
    }
    let appInstance: FastboardApp;

    if (apps.length < 5) {
      apps.push(
        {
          icon: ICON_LINK.googleDocs,
          kind: 'EmbeddedPage',
          label: 'Google Docs',
          onClick() {
            showGoogleDocModal.openModal();
          },
        },
        {
          icon: ICON_LINK.screenshot,
          kind: 'Snapshot',
          label: 'Screenshot',
          onClick: screenShot,
        }
      );
    }

    createFastboard({
      sdkConfig,
      joinRoom: {
        uid: id,
        uuid,
        roomToken,
      },
    }).then((app) => {
      setApp((appInstance = app));
    });

    return () => {
      if (appInstance) {
        appInstance.destroy();
      }
    };
  }, [roomToken]);

  return (
    <>
      <div className="hide ">
        <canvas ref={canvasRef} />
        <a download="screenshot.png" ref={screenshotRef} />
      </div>
      <GoogleDocSrcModal
        show={showGoogleDocModal.isShowModal}
        closeModal={showGoogleDocModal.closeModal}
        onSubmit={submitGoogleDocSrc}
      />
      <Fastboard app={app} />
    </>
  );
};

export default Whiteboard;
