import { REGEX } from '@/common/utils/constants';
import useTheme from '@/hooks/useTheme';
import React, { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';

type Props = {
  show: boolean;
  closeModal: () => void;
  onSubmit: (src: string) => void;
};

const GoogleDocsSrcModal = ({ show, closeModal, onSubmit }: Props) => {
  const [src, setSrc] = useState('');
  const [error, setError] = useState('');
  const theme = useTheme();

  const onClick = () => {
    if (!REGEX.GOOGLE_DOCS.test(src)) {
      setError('Wrong google docs link format!');
      return;
    }
    onSubmit(src);
    setSrc('');
    setError('');
  };

  return (
    <Modal centered show={show} onHide={closeModal}>
      <Modal.Header closeButton {...(theme === 'dark' ? {closeVariant: 'white'} : {})}>
        <Modal.Title>Google doc source</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Control
          type="text"
          value={src}
          onChange={(e) => {
            setSrc(e.target.value);
            setError('');
          }}
          placeholder="Source"
        />
        <div className="text-danger">
          <small>{error}</small>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button className="btn-primary text-white" onClick={onClick}>
          OK
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default GoogleDocsSrcModal;
