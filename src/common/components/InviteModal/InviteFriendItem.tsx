import { postAPI } from '@/common/utils/customAPI';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { selectRoomInfo } from '@/store/slice/roomSlice';
import { selectUserInfo } from '@/store/slice/userSlice';
import { debounce } from 'lodash';
import React, { useState } from 'react';
import { Button, Spinner } from 'react-bootstrap';

type Props = {
  item: {
    avatar: string;
    username: string;
  };
};

const InviteFriendItem = ({ item }: Props) => {
  const dispatch = useAppDispatch();
  const userInfo = useAppSelector(selectUserInfo);
  const roomInfo = useAppSelector(selectRoomInfo);
  const [status, setStatus] = useState<'DEFAULT' | 'INPROGRESS' | 'DONE'>(
    'DEFAULT'
  );

  const handleInviteRoom = debounce(async (username: string) => {
    if (status !== 'DEFAULT') return;
    setStatus('INPROGRESS');
    const res = await postAPI('/v1/notification/invite', {
      title: 'Invite',
      body: `${userInfo.username} invite you to join ${roomInfo?.name}`,
      data: {
        type: 'JOIN_ROOM_REQUEST',
        time: new Date().toISOString(),
        senderImage: userInfo.avatar_img_url || null,
        username: userInfo.username,
        code: roomInfo?.code,
      },
      username,
    });
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      setStatus('DEFAULT');
    } else {
      setStatus('DONE');
    }
  }, 500);

  return (
    <div className="item d-flex align-items-center justify-content-between w-100 mb-3">
      <div className="friend-info d-flex align-items-center">
        <img
          className="avatar rounded-circle mr-1"
          src={item.avatar}
          alt="friend-avatar"
        />
        <p className="ms-2 text-theme">{item.username}</p>
      </div>
      {status === 'DONE' ? (
        <p className="mr-3 text-theme">Sent</p>
      ) : (
        <Button
          variant="secondary"
          onClick={() => {
            handleInviteRoom(item.username);
          }}
          className="py-1"
          disabled={status === 'INPROGRESS'}
        >
          {status === 'DEFAULT' ? (
            'Invite'
          ) : (
            <Spinner size="sm" animation="border" />
          )}
        </Button>
      )}
    </div>
  );
};

export default InviteFriendItem;
