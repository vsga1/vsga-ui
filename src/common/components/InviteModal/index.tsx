import useTheme from '@/hooks/useTheme';
import React from 'react';
import { Modal } from 'react-bootstrap';
import InviteFriends from './InviteFriends';
import InviteLink from './InviteLink';

type Props = {
  isShowModal: boolean;
  closeModal: () => void;
};

const InviteModal: React.FC<Props> = ({ isShowModal, closeModal }) => {
  const theme = useTheme();
  return (
    <Modal centered show={isShowModal} onHide={closeModal} scrollable>
      <Modal.Header
        closeButton
        {...(theme === 'dark' ? { closeVariant: 'white' } : {})}
      >
        <Modal.Title>Invite friends</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <InviteFriends />
      </Modal.Body>
      <Modal.Footer>
        <InviteLink />
      </Modal.Footer>
    </Modal>
  );
};

export default InviteModal;
