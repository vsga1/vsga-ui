import { getAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Dropdown, Form, InputGroup } from 'react-bootstrap';
import { MdSearchOff } from 'react-icons/md';
import { useDebounce } from 'usehooks-ts';
import Loading from '../Loading';
import InviteFriendItem from './InviteFriendItem';

interface IFriend {
  avatar: string;
  username: string;
}

const InviteFriends = () => {
  const dispatch = useAppDispatch();

  const [searchValue, setSearchValue] = useState<string>('');
  const [friendList, setFriendList] = useState<Array<IFriend>>([]);
  const [displayFriends, setDisplayFriends] = useState<Array<IFriend>>([]);
  const [isLoading, setIsLoading] = useState(true);
  const debounceSearchValue = useDebounce<string>(searchValue, 500);

  const handleChangeSearchValue = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
  };

  const getFriends = async () => {
    setIsLoading(true);
    const res = await getAPI('/v1/friend/list');
    if (res.code === 200) {
      const list = res.data.map(
        (item: { avatar_img_url: string; username: string }) => ({
          avatar: item.avatar_img_url,
          username: item.username,
        })
      );
      setFriendList(list);
      setDisplayFriends(list);
    } else {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
    }
    setIsLoading(false);
  };

  useEffect(() => {
    const list = friendList.filter((item) =>
      item.username.includes(debounceSearchValue)
    );
    setDisplayFriends(list);
  }, [debounceSearchValue]);

  useEffect(() => {
    getFriends();
  }, []);

  return (
    <div className="invite-friends">
      <InputGroup className="mb-4">
        <Form.Control placeholder='Search friend...' value={searchValue} onChange={handleChangeSearchValue} />
      </InputGroup>
      <Dropdown.Divider />
      <div className="list-friend w-100">
        {isLoading ? (
          <div className="d-flex w-100 justify-content-center">
            <Loading />
          </div>
        ) : displayFriends.length > 0 ? (
          displayFriends.map((item) => {
            return <InviteFriendItem item={item} key={item.username} />;
          })
        ) : (
          <h5 className="text-center text-theme">
            <MdSearchOff />
            No friends found.
          </h5>
        )}
      </div>
    </div>
  );
};

export default InviteFriends;
