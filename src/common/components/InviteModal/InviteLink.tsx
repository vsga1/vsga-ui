import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';

const InviteLink = () => {
  const router = useRouter();
  const link = `${process.env.NEXT_PUBLIC_APP_URL}${router.asPath}`;
  const [btnValue, setBtnValue] = useState<'Copy' | 'Copied'>('Copy');
  const handleCopy = () => {
    navigator.clipboard.writeText(link);
    setBtnValue('Copied');
  };

  const changeBtnValue = () => {
    if (btnValue !== 'Copy') {
      setBtnValue('Copy');
    }
  };

  useEffect(() => {
    const timeout = setTimeout(changeBtnValue, 1000);
    return () => {
      clearTimeout(timeout);
    };
  }, [btnValue]);

  return (
    <div className="invite-link w-100">
      <p className="text-theme mb-2">Or send a room invite link to friends:</p>
      <InputGroup>
        <Form.Control readOnly value={link} />
        <Button
          variant={btnValue === 'Copy' ? 'secondary' : 'success'}
          onClick={handleCopy}
        >
          {btnValue}
        </Button>
      </InputGroup>
    </div>
  );
};

export default InviteLink;
