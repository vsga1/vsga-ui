/* eslint-disable indent */
import { RECEIVE_MESSAGE } from '@/common/utils/constants/event';
import { SocketContext } from '@/context/socket';
import { useAppSelector } from '@/hooks';
import { addMessage } from '@/store/slice/messageSlice';
import { selectUser } from '@/store/slice/userSlice';
import React, { useContext, useEffect, useState } from 'react';
import { Badge, Button } from 'react-bootstrap';
import {
  MdTask,
  MdNoteAlt,
  MdOutlineClose,
  MdChatBubble,
  MdPeopleAlt,
  MdCollectionsBookmark,
} from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { Tooltip } from 'react-tooltip';
import 'react-tooltip/dist/react-tooltip.css';
import Chat from '../Chat';
import Task from '../Task';
import UsersInRoom from '../UsersInRoom';
import Note from '../Note';
import { ChatType } from '@/common/utils/enums';
import Resource from '../Resource';

const features = [
  {
    id: 1,
    name: 'Task',
    icon: <MdTask />,
  },
  {
    id: 2,
    name: 'Note',
    icon: <MdNoteAlt />,
  },
  {
    id: 3,
    name: 'Chat',
    icon: <MdChatBubble />,
  },
  {
    id: 4,
    name: 'Resource',
    icon: <MdCollectionsBookmark />,
  },
  {
    id: 5,
    name: 'Users',
    icon: <MdPeopleAlt />,
  },
];

const Feature = ({ id }: { id: number }) => {
  switch (id) {
    case 1:
      return <Task />;
    case 2:
      return <Note />;
    case 3:
      return <Chat />;
    case 4:
      return <Resource />;
    case 5:
      return <UsersInRoom />;
    default:
      return <></>;
  }
};

const RoomSideBar: React.FC = () => {
  const socket = useContext(SocketContext);
  const [open, setOpen] = useState<boolean>(false);
  const [featureId, setFeatureId] = useState<number>(0);
  const [chatNotificationCount, setChatNotificationCount] = useState<number>(0);
  const dispatch = useDispatch();
  const user = useAppSelector(selectUser);

  const openSidebar = () => {
    setOpen(true);
  };
  const closeSidebar = () => {
    setOpen(false);
    setFeatureId(0);
  };
  const setFeatureComponent = (id: number) => {
    openSidebar();
    setFeatureId(id);
  };

  useEffect(() => {
    socket.on(
      RECEIVE_MESSAGE,
      ({
        sender,
        text,
        receiver,
        type,
      }: {
        sender: string;
        text: string;
        receiver: string;
        type: ChatType;
      }) => {
        dispatch(addMessage({ text, sender, receiver, type }));
        if (!open) {
          setChatNotificationCount((prev) => prev + 1);
        }
      }
    );

    if (open) {
      setChatNotificationCount(0);
    }

    return () => {
      socket.off(RECEIVE_MESSAGE);
    };
  }, [user.data.username, open]);

  return (
    <div id="room-side-bar" className={`${open && 'open'}`}>
      <div className="layer" onClick={closeSidebar}></div>
      <div className="features d-flex align-items-center justify-content-start flex-column">
        {features.map((item) => {
          return (
            <div key={item.id} className="position-relative">
              <Button
                data-tooltip-id={`btn-feature-${item.id}`}
                data-tooltip-content={item.name}
                data-tooltip-place="left"
                onClick={() => setFeatureComponent(item.id)}
                className={`circle feature-btn ${
                  item.id === featureId ? 'active' : ''
                }`}
              >
                {item.icon}
              </Button>
              <Tooltip id={`btn-feature-${item.id}`} className='d-none d-md-block'/>
              {item.name === 'Chat' && chatNotificationCount > 0 && (
                <Badge
                  pill
                  bg="primary"
                  className="position-absolute bottom-50 start-50"
                >
                  {chatNotificationCount}
                </Badge>
              )}
            </div>
          );
        })}
        {featureId !== 0 && (
          <div
            className="indicator"
            style={{ top: `${featureId * 50 - 40}px` } /* 50 = Button height + Gap; 40 = 50 -10 : D */}
          ></div>
        )}
      </div>
      <div className="feature-component">
        <div className="header p-2 align-items-center justify-content-between">
          {features.find((item) => item.id === featureId)?.name}
          <Button className="icon circle" onClick={closeSidebar}>
            <MdOutlineClose />
          </Button>
        </div>
        <Feature id={featureId} />
      </div>
    </div>
  );
};

export default RoomSideBar;
