import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import useModal from '@/hooks/useModal';
import useTheme from '@/hooks/useTheme';
import { Info } from '@/store/slice/userSlice';
import React, { ForwardedRef, forwardRef, useState } from 'react';
import { Dropdown, Toast } from 'react-bootstrap';
import { BsCheckLg } from 'react-icons/bs';
import ConfirmationModal from '../ConfirmationModal';

interface UserCardProps {
  user: Info;
  isShow: boolean;
  setIsShow: (isShow: boolean) => void;
  handleUnfriend: (username: string) => void;
}

const UserCard = forwardRef<HTMLDivElement | null, UserCardProps>(
  (
    { user, isShow, setIsShow, handleUnfriend }, 
    ref: ForwardedRef<HTMLDivElement | null>
  ) => {
    const [avatar, setAvatar] = useState(user.avatar_img_url);
    const theme = useTheme();
    const confirmationUnfriendModal = useModal();

    return (
      <Toast 
        ref={ref} 
        show={isShow} 
        className="user-card position-absolute" 
        onClose={(e) => {
          e?.stopPropagation();
          setIsShow(false);
        }}
      >
        <Toast.Header closeButton={false} className="pb-2">
          <div className="avatar avatar-medium mr-3">
            <img
              src={avatar ? avatar : DEFAULT_AVATAR_PATH}
              alt={user.full_name || user.username}
              onError={() => setAvatar(DEFAULT_AVATAR_PATH)}
            />
          </div>
          <div>
            <p className="text-primary fw-bold text-wrap text-break fs-4">{user.display_name || user.username}</p>
            <small className={`${theme === 'light' ? 'text-dark' : 'text-light'} text-wrap text-break`}>
              {user.username}
            </small>
          </div>
        </Toast.Header>
        <Toast.Body>
          {
            user.full_name && (
              <div className="mb-2">
                <p className="text-primary fw-bold fs-5">Fullname</p>
                <p className={`${theme === 'light' ? 'text-dark' : 'text-light'}`}>
                  {user.full_name}
                </p>
              </div>
            )
          }
          {
            user.description && (
              <div className="mb-2">
                <p className="text-primary fw-bold fs-5">About</p>
                <p className={`${theme === 'light' ? 'text-dark' : 'text-light'}`}>
                  {user.description}
                </p>
              </div>
            )
          }
          <div className="d-flex justify-content-end mt-3">
            <Dropdown align={{ sm: 'end' }}>
              <Dropdown.Toggle variant="secondary" id="friend-options">
                <BsCheckLg className="me-2" />
                <strong>Friends</strong>
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item onClick={() => confirmationUnfriendModal.openModal()}>
                  Unfriend
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <ConfirmationModal
              title={`Unfriend ${user.display_name || user.username}`}
              confirmationText="Remove"
              isShowModal={confirmationUnfriendModal.isShowModal}
              closeModal={confirmationUnfriendModal.closeModal}
              handleConfirm={() => {
                handleUnfriend(user.username);
                confirmationUnfriendModal.closeModal();
              }}
            >
              Are you sure you want to remove {' '}
              <strong className="text-primary">
                {user.display_name || user.username} {' '}
              </strong> 
              from the list?
            </ConfirmationModal>
          </div>
        </Toast.Body>
      </Toast>
    );
  }
);

UserCard.displayName = 'UserCard';

export default UserCard;
