import { FC, PropsWithChildren, useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { PATHS } from '@/common/utils/constants';
import { useSession } from 'next-auth/react';
import Loading from '../Loading';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import usePreviousPath from '@/hooks/usePreviousPath';
import { getAPI } from '@/common/utils/customAPI';
import { selectUser, setUser } from '@/store/slice/userSlice';
import { messaging, requestPermission } from '@/common/utils/firebase';

const RouteGuard: FC<PropsWithChildren> = ({ children }) => {
  const router = useRouter();
  const { status, data: session } = useSession();
  const [loading, setLoading] = useState(true);
  const dispatch = useAppDispatch();
  const prevPath = usePreviousPath();
  const user = useAppSelector(selectUser);

  const fetchUser = useCallback(async () => {
    const res = await getAPI('/v1/account/profile');
    if (res.code === 200) {
      dispatch(setUser(res.data));
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
  }, [dispatch]);

  useEffect(() => {
    if (PATHS.publicPaths.includes(router.pathname)) {
      setLoading(false);
      return;
    }
    setLoading(true);
    if (status === 'authenticated') {
      if (PATHS.authPaths.includes(router.pathname)) {
        // Check if user come by invite link
        if (prevPath?.includes('/room/')) {
          router.push(prevPath);
        } else {
          router.push('/home');
        }
      } else {
        setLoading(false);
      }
    } else if (status === 'unauthenticated') {
      if (
        router.pathname !== '/_error' &&
        !PATHS.authPaths.includes(router.pathname)
      ) {
        router.push('/login');
      } else {
        setLoading(false);
      }
    }
  }, [router, status, dispatch, prevPath, fetchUser]);

  useEffect(() => {
    if (PATHS.publicPaths.includes(router.pathname)) {
      return;
    }
    if (
      user.status === 'non_existed' &&
      session &&
      session?.user !== undefined
    ) {
      fetchUser();
    }
  }, [fetchUser, user.status, session, session?.user]);

  useEffect(() => {
    if (status === 'authenticated') {
      requestPermission(messaging);
    }
  }, [status]);

  if (loading) {
    return (
      <div
        className="d-flex align-items-center justify-content-center w-100"
        style={{ height: '100vh' }}
      >
        <Loading />
      </div>
    );
  }
  return <>{children}</>;
};

export default RouteGuard;
