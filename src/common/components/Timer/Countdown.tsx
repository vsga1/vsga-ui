import moment from 'moment';
import { useRouter } from 'next/router';
import React, { useState, useEffect, useRef, useCallback, useContext } from 'react';
import { MdPause, MdStop, MdPlayArrow } from 'react-icons/md';
import { EVENT } from '@/common/utils/constants';
import { TimerStatus } from '@/common/utils/enums';
import formatNumber from '@/common/utils/formatNumber';
import { SocketContext } from '@/context/socket';
import { useAppDispatch, useAppSelector } from '@/hooks';
import useVisibility from '@/hooks/useVisibility';
import {
  selectGroupTimer,
  selectPersonalTimer,
  setEndAt,
  setStatus,
  TimerType,
} from '@/store/slice/timerSlice';
import { putAPI } from '@/common/utils/customAPI';
import { showNotification } from '@/store/slice/appSlice';

interface Props {
  type?: TimerType;
  editable?: boolean;
}

const Countdown = ({ type = 'PERSONAL', editable = false }: Props) => {
  const socket = useContext(SocketContext);
  const isPersonal = type === 'PERSONAL';
  const { id } = useRouter().query;
  const timerState = useAppSelector(
    isPersonal ? selectPersonalTimer : selectGroupTimer
  );
  const dispatch = useAppDispatch();
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  /*global NodeJS*/
  const timer = useRef<NodeJS.Timer>();
  const audio = useRef(new Audio('/static/audio/timer.wav'));
  const [curVision, preVision] = useVisibility();
  const timerCallback = useCallback(() => {
    if (seconds > 0) {
      setSeconds(seconds - 1);
    } else if (seconds === 0) {
      if (minutes === 0) {
        if (hours === 0) {
          clearInterval(timer.current);
          audio.current.play();
          dispatch(setStatus({ value: TimerStatus.IDLE, type }));
        } else {
          setHours(hours - 1);
          setMinutes(59);
        }
      } else {
        setMinutes(minutes - 1);
        setSeconds(59);
      }
    }
  }, [dispatch, hours, minutes, seconds, type]);

  const initTimer = useCallback(
    (time: string) => {
      const endTime = moment(time);
      const dur = moment.duration(endTime.diff(moment()));
      const h = dur.hours(),
        m = dur.minutes(),
        s = dur.seconds();
      if (h < 0 || m < 0 || s < 0) {
        clearInterval(timer.current);
        dispatch(setStatus({ value: TimerStatus.IDLE, type }));
        return false;
      }
      setHours(h);
      setMinutes(m);
      setSeconds(s);
      return true;
    },
    [dispatch, type]
  );

  useEffect(() => {
    if (type === 'ROOM') {
      socket.on(EVENT.STOP_TIMER, () => {
        clearInterval(timer.current);
        dispatch(setEndAt({ value: '', type: 'ROOM' }));
        dispatch(setStatus({ value: TimerStatus.IDLE, type: 'ROOM' }));
        dispatch(
          showNotification({
            msg: 'Room Timer has been stopped',
            status: 'info',
          })
        );
      });
      socket.on(EVENT.PAUSE_TIMER, () => {
        clearInterval(timer.current);
        dispatch(setStatus({ value: TimerStatus.PAUSE, type: 'ROOM' }));
        dispatch(
          showNotification({
            msg: 'Room Timer has been paused',
            status: 'info',
          })
        );
      });
    }

    return () => {
      socket.off(EVENT.STOP_TIMER);
      socket.off(EVENT.PAUSE_TIMER);
    };
  }, [dispatch, initTimer, type]);

  useEffect(() => {
    if (timerState.status === TimerStatus.RUN && curVision !== preVision) {
      initTimer(timerState.endAt as string);
    }
  }, [curVision, preVision, timerState.endAt, timerState.status, initTimer]);

  useEffect(() => {
    if (timerState.endAt) {
      initTimer(timerState.endAt as string);
    }
    if (timerState.status === TimerStatus.RUN) {
      timer.current = setInterval(timerCallback, 1000);
    }

    return () => {
      clearInterval(timer.current);
    };
  }, [initTimer, timerCallback, timerState.endAt, timerState.status]);

  const handleStart = async () => {
    const m = moment();
    m.add(hours, 'h');
    m.add(minutes, 'm');
    m.add(seconds + 1, 's');
    const timeStr = m.toISOString();
    dispatch(setEndAt({ value: timeStr, type }));
    dispatch(setStatus({ value: TimerStatus.RUN, type }));
    timer.current = setInterval(timerCallback, 1000);
    if (type === 'ROOM') {
      await putAPI('/v1/event/timer', {
        room_code: id,
        updated_at: timeStr,
        status: TimerStatus.RUN,
      });
      socket.emit(EVENT.SET_TIMER, { roomCode: id, time: timeStr });
    }
  };

  const handlePause = async () => {
    clearInterval(timer.current);
    dispatch(setStatus({ value: TimerStatus.PAUSE, type }));
    if (type === 'ROOM') {
      await putAPI('/v1/event/timer', {
        room_code: id,
        updated_at: timerState.endAt,
        status: TimerStatus.PAUSE,
        hour: hours,
        minute: minutes,
        second: seconds,
      });
      socket.emit(EVENT.PAUSE_TIMER, { roomCode: id });
    }
  };

  const handleStop = async () => {
    clearInterval(timer.current);
    dispatch(setStatus({ value: TimerStatus.IDLE, type }));
    if (type === 'ROOM') {
      await putAPI('/v1/event/timer', {
        room_code: id,
        updated_at: '',
        status: TimerStatus.IDLE,
      });
      socket.emit(EVENT.STOP_TIMER, { roomCode: id });
    }
  };

  return (
    <div className="timer">
      <h1>
        {formatNumber(hours, 2)}:{formatNumber(minutes, 2)}:
        {formatNumber(seconds, 2)}
      </h1>
      {editable && (
        <div className="control">
          {timerState.status === TimerStatus.RUN ? (
            <MdPause onClick={handlePause} />
          ) : (
            <MdPlayArrow onClick={handleStart} />
          )}
          <MdStop onClick={handleStop} />
        </div>
      )}
    </div>
  );
};

export default Countdown;
