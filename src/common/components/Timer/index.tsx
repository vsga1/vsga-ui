import React from 'react';
import { useAppSelector } from '@/hooks';
import { selectPersonalTimer } from '@/store/slice/timerSlice';
import { TimerStatus } from '@/common/utils/enums';
import SetTimer from './SetTimer';
import Countdown from './Countdown';

const Timer = () => {
  const { status } = useAppSelector(selectPersonalTimer);

  return status === TimerStatus.IDLE ? <SetTimer /> : <Countdown editable />;
};

export default Timer;
