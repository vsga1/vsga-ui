import React, { useContext, useEffect } from 'react';
import Countdown from './Countdown';
import { useAppDispatch, useAppSelector } from '@/hooks';
import {
  selectGroupTimer,
  setEndAt,
  setStatus,
} from '@/store/slice/timerSlice';
import { SocketContext } from '@/context/socket';
import { EVENT } from '@/common/utils/constants';
import { TimerStatus } from '@/common/utils/enums';
import SetTimer from './SetTimer';
import { getAPI } from '@/common/utils/customAPI';
import { useRouter } from 'next/router';
import moment from 'moment';
import { showNotification } from '@/store/slice/appSlice';

const RoomTimer = ({ editable }: { editable: boolean }) => {
  const socket = useContext(SocketContext);
  const { id } = useRouter().query;
  const { status } = useAppSelector(selectGroupTimer);
  const dispatch = useAppDispatch();

  useEffect(() => {
    const fetchTimer = async () => {
      const { code, data } = await getAPI(`/v1/event/timer?room_code=${id}`);
      if (code === 200) {
        const { status, updated_at, timer_left } = data;
        dispatch(setStatus({ value: status, type: 'ROOM' }));
        if (status === TimerStatus.PAUSE) {
          const newTime = moment(updated_at).add({
            h: timer_left.hour,
            m: timer_left.minute,
            s: timer_left.second,
          });
          dispatch(setEndAt({ value: newTime.toISOString(), type: 'ROOM' }));
          return;
        }
        dispatch(setEndAt({ value: updated_at, type: 'ROOM' }));
      }
    };
    fetchTimer();
    socket.on(EVENT.SET_TIMER, ({ time }) => {
      dispatch(setEndAt({ value: time as string, type: 'ROOM' }));
      dispatch(setStatus({ value: TimerStatus.RUN, type: 'ROOM' }));
      dispatch(
        showNotification({
          msg: 'Room Timer has been started',
          status: 'info',
        })
      );
    });

    return () => {
      socket.off(EVENT.SET_TIMER);
    };
  }, [dispatch, id]);

  if (!editable && status === TimerStatus.IDLE) {
    return <></>;
  }

  return status === TimerStatus.IDLE ? (
    <SetTimer type="ROOM" />
  ) : (
    <Countdown type="ROOM" editable={editable} />
  );
};

export default RoomTimer;
