import moment from 'moment';
import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import { EVENT } from '@/common/utils/constants';
import { TimerStatus } from '@/common/utils/enums';
import formatNumber from '@/common/utils/formatNumber';
import { SocketContext } from '@/context/socket';
import { useAppDispatch } from '@/hooks';
import { setEndAt, setStatus, TimerType } from '@/store/slice/timerSlice';
import { putAPI } from '@/common/utils/customAPI';

interface Props {
  type?: TimerType;
}

const SetTimer = ({ type = 'PERSONAL' }: Props) => {
  const socket = useContext(SocketContext);
  const { id } = useRouter().query;
  const dispatch = useAppDispatch();
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(50);

  const handleIncr = () => {
    if (hours < 2) {
      const newMin = minutes + 5;
      if (newMin === 60) {
        setMinutes(0);
        setHours(hours + 1);
      } else {
        setMinutes(newMin);
      }
    }
  };

  const handleDec = () => {
    const newMin = minutes - 5;
    if (hours === 0 && newMin === 0) {
      return;
    }
    if (newMin < 0) {
      if (hours > 0) {
        setMinutes(55);
        setHours(hours - 1);
      }
    } else {
      setMinutes(newMin);
    }
  };

  const handleStart = async () => {
    const m = moment();
    m.add({
      h: hours,
      m: minutes,
      s: 1,
    });
    const timeStr = m.toISOString();
    if (type === 'PERSONAL') {
      dispatch(setEndAt({ value: timeStr, type: 'PERSONAL' }));
      dispatch(setStatus({ value: TimerStatus.RUN, type: 'PERSONAL' }));
    } else {
      await putAPI('/v1/event/timer', {
        room_code: id,
        updated_at: timeStr,
        status: TimerStatus.RUN,
      });
      socket.emit(EVENT.SET_TIMER, { roomCode: id, time: timeStr });
      dispatch(setEndAt({ value: timeStr, type: 'ROOM' }));
      dispatch(setStatus({ value: TimerStatus.RUN, type: 'ROOM' }));
    }
  };

  return (
    <div className="set-timer">
      <div className="form-timer">
        <Button className="count" variant="transparent" onClick={handleDec}>
          -
        </Button>
        <span className="timer-input">
          {formatNumber(hours, 2)}:{formatNumber(minutes, 2)}:00
        </span>
        <Button className="count" variant="transparent" onClick={handleIncr}>
          +
        </Button>
      </div>
      <Button className="start" variant="transparent" onClick={handleStart}>
        Start
      </Button>
    </div>
  );
};

export default SetTimer;
