import React from 'react';
import { Form } from 'react-bootstrap';
import { MdOutlineSearch } from 'react-icons/md';

type Props = {
  placeholder: string;
  label?: string;
  id: string;
  fullWidth?: boolean;
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onKeyDown: (event: React.KeyboardEvent<HTMLInputElement>) => void;
};

const Search = ({
  placeholder,
  label,
  fullWidth,
  id,
  value,
  onChange,
  onKeyDown,
}: Props) => {
  return (
    <div className={`search ${fullWidth && 'w-100'}`}>
      {label ? <label htmlFor={id}>{label}</label> : <></>}
      <Form.Control
        id={id}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onKeyDown={onKeyDown}
        type="text"
      />
      <MdOutlineSearch />
    </div>
  );
};

export default Search;
