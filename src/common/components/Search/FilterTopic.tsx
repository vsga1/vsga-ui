import { getAPI } from '@/common/utils/customAPI';
import { TopicResponse } from '@/common/utils/interface';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import React, { useCallback, useEffect, useState } from 'react';
import { Dropdown, Form, Spinner } from 'react-bootstrap';
import { MdFilterAlt, MdHistory } from 'react-icons/md';
import { useIsFirstRender } from 'usehooks-ts';

type Props = {
  options: string[];
  setOptions: React.Dispatch<React.SetStateAction<string[]>>;
};

const FilterTopic = ({ options, setOptions }: Props) => {
  const isMount = useIsFirstRender();
  const dispatch = useAppDispatch();
  const [optionList, setOptionList] = useState<string[]>([]);
  const [searchText, setSearchText] = useState('');
  const [loading, setLoading] = useState<boolean>(false);

  const parseResponse = useCallback((data: TopicResponse[]) => {
    return data.map(({ name }) => name);
  }, []);

  useEffect(() => {
    const fetchTopic = async () => {
      const res = await getAPI('/v1/topic/suggest');
      if (res.code === 200) {
        setOptionList(parseResponse(res.data));
      }
    };
    fetchTopic();
  }, [parseResponse]);

  useEffect(() => {
    if (options.length === 0) {
      setSearchText('');
    }
  }, [options]);

  const searchCallback = useCallback(
    async (searchText?: string) => {
      setLoading(true);
      const url = searchText
        ? `/v1/topic?name=${searchText.toLocaleLowerCase()}`
        : '/v1/topic/suggest';
      const { code, data } = await getAPI(url);
      if (code === 200) {
        setOptionList(parseResponse(data));
      } else {
        dispatch(
          showNotification({
            msg: data as string,
            status: 'error',
          })
        );
      }
      setLoading(false);
    },
    [dispatch, parseResponse]
  );

  const onChangeSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const onKeyDownSearch = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      searchCallback(searchText);
    }
  };

  useEffect(() => {
    if (isMount) return;
    const delayDebounceFn = setTimeout(
      () => searchCallback(searchText.trim()),
      500
    );

    return () => clearTimeout(delayDebounceFn);
  }, [dispatch, searchText]);

  const selectedList = options.map((item, index) => (
    <Form.Check
      key={index}
      type="checkbox"
      onClick={() => {
        setOptions((prev) => {
          prev.splice(index, 1);
          return [...prev];
        });
      }}
      id={`filter-checked-${index}`}
      label={item}
      checked
      readOnly
    />
  ));

  const searchList = optionList.map(
    (item, index) =>
      !options.includes(item) && (
        <Form.Check
          key={index}
          type="checkbox"
          onClick={() => {
            setOptions((prev) => [...prev, optionList[index]]);
          }}
          id={`filter-${index}`}
          label={item}
        />
      )
  );

  return (
    <div className="filter">
      <Dropdown align="end">
        <Dropdown.Toggle className="icon outline md secondary border-0 ms-1">
          <MdFilterAlt />
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <div className="d-flex">
            <Form.Control
              type="text"
              className="filter-search"
              placeholder="Search..."
              onChange={onChangeSearch}
              onKeyDown={onKeyDownSearch}
            />

            <button
              className="icon outline secondary border-0 p-1 ms-1"
              onClick={() => setOptions([])}
            >
              <MdHistory />
            </button>
          </div>

          {!loading ? (
            optionList.length ? (
              <div className="filter-list mt-2">
                {selectedList}
                {searchList}
              </div>
            ) : (
              <div className="mt-2">No topic found</div>
            )
          ) : (
            <div className="d-flex justify-content-center mt-3">
              <Spinner variant="primary" />
            </div>
          )}
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
};

export default FilterTopic;
