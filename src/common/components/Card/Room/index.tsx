import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  MdOutlinePeople,
  MdOutlineFavoriteBorder,
  MdOutlineLogin,
  MdFavorite,
} from 'react-icons/md';
import { BiLink } from 'react-icons/bi';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { getAPI } from '@/common/utils/customAPI';
import { Room } from '@/common/utils/interface';
import { useIsFirstRender } from 'usehooks-ts';
import { Tooltip } from 'react-tooltip';
import 'react-tooltip/dist/react-tooltip.css';
import MyBadge from '../../Badge';
import { setRoomInfo } from '@/store/slice/roomSlice';

interface Props {
  room: Room;
  toggleFavor?: boolean;
}

const RoomCard = ({ room, toggleFavor = true }: Props) => {
  const dispatch = useAppDispatch();
  const [isFavor, setIsFavor] = useState(room.is_favored);
  const router = useRouter();
  const isMount = useIsFirstRender();

  const handleJoinRoom = async () => {
    dispatch(setRoomInfo(room));
    router.push(`/room/${room.code}`);
  };

  useEffect(() => {
    if (isMount) return;
    const delayDebounceFn = setTimeout(async () => {
      if (isFavor) {
        await getAPI(`/v1/room/${room.code}/favor`);
      } else {
        await getAPI(`/v1/room/${room.code}/disfavor`);
      }
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [isFavor, room.code]);

  return (
    <>
      <div className="room-card">
        <div className="header d-flex flex-column flex-shrink-0">
          <div className="info d-flex align-items-center justify-content-between">
            <div className={`shape ${room.total_member > 0 ? 'live' : 'off'}`}>
              {room.total_member > 0 ? 'active' : 'inactive'}
            </div>
          </div>
          <h6 className="text-light">{room.name}</h6>
          <div className="host-info">
            <div className="avatar">
              <img
                src={
                  room.host_avatar_url || '/static/images/default_avatar.jpg'
                }
                alt="Host avatar"
                width="32"
                height="32"
              />
            </div>
            <div className="stat d-flex">
              <div className="shape">
                <MdOutlinePeople />
                {room.total_member < 0 ? 0 : room.total_member}/{room.capacity}
              </div>
            </div>
          </div>
        </div>
        <div className="body d-flex flex-column">
          <div className="topics d-flex align-items-center justify-content-start flex-wrap mb-1">
            {room.topics?.map((topic, index) => {
              return (
                <MyBadge 
                  key={index} 
                  color={topic.color} 
                  name={topic.name} 
                  tooltipClass="topic-badge"
                >
                  {topic.name}
                </MyBadge>
              );
            })}
          </div>
          <Tooltip 
            anchorSelect=".topic-badge" 
            place="bottom" 
            noArrow={true}
            className="px-2 py-1 fs-7"
          />
          <div className="description text-truncate text-wrap mb-2">{room.description}</div>
          <div className="spacer"></div>
          <div className="actions d-flex align-items-center">
            {toggleFavor && (
              <>
                <button
                  className="outlined secondary"
                  data-tooltip-id="btn-room-card-favorite"
                  data-tooltip-content="Favorite"
                  data-tooltip-place="top"
                  onClick={() => setIsFavor(!isFavor)}
                >
                  {isFavor ? <MdFavorite /> : <MdOutlineFavoriteBorder />}
                </button>
                <Tooltip
                  id="btn-room-card-favorite"
                  className="px-2 py-1 fs-7"
                />
              </>
            )}
            <button
              className="outlined secondary"
              data-tooltip-id="btn-room-card-copy"
              data-tooltip-content="Copy room link"
              data-tooltip-place="top"
              onClick={() => {
                navigator.clipboard.writeText(
                  `${process.env.NEXT_PUBLIC_APP_URL}/room/${room.code}`
                );
                dispatch(
                  showNotification({
                    msg: 'Copy invite link success!',
                    status: 'info',
                  })
                );
              }}
            >
              <BiLink />
            </button>
            <Tooltip id="btn-room-card-copy" className="p-1 fs-7" />
            <button
              className="outlined secondary join-button"
              onClick={handleJoinRoom}
            >
              <MdOutlineLogin className="mr-2" />
              Join room
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default RoomCard;
