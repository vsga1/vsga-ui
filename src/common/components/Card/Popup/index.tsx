import React, { Dispatch, SetStateAction } from 'react';
import { MdClose } from 'react-icons/md';

type Props = {
  header?: string;
  show: boolean;
  setShow: Dispatch<SetStateAction<boolean>>;
  children: React.ReactNode;
};

const PopupCard = ({ header, show, setShow, children }: Props) => {
  return (
    <div className={`item-card ${show ? 'show' : ''}`}>
      <div className="card-header">
        <span className="mr-auto">{header}</span>
        <MdClose
          className={!header ? 'float' : ''}
          onClick={() => setShow(false)}
        />
      </div>
      {children}
    </div>
  );
};

export default PopupCard;
