import React, { useContext, useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/dist/client/router';
import { useAppDispatch, useAppSelector } from '@/hooks';
import {
  RoomNote,
  selectRoomNote,
  updateRoomNote,
} from '@/store/slice/noteSlice';
import { getAPI, putAPI } from '@/common/utils/customAPI';
import { Button } from 'react-bootstrap';
import { Tooltip } from 'react-tooltip';
import { SocketContext } from '@/context/socket';
import { EVENT } from '@/common/utils/constants';
import { showNotification } from '@/store/slice/appSlice';

const RoomNote = () => {
  const socket = useContext(SocketContext);
  const { id } = useRouter().query;
  const dispatch = useAppDispatch();
  const note = useAppSelector(selectRoomNote);
  const [header, setHeader] = useState(note.header);
  const [desc, setDesc] = useState(note.description);
  const [isEdit, setIsEdit] = useState(false);

  const textAreaRef = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    if (textAreaRef.current) {
      textAreaRef.current.style.height = '0';
      const scrollHeight = textAreaRef.current.scrollHeight;
      textAreaRef.current.style.height = scrollHeight + 'px';
    }
  }, [textAreaRef, desc]);

  useEffect(() => {
    const fetchNote = async () => {
      const { code, data } = await getAPI(`/v1/event/note?room_code=${id}`);
      if (code === 200) {
        setHeader(data.header);
        setDesc(data.description);
        dispatch(updateRoomNote(data));
      }
    };
    fetchNote();
  }, [dispatch, id]);

  useEffect(() => {
    socket.on(EVENT.OPEN_EDIT_NOTE, ({ header, desc }) => {
      const dispatchData: Partial<RoomNote> = { status: 'OPEN' };
      if (header) {
        setHeader(header);
        dispatchData.header = header;
      }
      if (desc) {
        setDesc(desc);
        dispatchData.description = desc;
      }
      dispatch(updateRoomNote(dispatchData));
    });

    socket.on(EVENT.CLOSE_EDIT_NOTE, () => {
      dispatch(updateRoomNote({ status: 'EDIT' }));
      dispatch(
        showNotification({
          status: 'info',
          msg: 'Room note has been updated!',
        })
      );
    });
  }, [dispatch, id, socket]);

  const handleStartEdit = () => {
    if (note.status === 'OPEN' && !isEdit) {
      putAPI('/v1/event/note', {
        room_code: id,
        status: 'EDIT',
      });
      socket.emit(EVENT.CLOSE_EDIT_NOTE, { roomCode: id });
      setIsEdit(true);
    }
  };

  const handleSave = () => {
    socket.emit(EVENT.OPEN_EDIT_NOTE, { roomCode: id, header, desc });
    putAPI('/v1/event/note', {
      room_code: id,
      header,
      description: desc,
      status: 'OPEN',
    });
    setIsEdit(false);
  };

  return (
    <>
      <div
        className="room-note"
        data-tooltip-content="Someone is editing."
        data-tooltip-place="top"
        data-tooltip-id="room-note-tt"
      >
        <input
          placeholder="Note Header"
          type="text"
          className="header"
          value={header}
          onChange={(e) => setHeader(e.target.value)}
          readOnly={note.status === 'EDIT' && !isEdit}
          onClick={handleStartEdit}
        />
        <textarea
          placeholder="Note content"
          className="content"
          ref={textAreaRef}
          value={desc}
          onChange={(e) => setDesc(e.target.value)}
          readOnly={note.status === 'EDIT' && !isEdit}
          onClick={handleStartEdit}
        ></textarea>
        {isEdit && (
          <Button variant="transparent" onClick={handleSave}>
            Save
          </Button>
        )}
      </div>
      {note.status === 'EDIT' && !isEdit && (
        <Tooltip id="room-note-tt" className="px-2 py-1 fs-7" />
      )}
    </>
  );
};

export default RoomNote;
