import React, { useCallback, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { useRouter } from 'next/router';
import { Note, selectAllNote, initNote } from '@/store/slice/noteSlice';
import { getAPI, putAPI } from '@/common/utils/customAPI';
import { showNotification } from '@/store/slice/appSlice';
import NoteItem from './NoteItem';
import AddNote from './AddNote';
import Loading from '../Loading';

const Note = () => {
  const dispatch = useAppDispatch();
  const allNote = useAppSelector(selectAllNote);
  const [active, setActive] = useState(-1);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const roomId = router.query.id;

  const updateNoteData = useCallback(
    async (note: Note) => {
      if (roomId) {
        const res = await putAPI(`/v1/room/${roomId}/notes`, note);
        if (res.code !== 200) {
          dispatch(
            showNotification({
              msg: res.data,
              status: 'error',
            })
          );
        }
      }
    },
    [dispatch, roomId]
  );

  useEffect(() => {
    if (roomId) {
      const fetchNoteData = async () => {
        setLoading(true);
        const res = await getAPI(`/v1/room/${roomId}/notes`);
        if (res.code === 200) {
          dispatch(initNote(res.data));
        } else {
          dispatch(
            showNotification({
              msg: res.data as string,
              status: 'error',
            })
          );
        }
        setLoading(false);
      };
      fetchNoteData();
    }
  }, [dispatch, roomId]);

  const noteList = allNote.map((note, index) => {
    return (
      <NoteItem
        key={note.code || index}
        code={note.code}
        title={note.header}
        content={note.description}
        isEdit={active === index}
        disableFocus={() => setActive(-1)}
        updateData={updateNoteData}
        onClick={() => setActive(index)}
      />
    );
  });

  return (
    <div className="notes">
      <AddNote />
      {loading ? (
        <div className="d-flex justify-content-center align-items-center">
          <Loading />
        </div>
      ) : (
        <div className="note-list">{noteList}</div>
      )}
    </div>
  );
};

export default Note;
