import React, {
  HTMLProps,
  MouseEventHandler,
  useEffect,
  useRef,
  useState,
} from 'react';
import { MdClose } from 'react-icons/md';
import { useAppDispatch } from '@/hooks';
import { Note, updateNote, removeNote } from '@/store/slice/noteSlice';
import { Button } from 'react-bootstrap';

interface Props extends HTMLProps<HTMLDivElement> {
  code: string | null;
  title: string;
  content: string;
  isEdit?: boolean;
  disableFocus: () => void;
  updateData: (note: Note) => Promise<void>;
}

const NoteItem = ({
  code,
  title,
  content,
  isEdit,
  disableFocus,
  updateData,
  ...rest
}: Props) => {
  const dispatch = useAppDispatch();
  const [header, setHeader] = useState(title);
  const [note, setNote] = useState(content);

  const textAreaRef = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    if (textAreaRef.current) {
      textAreaRef.current.style.height = '0';
      const scrollHeight = textAreaRef.current.scrollHeight;
      textAreaRef.current.style.height = scrollHeight + 'px';
    }
  }, [textAreaRef, note]);

  const handleSave: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.stopPropagation();
    updateData({
      code,
      description: note,
      header: header,
      status: 'ACTIVE',
    });
    dispatch(updateNote({ code, description: note, header }));
    disableFocus();
  };

  const handleRemove = () => {
    updateData({
      code,
      header: '',
      description: '',
      status: 'INACTIVE',
    });
    dispatch(removeNote(code));
  };

  return (
    <div className="note" {...rest}>
      <div className="note-header">
        <input
          type="text"
          value={header}
          onChange={(e) => setHeader(e.target.value)}
          className="me-auto"
          placeholder="Header"
          spellCheck={false}
        />
        <MdClose onClick={handleRemove} />
      </div>
      <textarea
        className="note-content"
        onChange={(e) => setNote(e.target.value)}
        value={note}
        placeholder="Note..."
        spellCheck={false}
        ref={textAreaRef}
      />
      <div className="note-footer">
        {isEdit && <Button onClick={handleSave}>Save</Button>}
      </div>
    </div>
  );
};

export default NoteItem;
