import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { MdNoteAdd } from 'react-icons/md';
import { Spinner } from 'react-bootstrap';
import { useAppDispatch } from '@/hooks';
import { addNote } from '@/store/slice/noteSlice';
import { showNotification } from '@/store/slice/appSlice';
import { postAPI } from '@/common/utils/customAPI';

const AddNote = () => {
  const [loading, setLoading] = useState(false);
  const dispatch = useAppDispatch();
  const router = useRouter();
  const roomId = router.query.id;

  const handleAddNote = async () => {
    setLoading(true);
    if (roomId) {
      const res = await postAPI(`/v1/room/${roomId}/notes`, {
        header: '',
        description: '',
      });
      if (res.code !== 200) {
        dispatch(
          showNotification({
            msg: res.data,
            status: 'error',
          })
        );
      } else {
        dispatch(addNote(res.data));
      }
    }
    setLoading(false);
  };

  return (
    <div
      className={`add-note ${loading ? 'loading' : ''}`}
      onClick={handleAddNote}
    >
      {loading ? (
        <Spinner className="mr-2" size="sm" />
      ) : (
        <MdNoteAdd className="mr-2" />
      )}{' '}
      Add new note...
    </div>
  );
};

export default AddNote;
