import React, { useState } from 'react';
import { Button, Spinner } from 'react-bootstrap';
import Cropper, { Area, Point } from 'react-easy-crop';

type Props = {
  image: string;
  onCropDone: (imgCroppedArea: Area | null) => void;
  loading: boolean;
  aspectRatio?: number;
};

const ImageCropper = ({
  image,
  onCropDone,
  loading,
  aspectRatio = 1 / 1,
}: Props) => {
  const [crop, setCrop] = useState<Point>({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [croppedArea, setCroppedArea] = useState<Area | null>(null);

  const onCropComplete = (
    croppedAreaPercentage: Area,
    croppedAreaPixels: Area
  ) => {
    setCroppedArea(croppedAreaPixels);
  };

  return (
    <div className="cropper">
      <div className="cropper-container">
        <Cropper
          image={image}
          aspect={aspectRatio}
          crop={crop}
          zoom={zoom}
          onCropChange={setCrop}
          onZoomChange={setZoom}
          onCropComplete={onCropComplete}
          style={{
            containerStyle: {
              backgroundColor: '#fff',
            },
          }}
        />
      </div>

      <div className="d-flex justify-content-end py-3">
        <Button
          variant="secondary"
          disabled={loading}
          className="text-uppercase"
          onClick={() => {
            onCropDone(croppedArea);
          }}
        >
          {loading && <Spinner size="sm" className="me-2" />}
          Done
        </Button>
      </div>
    </div>
  );
};

export default ImageCropper;
