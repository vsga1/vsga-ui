import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import { Info } from '@/store/slice/userSlice';
import React, { useState } from 'react'
import UserCard from '../UserCard';
import useComponentVisible from '@/hooks/useComponentVisible';

interface FriendProps {
  user: Info;
  handleUnfriend: (username: string) => void;
}

const FriendItem: React.FC<FriendProps> = ({ user, handleUnfriend }) => {
  const [avatar, setAvatar] = useState(user.avatar_img_url);
  const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

  return (
    <div className="user py-2 px-3" onClick={() => setIsComponentVisible(true)}>
      <div className="d-flex align-items-center overflow-hidden">
        <div className="avatar mr-2">
          <img
            src={avatar ? avatar : DEFAULT_AVATAR_PATH}
            alt={user.display_name || user.username}
            onError={() => setAvatar(DEFAULT_AVATAR_PATH)}
          />
        </div>
        <div className="text-truncate">{user.display_name || user.username}</div>
      </div>
      <UserCard
        handleUnfriend={handleUnfriend}
        ref={ref}
        user={user} 
        isShow={isComponentVisible} 
        setIsShow={(isShow: boolean) => setIsComponentVisible(isShow)} 
      />
    </div>
  )
}

export default FriendItem;
