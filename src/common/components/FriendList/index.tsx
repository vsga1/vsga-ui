import { getAPI, postAPI } from '@/common/utils/customAPI';
import { SidebarContext } from '@/layouts/MainLayout';
import { showNotification } from '@/store/slice/appSlice';
import React, { useContext, useEffect } from 'react';
import { Button, Offcanvas } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import FriendItem from './FriendItem';
import { useRouter } from 'next/router';
import {
  removeFriend,
  selectFriendList,
  setFriendList,
} from '@/store/slice/friendSlice';

const FriendList = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const friendList = useSelector(selectFriendList);

  useEffect(() => {
    const fetchFriendList = async () => {
      const res = await getAPI('/v1/friend/list');
      if (res.code === 200) {
        dispatch(setFriendList(res.data));
      } else {
        dispatch(
          showNotification({
            msg: res.data as string,
            status: 'error',
          })
        );
      }
    };

    fetchFriendList();
  }, []);

  const handleUnfriend = async (username: string) => {
    const res = await postAPI('/v1/friend/unfriend', { username });
    if (res.code === 200) {
      dispatch(removeFriend(username));
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
  };

  const {
    secondarySidebar: { isShowSidebar, closeSidebar },
  } = useContext(SidebarContext);

  return (
    <Offcanvas
      className="friend-list overflow-visible"
      responsive="xl"
      show={isShowSidebar}
      onHide={closeSidebar}
      placement="end"
    >
      <Offcanvas.Body>
        <div className="d-flex flex-column">
          <h5 className="title py-2 px-3 mb-md-0 fw-bold">
            Friends
          </h5>
          {friendList.length ? (
            friendList.map((friend) => (
              <FriendItem
                key={friend.username}
                user={friend}
                handleUnfriend={handleUnfriend}
              />
            ))
          ) : (
            <>
              <span className="text-center my-2 px-2">
                {'Don\'t have a friend? Let\'s add some new friends.'}
              </span>
              <div className="d-flex justify-content-center">
                <Button
                  variant="secondary"
                  size="sm"
                  onClick={() => router.push('/users')}
                  className="fw-bold"
                >
                  Find users
                </Button>
              </div>
            </>
          )}
        </div>
      </Offcanvas.Body>
    </Offcanvas>
  );
};

export default FriendList;
