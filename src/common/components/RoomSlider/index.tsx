import React from 'react';
import { Room } from '@/common/utils/interface';
import RoomCard from '../Card/Room';
import { MdSearchOff } from 'react-icons/md';
import Slider from 'react-slick';
type Props = {
  list: Room[];
  title: string;
};

const RoomSlider = ({ list, title }: Props) => {
  const sliderOption = {
    autoplay: false,
    arrows: list.length > 3,
    dots: true,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    dotsClass: 'slick-dots dots-theme',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          arrows: list.length > 2,
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 992,
        settings: {
          arrows: list.length > 1,
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div className="room-slider">
      <h5 className="title">{title}</h5>
      {list.length ? (
        <Slider {...sliderOption}>
          {list.map((room, index) => (
            <div className="px-3 pb-3" key={index}>
              <RoomCard room={room} toggleFavor={false}/>
            </div>
          ))}
        </Slider>
      ) : (
        <p className="not-found">
          <MdSearchOff />
          No room found
        </p>
      )}
    </div>
  );
};

export default RoomSlider;
