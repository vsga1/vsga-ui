import React from 'react';
import { Toast, ToastContainer } from 'react-bootstrap';
import { MdError, MdWarning } from 'react-icons/md';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { selectNotification, hideNotification } from '@/store/slice/appSlice';
import useTheme from '@/hooks/useTheme';

const Notification = () => {
  const dispatch = useAppDispatch();
  const noti = useAppSelector(selectNotification);
  const theme = useTheme();

  return (
    <ToastContainer className="p-3 position-fixed" position="top-end">
      <Toast
        onClose={() => dispatch(hideNotification())}
        show={noti.show}
        autohide
        delay={5000}
      >
        <Toast.Header className="border-0" {...(theme === 'dark' ? {closeVariant: 'white'} : {})}>
          <span className={`icon-status ${noti.status}`}>
            {noti.status === 'warn' ? <MdWarning /> : <MdError />}
          </span>
          <p className={`my-auto me-auto ${theme === 'dark' ? 'text-light' : 'text-dark'}`}>
            VStudy
          </p>
        </Toast.Header>
        <Toast.Body className={`${theme === 'dark' ? 'text-light' : 'text-dark'}`}>{noti.msg}</Toast.Body>
      </Toast>
    </ToastContainer>
  );
};

export default Notification;
