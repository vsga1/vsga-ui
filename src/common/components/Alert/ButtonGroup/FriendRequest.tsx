import { postAPI, putAPI } from '@/common/utils/customAPI';
import { showNotification } from '@/store/slice/appSlice';
import { decreaseUnreadNotification } from '@/store/slice/notificationSlice';
import { debounce } from 'lodash';
import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';

type Props = {
  id: string;
  username: string;
  type: string;
  time: string;
};

const FriendRequest = ({ id, username, type, time }: Props) => {
  const dispatch = useDispatch();
  const [buttonStatus, setButtonStatus] = useState<
    'DEFAULT' | 'ACCEPTED' | 'DECLINED'
  >('DEFAULT');

  const onAccept = debounce(() => {
    postAPI('/v1/friend/add-friend', { username }).then((res) => {
      if (res.code !== 200) {
        dispatch(
          showNotification({
            msg: res.data as string,
            status: 'error',
          })
        );
      } else {
        putAPI(`/v1/notification/${id}/delete`, {});
        setButtonStatus('ACCEPTED');
        dispatch(decreaseUnreadNotification());
      }
    });
  }, 500);

  const onDecline = debounce(async () => {
    const res = await postAPI('/v1/notification/add-friend/decline', {
      username,
      data: {
        noti_id: id,
        type,
        time,
      },
    });
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    } else {
      setButtonStatus('DECLINED');
      dispatch(decreaseUnreadNotification());
    }
  }, 500);

  return (
    <div className="d-flex mt-1 ml-2">
      {buttonStatus === 'ACCEPTED' && (
        <small className="text-theme">Friend request accepted.</small>
      )}
      {buttonStatus === 'DECLINED' && (
        <small className="text-theme">Declined the friend request.</small>
      )}
      {buttonStatus === 'DEFAULT' && (
        <>
          <Button variant="secondary" onClick={onAccept}>
            Accept
          </Button>
          <Button variant="danger" className="ml-2" onClick={onDecline}>
            Decline
          </Button>
        </>
      )}
    </div>
  );
};

export default FriendRequest;
