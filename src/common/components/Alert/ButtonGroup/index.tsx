/* eslint-disable indent */
import React from 'react';
import FriendRequest from './FriendRequest';
import { INotification } from '@/common/utils/interface';
import { NotificationType } from '@/common/utils/enums';
import InviteJoinRoom from './InviteJoinRoom';

type Props = {
  notification: INotification;
};

const ButtonGroup = ({ notification }: Props) => {
  const type = notification.data?.type;

  switch (type) {
    case NotificationType.FRIEND_REQUEST:
      return (
        <FriendRequest
          id={notification.id || notification.data?.noti_id || ''}
          username={
            notification.data?.username || notification.sender?.username || ''
          }
          type={`${notification.data?.type}`}
          time={`${notification.data?.time}`}
        />
      );
    case NotificationType.JOIN_ROOM_REQUEST:
      return (
        <InviteJoinRoom
          id={notification.id || notification.data?.noti_id || ''}
          code={`${notification.data?.code}`}
        />
      );
    case NotificationType.DEFAULT:
      return <></>;
    default:
      return <></>;
  }
};

export default ButtonGroup;
