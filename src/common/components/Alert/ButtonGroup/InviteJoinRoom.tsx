import { putAPI } from '@/common/utils/customAPI';
import { useRouter } from 'next/router';
import React from 'react';
import { Button } from 'react-bootstrap';

type Props = {
  id: string;
  code: string;
};

const InviteJoinRoom = ({ id, code }: Props) => {
  const router = useRouter();
  const handleClickJoinRoom = async () => {
    await putAPI(`/v1/notification/${id}/delete`, {});
    router.replace(`/room/${code}`);
  };
  return (
    <div className="d-flex mt-1 ml-2">
      <Button variant="secondary" onClick={handleClickJoinRoom}>
        Join
      </Button>
    </div>
  );
};

export default InviteJoinRoom;
