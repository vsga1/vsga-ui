import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '@/hooks';
import {
  addListNotification,
  readAllNotifications,
  selectNotificationList,
  selectNotificationPageInfo,
  updateNotificationPageInfo,
} from '@/store/slice/notificationSlice';
import { MdCheck } from 'react-icons/md';
import { Button, Spinner } from 'react-bootstrap';
import Item from './Item';
import { Tooltip } from 'react-tooltip';
import InfiniteScroll from 'react-infinite-scroll-component';
import { getAPI, putAPI } from '@/common/utils/customAPI';
import { debounce } from 'lodash';

const Alert = () => {
  const dispatch = useAppDispatch();
  const notifications = useAppSelector(selectNotificationList);
  const pageInfo = useAppSelector(selectNotificationPageInfo);
  const [hasMore, setHasMore] = useState(true);
  const handleReadAll = debounce(async () => {
    dispatch(readAllNotifications());
    await putAPI('/v1/notification/read-all', {});
  }, 500);

  const handleLoadMoreNotifications = async () => {
    const res = await getAPI('/v1/notification', {
      limit: pageInfo.limit,
      offset: pageInfo.next * pageInfo.limit,
    });
    if (res.code === 200) {
      dispatch(addListNotification(res.data));
      dispatch(
        updateNotificationPageInfo({
          ...pageInfo,
          next: pageInfo.next + 1,
        })
      );
    }
  };

  useEffect(() => {
    if (pageInfo.next * pageInfo.limit > pageInfo.total) {
      setHasMore(false);
    }
  }, [pageInfo.next]);

  return (
    <div>
      <div className="d-flex align-items-center justify-content-between pb-2 px-2">
        <h4 className="text-theme mb-0">Notifications</h4>
        <Button
          className="icon outline secondary border-0"
          data-tooltip-content="Read all"
          data-tooltip-id="check-read-all"
          data-tooltip-place="bottom"
          onClick={handleReadAll}
        >
          {<MdCheck />}
        </Button>
        <Tooltip id="check-read-all" />
      </div>
      {notifications.length !== 0 ? (
        <div id="scrollable-notifications" className="list">
          <InfiniteScroll
            dataLength={notifications.length}
            loader={
              <div className="w-100 d-flex justify-content-center">
                <Spinner animation="border" variant="primary" />
              </div>
            }
            next={handleLoadMoreNotifications}
            hasMore={hasMore}
            scrollableTarget="scrollable-notifications"
            style={{
              overflow: 'none !important',
            }}
          >
            {notifications.map((item, index) => {
              return (
                <div key={index}>
                  <Item notification={item} />
                </div>
              );
            })}
          </InfiniteScroll>
        </div>
      ) : (
        <div className="d-flex flex-column align-items-center justify-content-center w-100 p-2 gap-3">
          <img
            className="w-50"
            src="/static/images/notification.png"
            alt="bell"
          />
          <div>
            <h4 className="text-theme text-center mb-0">No notifications</h4>
            <p className="text-theme text-center">
              We&apos;ll notify you when something arrives.
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

export default Alert;
