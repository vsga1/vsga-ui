import React, { useState } from 'react';
import moment from 'moment';
import { INotification } from '@/common/utils/interface';
import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import ButtonGroup from './ButtonGroup';
import { NotificationStatus } from '@/common/utils/enums';
import { putAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { decreaseUnreadNotification } from '@/store/slice/notificationSlice';

type Props = {
  notification: INotification;
};

const Item: React.FC<Props> = ({ notification }: Props) => {
  const dispatch = useAppDispatch();
  const [unread, setUnread] = useState(
    notification.status === NotificationStatus.UNREAD
  );

  const onItemClick = async (e: React.MouseEvent<HTMLElement>) => {
    if (
      e.target instanceof HTMLElement &&
      e.target.tagName.toLowerCase() !== 'button'
    ) {
      await putAPI(`/v1/notification/${notification.id}/update-status`, {});
      setUnread(false);
      dispatch(decreaseUnreadNotification());
    }
  };

  return (
    <div onClick={onItemClick} className="item py-2 d-flex border-top">
      <div className="user-avatar">
        <img
          src={
            notification.data?.senderImage ||
            notification.sender?.avatar_img_url ||
            DEFAULT_AVATAR_PATH
          }
          alt="Notification image"
        />
      </div>
      <div>
        <div className="content">
          <p className={`text-theme ${unread && 'font-weight-bold'}`}>
            {notification.body}
          </p>
          <p className="text-primary">
            <small>{moment().from(notification.data?.time || '')}</small>
          </p>
        </div>
        <ButtonGroup notification={notification} />
      </div>
    </div>
  );
};

export default Item;
