import { UPDATE_RESOURCE } from '@/common/utils/constants/event';
import { getAPI, deleteAPI, uploadFileAPI } from '@/common/utils/customAPI';
import { SocketContext } from '@/context/socket';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { selectRoomInfo } from '@/store/slice/roomSlice';
import moment from 'moment';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { Button, Dropdown, Spinner } from 'react-bootstrap';
import { MdOutlineAdd, MdOutlineMoreVert } from 'react-icons/md';
import { FcEmptyTrash } from 'react-icons/fc';
import Loading from '../Loading';
import useTheme from '@/hooks/useTheme';

const MAX_FILE_SIZE = 10 * 1024 * 1024;

interface IFile {
  id: string;
  iconLink: string;
  thumbnailLink: string;
  webContentLink: string;
  webViewLink: string;
  name: string;
  modifiedTime: string;
}

const Resource = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const dispatch = useAppDispatch();
  const socket = useContext(SocketContext);
  const room = useAppSelector(selectRoomInfo);
  const [fileList, setFileList] = useState<Array<IFile>>([]);
  const [loading, setLoading] = useState(false);
  const [loadingList, setLoadingList] = useState(false);
  const theme = useTheme();

  const onAddResource = () => {
    if (loading) {
      return;
    }
    if (inputRef.current) {
      inputRef.current.click();
    }
  };

  const fetchFileList = async () => {
    setLoadingList(true);
    const res = await getAPI(`/v1/room/${room?.code}/resource`);
    setLoadingList(false);
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    setFileList(res.data);
  };

  const inputChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (loading) {
      return;
    }
    setLoading(true);
    const files = e.target.files;
    if (files) {
      if (files[0].size > MAX_FILE_SIZE) {
        dispatch(
          showNotification({
            msg: 'File size must not be bigger than 10MB!',
            status: 'error',
          })
        );
        return;
      }
      const formData = new FormData();
      formData.append('file', files[0]);
      const res = await uploadFileAPI(
        `/v1/room/${room?.code}/resource`,
        formData
      );
      if (res.code === 200) {
        socket.emit(UPDATE_RESOURCE, { roomCode: room?.code });
      } else {
        dispatch(
          showNotification({
            msg: res.data,
            status: 'error',
          })
        );
      }
    }
    setLoading(false);
    e.target.value = '';
  };

  const handleDeleteFile = async (fileId: string) => {
    if (loading) {
      return;
    }
    setLoading(true);
    const res = await deleteAPI(`/v1/room/${room?.code}/resource/${fileId}`);
    if (res.code === 200) {
      socket.emit(UPDATE_RESOURCE, { roomCode: room?.code });
    } else {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
    }
    setLoading(false);
  };

  const cleanUp = () => {
    socket.off(UPDATE_RESOURCE);
  };

  useEffect(() => {
    fetchFileList();

    socket.on(UPDATE_RESOURCE, () => {
      dispatch(
        showNotification({
          status: 'info',
          msg: 'Resource has been updated!',
        })
      );
      fetchFileList();
    });
    window.addEventListener('beforeunload', cleanUp);

    return () => {
      cleanUp();
      window.removeEventListener('beforeunload', cleanUp);
    };
  }, []);
  return (
    <div id="resource" className="p-2">
      <Button
        className="btn-add-resource outlined secondary mb-2"
        onClick={onAddResource}
        disabled={loading}
      >
        <MdOutlineAdd className="mr-2" />
        Add new resource
      </Button>
      <input onChange={inputChange} type="file" ref={inputRef} />
      <div className="file-list">
        {loadingList ? (
          <div className="d-flex justify-content-center">
            <Loading />
          </div>
        ) : (
          fileList.length === 0 && (
            <div className="d-flex align-items-center justify-content-center flex-column">
              <FcEmptyTrash className="empty-icon" />
              There are no files
            </div>
          )
        )}
        {fileList.map((file, index) => {
          return (
            <div className="file-card mb-2" key={index}>
              <img
                className="mr-3"
                src={file.iconLink}
                alt="iconLink"
                width={24}
                height={24}
              />
              <div className="body">
                <div className="file-name">{file.name}</div>
                <div className="modified-time">
                  {moment(file.modifiedTime).format('Do MMMM YYYY')}
                </div>
              </div>
              <Dropdown className="icon">
                <Dropdown.Toggle className="icon circle">
                  <MdOutlineMoreVert />
                </Dropdown.Toggle>
                <Dropdown.Menu
                  {...(theme === 'dark' ? { variant: 'dark' } : {})}
                >
                  <Dropdown.Item
                    disabled={loading}
                    target="_blank"
                    href={file.webContentLink}
                  >
                    Download
                  </Dropdown.Item>
                  <Dropdown.Item
                    as="button"
                    disabled={loading}
                    onClick={() => handleDeleteFile(file.id)}
                  >
                    Delete
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          );
        })}
        {loading && (
          <div className="d-flex w-100 justify-content-center">
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </div>
        )}
      </div>
    </div>
  );
};

export default Resource;
