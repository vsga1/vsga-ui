import React, { ReactNode } from 'react';

interface PageSectionProps {
    children: ReactNode;
    background?: string;
    id?: string;
}

const PageSection: React.FC<PageSectionProps> = ({ children, background = 'white', id ='' }) => {
  return (
    <div className={`pb-5 bg-${background}`} style={{ paddingTop: '5rem' }} {...id && {id}}>
      <div className="container d-flex align-items-center justify-content-center">
        {children}
      </div>
    </div>
  )
}

export default PageSection;
