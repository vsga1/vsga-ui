import React, { useContext } from 'react';
import { UserInRoom } from '@/store/slice/roomSlice';
import { MdOutlineMoreVert } from 'react-icons/md';
import { Dropdown } from 'react-bootstrap';
import { SocketContext } from '@/context/socket';
import { KICK_USER, UPDATE_ROLE } from '@/common/utils/constants/event';
import { Role } from '@/common/utils/enums';
import {
  canDemote,
  canKick,
  canPromote,
} from '@/common/utils/roomAuthorization';
import { postAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import useTheme from '@/hooks/useTheme';

interface Props {
  user: UserInRoom;
  role?: Role;
  roomCode?: string;
}

const User: React.FC<Props> = ({ user, role, roomCode }) => {
  const theme = useTheme();
  const socket = useContext(SocketContext);
  const dispatch = useAppDispatch();

  const handleDelete = () => {
    if (user.id) {
      socket.emit(KICK_USER, { id: user.id });
    }
  };
  const handlePromote = async () => {
    const res = await postAPI('/v1/event/promote', {
      room_code: roomCode,
      username: user.username,
    });
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    socket.emit(UPDATE_ROLE, {
      roomCode,
      username: user.username,
      role: Role.CO_HOST,
    });
  };

  const handleDemote = async () => {
    const res = await postAPI('/v1/event/demote', {
      room_code: roomCode,
      username: user.username,
    });
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    socket.emit(UPDATE_ROLE, {
      roomCode,
      username: user.username,
      role: Role.MEMBER,
    });
  };

  const optionMenu = [
    {
      name: 'Promote to co-host',
      show: canPromote(role, user.role),
      onClick: handlePromote,
    },
    {
      name: 'Demote to member',
      show: canDemote(role, user.role),
      onClick: handleDemote,
    },
    {
      name: 'Delete',
      show: canKick(role, user.role),
      onClick: handleDelete,
    },
  ];

  const isShowOption = optionMenu.filter((i) => i.show).length > 0;

  return (
    <div className="user">
      <div className="d-flex align-items-center text-truncate">
        <div className="avatar mr-2">
          <img
            src={user.avatar_img_url || DEFAULT_AVATAR_PATH}
            alt="avatar"
          />
        </div>
        <span className="text-truncate">{user.username}</span>&nbsp;<i>({user.role})</i>
      </div>
      <div className="actions">
        {isShowOption && (
          <Dropdown className="icon">
            <Dropdown.Toggle className="icon circle">
              <MdOutlineMoreVert />
            </Dropdown.Toggle>
            <Dropdown.Menu {...(theme === 'dark' ? {variant: 'dark'} : {})}>
              {optionMenu.map(
                (item, index) =>
                  item.show && (
                    <Dropdown.Item
                      as="button"
                      onClick={item.onClick}
                      key={index}
                    >
                      {item.name}
                    </Dropdown.Item>
                  )
              )}
            </Dropdown.Menu>
          </Dropdown>
        )}
      </div>
    </div>
  );
};

export default User;
