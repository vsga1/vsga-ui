import { useAppSelector } from '@/hooks';
import {
  selectRole,
  selectUsers,
  selectRoomInfo,
} from '@/store/slice/roomSlice';
import React from 'react';
import User from './User';

const UsersInRoom = () => {
  const users = useAppSelector(selectUsers);
  const role = useAppSelector(selectRole);
  const room = useAppSelector(selectRoomInfo);

  return (
    <div className="user-in-room">
      <div>
        {users?.map((u, index) => {
          return (
            <User key={index} user={u} role={role} roomCode={room?.code} />
          );
        })}
      </div>
    </div>
  );
};

export default UsersInRoom;
