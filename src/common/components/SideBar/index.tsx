import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { useAppSelector } from '@/hooks';
import { selectAppTheme } from '@/store/slice/appSlice';
import { Offcanvas } from 'react-bootstrap';
import { SidebarContext } from '@/layouts/MainLayout';
import { AiFillHome } from 'react-icons/ai';
import { FaUsers } from 'react-icons/fa';
import { HiVideoCamera } from 'react-icons/hi2';

const MENU = [
  {
    id: 1,
    text: 'Home',
    href: '/home',
    icon: <AiFillHome />,
  },
  {
    id: 2,
    text: 'Room',
    href: '/rooms',
    icon: <HiVideoCamera />,
  },
  {
    id: 3,
    text: 'Users',
    href: '/users',
    icon: <FaUsers />,
  },
];

const Sidebar = () => {
  const router = useRouter();
  const theme = useAppSelector(selectAppTheme);
  const { primarySidebar: { isShowSidebar, closeSidebar } } = useContext(SidebarContext);

  return (
    <Offcanvas show={isShowSidebar} id="side-bar" responsive="md" className={`${theme}`} onHide={closeSidebar}>
      <Offcanvas.Body>
        <Link
          className="d-flex align-items-center justify-content-center"
          href="/home"
        >
          <Image
            src="/static/images/logo.png"
            alt="logo"
            width="45"
            height="47"
            priority
          />
        </Link>
        <div className="menu d-flex flex-column mt-2">
          {MENU.map((item) => (
            <Link
              className={`item d-flex align-items-center justify-content-center flex-column py-1 mb-2 ${
                router.pathname === item.href ? 'active' : ''
              }`}
              key={item.id}
              href={item.href}
            >
              {item.icon}
              {item.text}
            </Link>
          ))}
        </div>
      </Offcanvas.Body>
    </Offcanvas>
  );
};

export default Sidebar;
