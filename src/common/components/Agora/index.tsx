import React, { useEffect, useRef, useState } from 'react';
import AgoraRTC, {
  UID,
  IRemoteAudioTrack,
  IMicrophoneAudioTrack,
  IAgoraRTCClient,
  IAgoraRTCRemoteUser,
  ILocalVideoTrack,
  IRemoteVideoTrack,
} from 'agora-rtc-sdk-ng';
import AgoraActions from './AgoraActions';
import { Role } from '@/common/utils/enums';
import { fetchToken } from '@/common/utils/agora/fetchToken';
import { AGORA } from '@/common/utils/constants';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';

interface IChannelParameters {
  localAudioTrack: null | IMicrophoneAudioTrack;
  localVideoTrack: null | ILocalVideoTrack;
}

interface IRemoteUser {
  remoteAudioTrack: IRemoteAudioTrack | undefined;
  remoteVideoTrack: IRemoteVideoTrack | undefined;
  remoteUid: UID;
}

const channelParameters: IChannelParameters = {
  localAudioTrack: null,
  localVideoTrack: null,
};

const remoteUsers: Array<IRemoteUser> = [];

type Props = {
  id: string;
  role?: Role;
  uid: string;
  show: boolean;
};

AgoraRTC.setLogLevel(4);

const Agora = ({ id, role, uid, show }: Props) => {
  const dispatch = useAppDispatch();
  const options = {
    appId: AGORA.appId,
    uid,
  };
  const mainVideoRef = useRef(null);
  const localRef = useRef(null);
  const remoteRef = useRef<Array<HTMLElement | string>>([]);
  const [users, setUsers] = useState<Array<IRemoteUser>>([]);
  const [prevSrc, setPrevSrc] = useState<number | null>(null);
  const [mute, setMute] = useState<{
    voice: boolean;
    video: boolean;
  }>({
    voice: true,
    video: true,
  });
  const agoraEngine = AgoraRTC.createClient({ mode: 'rtc', codec: 'vp8' });
  const agoraRole = role === 'HOST' ? 'host' : 'audience';

  async function start() {
    agoraEngine.on('user-published', async (user, mediaType) => {
      const existedUser = remoteUsers.find((u) => u.remoteUid === user.uid);
      await agoraEngine.subscribe(user, mediaType);
      if (!existedUser) {
        const newUser: IRemoteUser = {
          remoteUid: user.uid,
          remoteAudioTrack: user.audioTrack,
          remoteVideoTrack: user.videoTrack,
        };
        newUser.remoteAudioTrack?.play();
        remoteRef.current.push('');
        remoteUsers.push(newUser);
        setUsers((prev) => [...prev, newUser]);
      } else {
        existedUser.remoteAudioTrack = user.audioTrack;
        existedUser.remoteAudioTrack?.play();
        existedUser.remoteVideoTrack = user.videoTrack;
      }
    });

    agoraEngine.on('user-unpublished', (user) => {
      const index = remoteUsers.findIndex((u) => u.remoteUid === user.uid);
      if (index === -1) {
        return;
      }
      const newRemoteUsers = remoteUsers.filter(
        (u) => u.remoteUid !== user.uid
      );
      setUsers(newRemoteUsers);
      remoteRef.current.splice(index, 1);
      remoteUsers.splice(index, 1);
    });
  }
  const mount = async () => {
    const channel = id;
    try {
      const newToken = await fetchToken(channel, agoraRole, options.uid);
      await agoraEngine.join(options.appId, channel, newToken, options.uid);
      channelParameters.localAudioTrack =
        await AgoraRTC.createMicrophoneAudioTrack();
      channelParameters.localVideoTrack =
        await AgoraRTC.createCameraVideoTrack();
      await agoraEngine.publish([
        channelParameters.localAudioTrack,
        channelParameters.localVideoTrack,
      ]);
      channelParameters.localAudioTrack.setEnabled(false);
      if (localRef.current) {
        channelParameters.localVideoTrack.play(localRef.current);
        channelParameters.localVideoTrack.setEnabled(false);
      }
    } catch (err) {}
  };
  const stopHost = (client: IAgoraRTCClient) => {
    if (!client.channelName) {
      return;
    }
    client.unpublish();
    client.remoteUsers.forEach((user: IAgoraRTCRemoteUser) => {
      client.unsubscribe(user);
    });
    client.removeAllListeners();
  };

  const stopAudience = (client: IAgoraRTCClient) => {
    if (!client.channelName) {
      return;
    }
    client.remoteUsers.forEach((user: IAgoraRTCRemoteUser) => {
      client.unsubscribe(user);
    });
    client.removeAllListeners();
  };

  const unmount = async () => {
    try {
      channelParameters.localAudioTrack?.close();
      channelParameters.localVideoTrack?.close();
      if (agoraRole === 'host') {
        stopHost(agoraEngine);
      } else {
        stopAudience(agoraEngine);
      }
      await agoraEngine.leave();
    } catch (err) {
      dispatch(
        showNotification({
          msg: err as string,
          status: 'error',
        })
      );
    }
  };

  const setPrevVideo = () => {
    if (prevSrc === null) {
      return;
    }
    if (prevSrc === -1 && localRef.current) {
      channelParameters.localVideoTrack?.play(localRef.current);
    } else {
      if (remoteRef.current[prevSrc]) {
        remoteUsers[prevSrc].remoteVideoTrack?.play(remoteRef.current[prevSrc]);
      }
    }
  };

  const pinLocalVideo = () => {
    setPrevVideo();
    if (mainVideoRef.current) {
      channelParameters.localVideoTrack?.play(mainVideoRef.current);
      setPrevSrc(-1);
    }
  };

  const unpinMainVideo = () => {
    if (prevSrc === null || !localRef.current) {
      return;
    }
    if (prevSrc === -1) {
      channelParameters.localVideoTrack?.play(localRef.current);
    } else {
      if (remoteRef.current[prevSrc]) {
        remoteUsers[prevSrc].remoteVideoTrack?.play(remoteRef.current[prevSrc]);
      }
    }
    setPrevSrc(null);
  };

  const toggleVoice = async () => {
    if (mute.voice) {
      // eslint-disable-next-line no-undef
      const microphone = 'microphone' as PermissionName;
      navigator.permissions
        .query({ name: microphone })
        .then(async function (result) {
          if (result.state === 'granted') {
            channelParameters.localAudioTrack?.setEnabled(true);
          } else if (result.state === 'denied') {
            try {
              channelParameters.localAudioTrack =
                await AgoraRTC.createMicrophoneAudioTrack();
              await agoraEngine.publish(channelParameters.localAudioTrack);
            } catch (err) {
              dispatch(
                showNotification({
                  msg: 'Please allow use the microphone on the browser and reload page.',
                  status: 'info',
                })
              );
              return;
            }
          }
          setMute({ ...mute, voice: false });
        }).catch(() => {
          dispatch(
            showNotification({
              msg: 'There are some errors when using the microphone.',
              status: 'error',
            }));
        });
    } else {
      channelParameters.localAudioTrack?.setEnabled(false);
      setMute({ ...mute, voice: true });
    }
  };

  const toggleVideo = async () => {
    if (mute.video) {
      // eslint-disable-next-line no-undef
      const camera = 'camera' as PermissionName;
      navigator.permissions
        .query({ name: camera })
        .then(async function (result) {
          if (result.state === 'granted') {
            channelParameters.localVideoTrack?.setEnabled(true);
          } else if (result.state === 'denied') {
            try {
              channelParameters.localVideoTrack =
                await AgoraRTC.createCameraVideoTrack();
              await agoraEngine.publish(channelParameters.localVideoTrack);
            } catch (err) {
              dispatch(
                showNotification({
                  msg: 'Please allow use the camera on the browser and reload page.',
                  status: 'info',
                })
              );
              return;
            }
          }
          setMute({ ...mute, video: false });
        })
        .catch(() => {
          dispatch(
            showNotification({
              msg: 'There are some errors when using the video.',
              status: 'error',
            }));
        });
    } else {
      channelParameters.localVideoTrack?.setEnabled(false);
      setMute({ ...mute, video: true });
    }
  };

  useEffect(() => {
    start();
    mount();

    return () => {
      unmount();
    };
  }, []);

  useEffect(() => {
    remoteUsers.forEach((u, index) => {
      if (remoteRef.current[index] !== null) {
        u.remoteVideoTrack?.play(remoteRef.current[index]);
      }
    });
  }, [remoteUsers.length]);

  return (
    <div className={`agora-component ${!show && 'hide'}`}>
      <div
        className="main-video"
        onClick={unpinMainVideo}
        ref={mainVideoRef}
      ></div>
      <div className="list-video">
        <div className="video" ref={localRef} onClick={pinLocalVideo}></div>
        {users.map((u, index) => (
          <div
            ref={(el) => {
              if (el) {
                remoteRef.current[index] = el;
              }
            }}
            key={index}
            className="video"
            onClick={() => {
              setPrevVideo();
              if (mainVideoRef.current) {
                remoteUsers[index].remoteVideoTrack?.play(mainVideoRef.current);
                setPrevSrc(index);
              }
            }}
          ></div>
        ))}
      </div>
      <AgoraActions
        voiceMute={mute.voice}
        voiceBtnClick={toggleVoice}
        videoMute={mute.video}
        videoBtnClick={toggleVideo}
      />
    </div>
  );
};

export default Agora;
