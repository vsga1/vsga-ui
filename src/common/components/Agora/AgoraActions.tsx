import React, { useState } from 'react';
import {
  BsMicFill,
  BsMicMuteFill,
  BsFillCameraVideoFill,
  BsFillCameraVideoOffFill,
  BsChevronDown,
  BsChevronUp,
} from 'react-icons/bs';
import { Button } from 'react-bootstrap';
import { Tooltip } from 'react-tooltip';

type Props = {
  voiceMute: boolean;
  voiceBtnClick: () => Promise<void>;
  videoMute: boolean;
  videoBtnClick: () => Promise<void>;
};

function RoomActions({
  voiceMute,
  voiceBtnClick,
  videoMute,
  videoBtnClick,
}: Props) {
  const [isCollapse, setIsCollapse] = useState(true);

  const toggleCollapse = () => {
    setIsCollapse(!isCollapse);
  };

  return (
    <div className={`room-actions d-flex ${isCollapse && 'collapse'}`}>
      <Tooltip id="collapse-btn" className='d-none d-md-block'/>
      <div
        className="collapse-btn d-flex"
        data-tooltip-content={isCollapse ? 'Room actions' : 'Collapse'}
        onClick={toggleCollapse}
        data-tooltip-id="collapse-btn"
        data-tooltip-place="top"
      >
        {isCollapse ? (
          <BsChevronUp height="16px" color="white" />
        ) : (
          <BsChevronDown height="16px" color="white" />
        )}
      </div>
      <Tooltip id="btn-feature-voice" className='d-none d-md-block'/>
      <Button
        data-tooltip-id="btn-feature-voice"
        data-tooltip-content={voiceMute ? 'Turn on voice' : 'Turn off voice'}
        data-tooltip-place="top"
        onClick={voiceBtnClick}
      >
        {voiceMute ? <BsMicMuteFill /> : <BsMicFill />}
      </Button>
      <Tooltip id="btn-feature-camera" className='d-none d-md-block'/>
      <Button
        data-tooltip-id="btn-feature-camera"
        data-tooltip-content={videoMute ? 'Turn on camera' : 'Turn off camera'}
        data-tooltip-place="top"
        onClick={videoBtnClick}
      >
        {videoMute ? <BsFillCameraVideoOffFill /> : <BsFillCameraVideoFill />}
      </Button>
    </div>
  );
}

export default RoomActions;
