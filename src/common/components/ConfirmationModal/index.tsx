import React from 'react'
import { Button, Modal } from 'react-bootstrap';

interface ConfirmationModalProps {
  title: string;
  children: React.ReactNode;
  confirmationText: string;
  isShowModal: boolean;
  closeModal: () => void;
  handleConfirm: () => void;
}

const ConfirmationModal: React.FC<ConfirmationModalProps> = ({ 
  title, 
  children, 
  confirmationText, 
  isShowModal, 
  closeModal,
  handleConfirm
}) => {
  return (
    <Modal 
      centered
      show={isShowModal} 
      onHide={closeModal}
    >
      <Modal.Header>
        <Modal.Title>
          <h3 className="text-primary">{title}</h3>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {children} 
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" onClick={closeModal}>
          Cancel
        </Button>
        <Button variant="secondary" onClick={handleConfirm}>
          {confirmationText}
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ConfirmationModal;
