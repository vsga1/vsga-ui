import React, { useState } from 'react';
import { TaskType } from '@/common/utils/enums';
import { Button } from 'react-bootstrap';
import { FaChevronDown } from 'react-icons/fa';
import CreateTask from './CreateTask';
import InputTask from './InputTask';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { addTask, selectPersonalTasks } from '@/store/slice/taskSlice';
import TaskItem from './TaskItem';
import { useRouter } from 'next/router';
import { postAPI } from '@/common/utils/customAPI';
import { showNotification } from '@/store/slice/appSlice';

const PersonalTask = () => {
  const dispatch = useAppDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [showCreateTaskField, setShowCreateTaskField] = useState(false);
  const tasks = useAppSelector(selectPersonalTasks);
  const router = useRouter();

  const { id: roomCode } = router.query;

  const handleOpen = () => {
    setIsOpen(!isOpen);
  };

  const onShowCreateTask = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setShowCreateTaskField(true);
  };

  const onHideCreateTask = (event: React.MouseEvent<HTMLDivElement>) => {
    if (event.target === event.currentTarget) {
      setShowCreateTaskField(false);
    }
  };

  const createTask = async (description: string) => {
    const task = {
      description,
      room_code: roomCode,
      type: TaskType.PERSONAL,
    };
    const res = await postAPI('/v1/task', task);
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    dispatch(addTask(res.data));
    setShowCreateTaskField(false);
  };

  return (
    <div className={`task-group ${!isOpen && 'close'}`}>
      <div
        className="header p-2 d-flex align-items-center"
        onClick={handleOpen}
      >
        <Button className="icon expand-btn">
          <FaChevronDown className={`${!isOpen && 'close'}`} />
        </Button>
        My tasks
      </div>
      <div
        className={`content ${!isOpen && 'close'}`}
        onClick={onHideCreateTask}
      >
        <CreateTask onCreate={onShowCreateTask} />
        {showCreateTaskField && <InputTask onSubmit={createTask} />}
        {tasks.map((item, index) => (
          <TaskItem key={index} task={item} type="PERSONAL" />
        ))}
      </div>
    </div>
  );
};

export default PersonalTask;
