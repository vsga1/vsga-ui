import React, { useCallback, useEffect } from 'react';
import { getAPI, postAPI } from '@/common/utils/customAPI';
import RoomTask from './RoomTask';
import PersonalTask from './PersonalTask';
import { initTask } from '@/store/slice/taskSlice';
import { showNotification } from '@/store/slice/appSlice';
import { useRouter } from 'next/router';
import { useAppDispatch } from '@/hooks';
import { TaskType } from '@/common/utils/enums';
import { TaskResponse } from '@/common/utils/interface';

const Task = () => {
  const router = useRouter();
  const { id } = router.query;
  const dispatch = useAppDispatch();

  const getTasks = useCallback(async () => {
    if (!router.isReady) return;

    const getUsersDoneTask = async (tasks: TaskResponse[]) => {
      const result = [];
      for (const task of tasks) {
        const res = await postAPI('/v1/task/done', {
          room_code: task.room_code,
          scheme: task.scheme,
        });
        if (res.code !== 200) {
          dispatch(
            showNotification({
              msg: res.data,
              status: 'error',
            })
          );
          result.push({ ...task, users: [] });
        } else {
          result.push({ ...task, users: res.data.members });
        }
      }
      return result;
    };

    const res = await getAPI(`/v1/task?room_code=${id}`);
    if (res.code === 200) {
      const personalTasks = res.data.filter(
        (t: TaskResponse) => t.type === TaskType.PERSONAL
      );
      let roomTasks = res.data.filter(
        (t: TaskResponse) => t.type === TaskType.ROOM
      );
      roomTasks = await getUsersDoneTask(roomTasks);
      dispatch(initTask([...personalTasks, ...roomTasks]));
    } else {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
    }
  }, [dispatch, router.isReady, id]);

  useEffect(() => {
    getTasks();
  }, [getTasks]);

  return (
    <div id="tasks">
      <RoomTask />
      <PersonalTask />
    </div>
  );
};

export default Task;
