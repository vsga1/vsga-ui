import React from 'react';
import { Button } from 'react-bootstrap';
import { MdAddCircle } from 'react-icons/md';

interface Props {
  onCreate: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const CreateTask = ({ onCreate }: Props) => {
  return (
    <div className="item-create-task p-2 d-flex align-items-center justify-content-start">
      <Button onClick={onCreate} className="btn-create-task">
        <MdAddCircle className="mr-2" />
        Create task
      </Button>
    </div>
  );
};

export default CreateTask;
