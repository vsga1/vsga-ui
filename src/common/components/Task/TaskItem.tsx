import React, { useContext, useState } from 'react';
import { Role, TaskStatus } from '@/common/utils/enums';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { doneTask, editTask, removeTask, Task } from '@/store/slice/taskSlice';
import { Dropdown, Form } from 'react-bootstrap';
import { MdMoreVert } from 'react-icons/md';
import { showNotification } from '@/store/slice/appSlice';
import { useRouter } from 'next/router';
import { putAPI } from '@/common/utils/customAPI';
import { SocketContext } from '@/context/socket';
import {
  DONE_TASK,
  EDIT_TASK,
  REMOVE_TASK,
} from '@/common/utils/constants/event';
import { selectUserInfo } from '@/store/slice/userSlice';
import UserDoneTask from './UserDoneTask';
import { taskAuthorization } from '@/common/utils/roomAuthorization';
import useTheme from '@/hooks/useTheme';
interface Props {
  task: Task;
  type: 'ROOM' | 'PERSONAL';
  role?: Role;
}

const TaskItem = ({ task, type, role }: Props) => {
  const theme = useTheme();
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUserInfo);
  const [mode, setMode] = useState<'EDIT' | 'NORMAL'>('NORMAL');
  const [valueEdit, setValueEdit] = useState(task.description);
  const router = useRouter();
  const { id: roomCode } = router.query;
  const socket = useContext(SocketContext);

  const handleChangeStatus = async () => {
    if (task.status === TaskStatus.DONE) {
      return;
    }
    const newTask = {
      code: task.code,
      description: task.description,
      room_code: roomCode,
      status: TaskStatus.DONE,
      type: task.type,
    };
    const res = await putAPI('/v1/task', newTask);
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    if (type === 'ROOM') {
      socket.emit(DONE_TASK, {
        scheme: task.scheme,
        username: user.username,
        avatar: user.avatar_img_url,
        code: task.code,
        roomCode,
      });
    }
    dispatch(doneTask(task.code));
  };

  const handleDeleteTask = async () => {
    const res = await putAPI('/v1/task', {
      code: task.code,
      description: task.description,
      room_code: roomCode,
      status: TaskStatus.DELETED,
      type: task.type,
      scheme: task.scheme,
    });
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    if (type === 'ROOM') {
      socket.emit(REMOVE_TASK, { scheme: task.scheme, roomCode });
    }
    dispatch(removeTask(task.code));
  };

  const changeEditMode = () => {
    const canEdit = type === 'PERSONAL' || taskAuthorization(role);
    if (canEdit) {
      setValueEdit(task.description);
      setMode('EDIT');
    }
  };

  const onEditTask = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValueEdit(e.target.value);
  };

  const submitEditTask = async () => {
    if (valueEdit === '') {
      dispatch(
        showNotification({
          msg: 'Please enter description',
          status: 'info',
        })
      );
      return;
    }
    if (task.description === valueEdit) {
      setMode('NORMAL');
      return;
    }
    const newTask = {
      code: task.code,
      description: valueEdit,
      room_code: roomCode,
      status: task.status,
      scheme: task.scheme,
      type: task.type,
    };
    const res = await putAPI('/v1/task', newTask);
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    if (type === 'ROOM') {
      socket.emit(EDIT_TASK, { task: res.data });
    }
    dispatch(
      editTask({ code: newTask.code, description: newTask.description })
    );
    setMode('NORMAL');
  };

  const optionMenu = [
    {
      name: 'Delete',
      show: type === 'PERSONAL' || taskAuthorization(role),
      onClick: handleDeleteTask,
    },
  ];

  const isShowOption = optionMenu.filter((i) => i.show).length > 0;

  return (
    <div className="task-item">
      <div className="d-flex align-items-center">
        <Form.Check
          className="task-checkbox mr-2"
          checked={task.status === TaskStatus.DONE}
          onChange={handleChangeStatus}
          readOnly={task.status === TaskStatus.DONE}
        />
        {mode === 'NORMAL' && (
          <span className="task-description" onClick={changeEditMode}>
            {task.description}
          </span>
        )}
        {mode === 'EDIT' && (
          <Form.Control
            autoFocus
            value={valueEdit}
            onChange={onEditTask}
            onBlur={submitEditTask}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                submitEditTask();
              }
            }}
          />
        )}
      </div>
      <div className="d-flex align-items-center">
        <UserDoneTask users={task.users} />
        {isShowOption && (
          <Dropdown className="icon">
            <Dropdown.Toggle className="icon circle">
              <MdMoreVert />
            </Dropdown.Toggle>
            <Dropdown.Menu {...(theme === 'dark' ? {variant: 'dark'} : {})}>
              {optionMenu.map(
                (item, index) =>
                  item.show && (
                    <Dropdown.Item
                      as="button"
                      key={index}
                      onClick={item.onClick}
                    >
                      {item.name}
                    </Dropdown.Item>
                  )
              )}
            </Dropdown.Menu>
          </Dropdown>
        )}
      </div>
    </div>
  );
};

export default TaskItem;
