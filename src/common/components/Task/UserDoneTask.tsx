import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import React from 'react';
import { Tooltip } from 'react-tooltip';

type Props = {
  users?: Array<{
    username: string;
    avatar_img_url: string;
  }>;
};

const UserDoneTask = ({ users }: Props) => {
  if (!users || users.length === 0) {
    return <></>;
  }

  const list = [...users];
  const firstThreeUsers = list.splice(0, 3);

  return (
    <div className="d-flex users-done-task">
      {firstThreeUsers.map((u, index) => {
        return (
          <div key={index} className="user-image" data-tooltip-content={u.username}>
            <img
              src={u.avatar_img_url ? u.avatar_img_url : DEFAULT_AVATAR_PATH}
              alt="img"
              style={{ width: '100%', height: '100%' }}
            />
          </div>
        );
      })}
      {list.length !== 0 && <div className="user-image">+{list.length}</div>}
      <Tooltip anchorSelect=".user-image" />
    </div>
  );
};

export default UserDoneTask;
