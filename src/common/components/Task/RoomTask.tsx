import React, { useContext, useState } from 'react';
import { TaskType } from '@/common/utils/enums';
import { Button } from 'react-bootstrap';
import { FaChevronDown } from 'react-icons/fa';
import CreateTask from './CreateTask';
import InputTask from './InputTask';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { addTask, selectRoomTasks } from '@/store/slice/taskSlice';
import { showNotification } from '@/store/slice/appSlice';
import TaskItem from './TaskItem';
import { SocketContext } from '@/context/socket';
import { CREATE_TASK } from '@/common/utils/constants/event';
import { postAPI } from '@/common/utils/customAPI';
import { useRouter } from 'next/router';
import { selectRole } from '@/store/slice/roomSlice';
import { taskAuthorization } from '@/common/utils/roomAuthorization';

const RoomTask = () => {
  const dispatch = useAppDispatch();
  const [isOpen, setIsOpen] = useState(false);
  const [showCreateTaskField, setShowCreateTaskField] = useState(false);
  const role = useAppSelector(selectRole);
  const tasks = useAppSelector(selectRoomTasks);
  const socket = useContext(SocketContext);
  const router = useRouter();
  const { id: roomCode } = router.query;

  const handleOpen = () => {
    setIsOpen(!isOpen);
  };

  const onShowCreateTask = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    setShowCreateTaskField(true);
  };

  const onHideCreateTask = (event: React.MouseEvent<HTMLDivElement>) => {
    if (event.target === event.currentTarget) {
      setShowCreateTaskField(false);
    }
  };

  const createTask = async (description: string) => {
    const task = {
      description,
      room_code: roomCode,
      type: TaskType.ROOM,
    };
    const res = await postAPI('/v1/task', task);
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    socket.emit(CREATE_TASK, { task: res.data });
    dispatch(addTask(res.data));
    setShowCreateTaskField(false);
  };

  const showCreateTaskBtn = taskAuthorization(role);

  return (
    <div className={`task-group ${!isOpen && 'close'}`}>
      <div
        className="header p-2 d-flex align-items-center"
        onClick={handleOpen}
      >
        <Button className="icon expand-btn">
          <FaChevronDown className={`${!isOpen && 'close'}`} />
        </Button>
        Room tasks
      </div>
      <div
        className={`content ${!isOpen && 'close'}`}
        onClick={onHideCreateTask}
      >
        {showCreateTaskBtn && <CreateTask onCreate={onShowCreateTask} />}
        {showCreateTaskField && <InputTask onSubmit={createTask} />}
        {tasks.map((item, index) => (
          <TaskItem key={index} task={item} type="ROOM" role={role} />
        ))}
      </div>
    </div>
  );
};

export default RoomTask;
