import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { MdOutlineAddCircleOutline } from 'react-icons/md';

interface Props {
  onSubmit: (description: string) => void;
}

const InputTask = ({ onSubmit }: Props) => {
  const [description, setDescription] = useState('');
  const dispatch = useAppDispatch();

  const createTask = () => {
    if (description === '') {
      dispatch(
        showNotification({
          msg: 'Please enter description',
          status: 'info',
        })
      );
      return;
    }
    onSubmit(description);
  };

  return (
    <div className="input-task p-2 pl-3 d-flex justify-content-center">
      <div className="w-100 d-flex flex-column">
        <Form.Control
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
          autoFocus
          placeholder="Description"
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              createTask();
            }
          }}
        />
      </div>
      <Button onClick={createTask} className="icon circle ml-2">
        <MdOutlineAddCircleOutline />
      </Button>
    </div>
  );
};

export default InputTask;
