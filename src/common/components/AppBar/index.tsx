import React, { useContext, useEffect, useState } from 'react';
import Link from 'next/link';
import {
  MdLightMode,
  MdDarkMode,
  MdOutlineLogout,
} from 'react-icons/md';
import { Button, Dropdown } from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { selectUser } from '@/store/slice/userSlice';
import { selectAppTheme, setAppTheme } from '@/store/slice/appSlice';
import { RiMenu2Line } from 'react-icons/ri';
import { SidebarContext } from '@/layouts/MainLayout';
import { signOut } from 'next-auth/react';
import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import NotificationButton from './NotificationButton';
import { FaUserAlt, FaUserFriends } from 'react-icons/fa';

const AppBar = () => {
  const user = useAppSelector(selectUser);
  const theme = useAppSelector(selectAppTheme);
  const dispatch = useAppDispatch();
  const { primarySidebar, secondarySidebar } = useContext(SidebarContext);
  const [avatar, setAvatar] = useState<string>(DEFAULT_AVATAR_PATH);
  const handleToggleTheme = () => {
    theme === 'light'
      ? dispatch(setAppTheme('dark'))
      : dispatch(setAppTheme('light'));
  };

  useEffect(() => {
    if (
      user.status === 'existed' &&
      typeof user.data.avatar_img_url === 'string'
    ) {
      setAvatar(user.data.avatar_img_url);
    }
  }, [user]);

  return (
    <div
      id="app-bar"
      className={`d-flex align-items-center justify-content-between justify-content-md-end ${theme}`}
    >
      <div className="action-buttons d-flex align-items-center d-block d-md-none">
        <Button
          className="icon outline secondary md border-0"
          onClick={primarySidebar.openSidebar}
        >
          <RiMenu2Line />
        </Button>
      </div>
      <div className="action-buttons d-flex align-items-center">
        <Button
          className="icon outline secondary md border-0"
          onClick={handleToggleTheme}
        >
          {theme === 'light' ? <MdDarkMode /> : <MdLightMode />}
        </Button>
        <Button
          className="icon outline secondary md border-0 d-xl-none d-inline-flex"
          onClick={secondarySidebar.openSidebar}
        >
          <FaUserFriends />
        </Button>
        <NotificationButton />
        <Dropdown className="avatar" align="end">
          <Dropdown.Toggle className="icon outline secondary avatar-btn">
            <img src={avatar} alt="avatar" width={36} height={36} />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <div className="header">
              <h6>Hello, {user.data.username || 'User'}</h6>
            </div>
            <Dropdown.Divider />
            <div className="nav">
              <Link href="/u/profile">
                <FaUserAlt />
                Profile
              </Link>
              <Dropdown.Item
                as="button"
                onClick={() => signOut({ callbackUrl: '/login' })}
              >
                <MdOutlineLogout />
                Logout
              </Dropdown.Item>
            </div>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </div>
  );
};

export default AppBar;
