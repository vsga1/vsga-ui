import {
  selectNotificationPageInfo,
  selectNumberNewNotifications,
  setFirstOfListNotification,
  updateNotificationPageInfo,
} from '@/store/slice/notificationSlice';
import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { FaBell } from 'react-icons/fa';
import Alert from '../Alert';
import { getAPI } from '@/common/utils/customAPI';
import { useAppDispatch, useAppSelector } from '@/hooks';

const NotificationButton = () => {
  const dispatch = useAppDispatch();
  const numberOfNewNotification = useAppSelector(selectNumberNewNotifications);
  const pageInfo = useAppSelector(selectNotificationPageInfo);

  const fetchFirstNotifications = async (isOpen: boolean) => {
    if (!isOpen || pageInfo.next !== 0) {
      return;
    }
    const res = await getAPI('/v1/notification', {
      limit: pageInfo.limit,
    });
    if (res.code === 200) {
      dispatch(setFirstOfListNotification(res.data));
      dispatch(
        updateNotificationPageInfo({
          ...pageInfo,
          total: res.meta.total,
          next: pageInfo.next + 1,
        })
      );
    }
  };

  return (
    <Dropdown
      onToggle={fetchFirstNotifications}
      className="notifications icon"
      align="end"
    >
      <Dropdown.Toggle className="icon outline secondary border-0">
        <FaBell />
        {numberOfNewNotification !== 0 && (
          <span className="new-notification d-flex align-items-center justify-content-center position-absolute top-0 text-white bg-danger">
            {numberOfNewNotification < 10 ? numberOfNewNotification : '9\u207A'}
          </span>
        )}
      </Dropdown.Toggle>
      <Dropdown.Menu className="notification-list">
        <Alert />
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default NotificationButton;
