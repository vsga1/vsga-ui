import React, { useEffect, useCallback, useState } from 'react';
import { putAPI } from '@/common/utils/customAPI';
import { Row, Col, Button, Form, Spinner } from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import userInfoSchema from '@/common/utils/yupSchema/userInfoForm';
import { selectUser } from '@/store/slice/userSlice';
import UpdateAvatar from './UpdateAvatar';
import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';

interface UserInfoForm {
  full_name: string;
  address: string;
  gender: string;
  age: number;
  description: string;
  display_name: string;
}

const Profile = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    reset,
    setFocus,
    formState: { errors },
  } = useForm<UserInfoForm>({
    resolver: yupResolver(userInfoSchema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const { status, data: info } = useAppSelector(selectUser);

  const handleChangeInfo = handleSubmit(async (data, event) => {
    event?.preventDefault();
    setIsLoading(true);
    const res = await putAPI('/v1/account', { ...data });
    if (res.code === 200) {
      dispatch(
        showNotification({
          msg: 'Update successfully!',
          status: 'info',
        })
      );
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    setIsLoading(false);
  });

  const setFormValue = useCallback(() => {
    reset(info);
  }, [reset, info]);

  const focusNextField = (
    e: React.KeyboardEvent<HTMLElement>,
    nextField: keyof UserInfoForm
  ) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      setFocus(nextField);
    }
  };

  useEffect(() => {
    if (status === 'existed') {
      setFormValue();
    }
  }, [status, setFormValue]);

  return (
    <div id="setting-profile">
      <Row>
        <Col md={6} lg={4} className="mb-3">
          <div className="setting-card">
            <div className="header">Avatar</div>
            <div className="body">
              <div className="d-flex align-items-center justify-content-center flex-column">
                <div className="avatar">
                  <img
                    src={
                      info.avatar_img_url || DEFAULT_AVATAR_PATH
                    }
                    alt="avatar"
                    width={150}
                    height={150}
                  />
                </div>
                <p className="my-2 text-primary">Upload/Change your avatar</p>
                <UpdateAvatar />
              </div>
            </div>
          </div>
        </Col>
        <Col md={6} lg={8} className="mb-3">
          <div className="setting-card">
            <div className="header">Information</div>
            <div className="body">
              <Form className="form" onSubmit={handleChangeInfo} noValidate>
                <Row className="mb-1">
                  <Col md={12} lg={6}>
                    <Form.Label>Username</Form.Label>
                    <Form.Control value={info.username} readOnly disabled type="text" />
                    <Form.Control.Feedback
                      type="invalid"
                      className="error"
                    ></Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Email</Form.Label>
                    <Form.Control value={info.email} readOnly disabled type="text" />
                    <Form.Control.Feedback
                      type="invalid"
                      className="error"
                    ></Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Full name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter full name"
                      {...register('full_name', {
                        onChange: () => {
                          clearErrors('full_name');
                        },
                      })}
                      isInvalid={!!errors.full_name}
                      onKeyDown={(e) => focusNextField(e, 'display_name')}
                      disabled={isLoading}
                    />
                    <Form.Control.Feedback type="invalid" className="error">
                      {errors.full_name?.message}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Display name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter display name"
                      {...register('display_name', {
                        onChange: () => {
                          clearErrors('display_name');
                        },
                      })}
                      isInvalid={!!errors.display_name}
                      onKeyDown={(e) => focusNextField(e, 'description')}
                      disabled={isLoading}
                    />
                    <Form.Control.Feedback type="invalid" className="error">
                      {errors.display_name?.message}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter description"
                      {...register('description', {
                        onChange: () => {
                          clearErrors('description');
                        },
                      })}
                      isInvalid={!!errors.description}
                      onKeyDown={(e) => focusNextField(e, 'address')}
                      disabled={isLoading}
                    />
                    <Form.Control.Feedback type="invalid" className="error">
                      {errors.description?.message}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter address"
                      {...register('address', {
                        onChange: () => {
                          clearErrors('address');
                        },
                      })}
                      isInvalid={!!errors.address}
                      onKeyDown={(e) => focusNextField(e, 'gender')}
                      disabled={isLoading}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.address?.message}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Gender</Form.Label>
                    <Form.Select
                      placeholder="Select gender"
                      {...register('gender')}
                      defaultValue=""
                      disabled={isLoading}
                    >
                      <option value="MALE">Male</option>
                      <option value="FEMALE">Female</option>
                    </Form.Select>
                    <div className="error invalid-feedback"></div>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>Age</Form.Label>
                    <Form.Control
                      placeholder="Enter your age"
                      {...register('age')}
                      type="number"
                      disabled={isLoading}
                      min={1}
                      max={100}
                    />
                    <div className="error invalid-feedback">
                      {errors.age?.message || ''}
                    </div>
                  </Col>
                </Row>
                <div className="d-flex align-items-center justify-content-end">
                  <Button
                    variant="secondary"
                    className="text-uppercase"
                    type="submit"
                    disabled={isLoading}
                  >
                    <Spinner
                      animation="border"
                      size="sm"
                      className={!isLoading ? 'd-none' : 'me-2'}
                    />
                    Change
                  </Button>
                </div>
              </Form>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Profile;
