import React, { useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Form, InputGroup, Spinner } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import enterPasswordSchema from '@/common/utils/yupSchema/enterRoomPasswordForm';
import { FiEyeOff, FiEye } from 'react-icons/fi';
import { postAPI } from '@/common/utils/customAPI';
import { useDispatch } from 'react-redux';
import { showNotification } from '@/store/slice/appSlice';
import { Role } from '@/common/utils/enums';
import Link from 'next/link';

type Props = {
  code: string;
  callback: (role: Role) => Promise<void>;
};

interface EnterPasswordForm {
  password: string;
}

const JoinRoom = ({ code, callback }: Props) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<EnterPasswordForm>({
    resolver: yupResolver(enterPasswordSchema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });
  const [loading, setLoading] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const onSubmit = handleSubmit(async (data, event) => {
    event?.preventDefault();
    setLoading(true);
    const res = await postAPI('/v1/event/join-room', {
      room_code: code,
      password: data.password,
    });
    setLoading(false);
    if (res.code !== 200) {
      dispatch(
        showNotification({
          msg: res.data,
          status: 'error',
        })
      );
      return;
    }
    callback(res.data.role);
  });

  return (
    <Form className="form" onSubmit={onSubmit}>
      <InputGroup className="mb-2">
        <Form.Control
          {...register('password', {
            onChange: () => {
              clearErrors('password');
            },
          })}
          placeholder="Room's password"
          isInvalid={!!errors.password}
          autoFocus
          type={isShow ? 'text' : 'password'}
        />
        <InputGroup.Text
          onClick={() => setIsShow(!isShow)}
          className="toggle-password rounded-end"
        >
          {isShow ? <FiEyeOff /> : <FiEye />}
        </InputGroup.Text>
      </InputGroup>
      <Form.Control.Feedback className="error" type="invalid">
        {errors.password?.message}
      </Form.Control.Feedback>
      <div className="d-flex justify-content-end mt-2">
        <Link href="/home">
          <Button className="me-2 rounded" variant="light" disabled={loading}>
            <Spinner
              animation="border"
              size="sm"
              className={!loading ? 'd-none' : 'me-2'}
            />
            Go back
          </Button>
        </Link>
        <Button variant="secondary" type="submit" disabled={loading}>
          <Spinner
            animation="border"
            size="sm"
            className={!loading ? 'd-none' : 'me-2'}
          />
          Join
        </Button>
      </div>
    </Form>
  );
};

export default JoinRoom;
