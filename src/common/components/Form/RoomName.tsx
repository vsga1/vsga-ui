import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { Form, Button } from 'react-bootstrap';
import roomNameSchema from '@/common/utils/yupSchema/roomNameForm';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { putAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { SocketContext } from '@/context/socket';
import { EVENT } from '@/common/utils/constants';

interface CreateFormData {
  name: string;
}

type Props = {
  roomName?: string;
  closeModal: () => void;
};

const RoomName = ({ roomName, closeModal }: Props) => {
  const socket = useContext(SocketContext);
  const {
    register,
    handleSubmit,
    clearErrors,
    formState: { errors },
  } = useForm<CreateFormData>({
    resolver: yupResolver(roomNameSchema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
    defaultValues: {
      name: roomName,
    },
  });
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { id } = router.query;

  const onSubmit = handleSubmit(async ({ name }) => {
    const res = await putAPI(`/v1/room/${id}`, {
      name,
    });
    if (res.code === 200) {
      // dispatch(setRoomInfo({ ...(room as Room), name }));
      socket.emit(EVENT.UPDATE_ROOM_NAME, { roomCode: id, name });
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    closeModal();
  });

  return (
    <Form className="form" onSubmit={onSubmit}>
      <Form.Group>
        <Form.Label>Room Name*</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter room's name"
          {...register('name', {
            onChange: () => {
              clearErrors('name');
            },
          })}
          isInvalid={!!errors.name}
        />
        <Form.Control.Feedback type="invalid" className="error">
          {errors.name?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <div className="d-flex justify-content-end mt-4">
        <Button
          variant="secondary"
          type="submit"
          className="submit text-uppercase"
        >
          Save
        </Button>
      </div>
    </Form>
  );
};

export default RoomName;
