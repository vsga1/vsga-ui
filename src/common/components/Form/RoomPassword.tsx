import React from 'react';
import { useRouter } from 'next/router';
import { Form, Button } from 'react-bootstrap';
import roomPasswordSchema from '@/common/utils/yupSchema/roomPasswordForm';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { putAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';

interface UpdatePasswordForm {
  current_password: string;
  new_password: string;
}

type Props = {
  closeModal: () => void;
};

const RoomPassword = ({ closeModal }: Props) => {
  const {
    register,
    handleSubmit,
    clearErrors,
    setFocus,
    formState: { errors },
  } = useForm<UpdatePasswordForm>({
    resolver: yupResolver(roomPasswordSchema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { id } = router.query;

  const onSubmit = handleSubmit(async (data, event) => {
    event?.preventDefault();
    const res = await putAPI(`/v1/room/${id}/password`, { ...data });
    if (res.code === 200) {
      dispatch(
        showNotification({
          msg: 'Update password successfully!',
          status: 'info',
        })
      );
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    closeModal();
  });

  return (
    <Form className="form" onSubmit={onSubmit}>
      <Form.Group className="mb-2">
        <Form.Label>Current password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter current password"
          {...register('current_password', {
            onChange: () => {
              clearErrors('current_password');
            },
          })}
          isInvalid={!!errors.current_password}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              e.preventDefault();
              setFocus('new_password');
            }
          }}
        />
        <Form.Control.Feedback type="invalid" className="error">
          {errors.current_password?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group>
        <Form.Label>New password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter new password"
          {...register('new_password', {
            onChange: () => {
              clearErrors('new_password');
            },
          })}
          isInvalid={!!errors.new_password}
        />
        <Form.Control.Feedback type="invalid" className="error">
          {errors.new_password?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <div className="d-flex justify-content-end mt-4">
        <Button
          variant="secondary"
          type="submit"
          className="submit text-uppercase"
        >
          Save
        </Button>
      </div>
    </Form>
  );
};

export default RoomPassword;
