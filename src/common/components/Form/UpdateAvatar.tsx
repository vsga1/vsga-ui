import useModal from '@/hooks/useModal';
import React, { useRef, useState } from 'react';
import { Button, Modal, Spinner } from 'react-bootstrap';
import { showNotification } from '@/store/slice/appSlice';
import { useAppDispatch } from '@/hooks';
import ImageCropper from '../ImageCropper';
import { Area } from 'react-easy-crop';
import { cloudinaryUpload, postAPI } from '@/common/utils/customAPI';
import { setAvatar } from '@/store/slice/userSlice';
import useTheme from '@/hooks/useTheme';

const MAX_FILE_SIZE = 2 * 1024 * 1024;

const UpdateAvatar = () => {
  const dispatch = useAppDispatch();
  const cropModal = useModal();
  const inputFile = useRef<HTMLInputElement>(null);
  const [loading, setLoading] = useState(false);
  const [fileData, setFileData] = useState('');
  const theme = useTheme();

  const handleClickUpload = () => {
    if (inputFile.current) {
      inputFile.current.click();
    }
  };

  const inputChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (loading) {
      return;
    }
    const files = e.target.files;
    if (files) {
      if (files[0].size > MAX_FILE_SIZE) {
        dispatch(
          showNotification({
            msg: 'File size must not be bigger than 2MB!',
            status: 'error',
          })
        );
        return;
      }

      const reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload = function () {
        setFileData(reader.result as string);
      };

      const image = new Image();
      image.onload = function () {
        cropModal.openModal();
      };
      image.onerror = function () {
        setFileData('');
        dispatch(
          showNotification({
            msg: 'Invalid image!',
            status: 'error',
          })
        );
      };
      image.src = window.URL.createObjectURL(files[0]);
    }
    e.target.value = '';
  };

  const handleCropDone = (imgCroppedArea: Area | null) => {
    setLoading(true);
    if (!imgCroppedArea) return;
    const canvasEle = document.createElement('canvas');
    canvasEle.width = imgCroppedArea.width;
    canvasEle.height = imgCroppedArea.height;

    const context = canvasEle.getContext('2d');

    const imageObj1 = new Image();
    imageObj1.src = fileData;
    imageObj1.onload = function () {
      context?.drawImage(
        imageObj1,
        imgCroppedArea.x,
        imgCroppedArea.y,
        imgCroppedArea.width,
        imgCroppedArea.height,
        0,
        0,
        imgCroppedArea.width,
        imgCroppedArea.height
      );

      canvasEle.toBlob(async (data) => {
        if (!data) return;
        const formData = new FormData();
        formData.append('file', data);
        formData.append(
          'api_key',
          process.env.NEXT_PUBLIC_CLOUDINARY_API_KEY || ''
        );
        formData.append('upload_preset', 'vstudy');
        const res = await cloudinaryUpload(formData);
        if (res.code === 200) {
          const url = res.data.secure_url;
          const update = await postAPI('/v1/account/profile/avatar', { url });
          if (update.code === 200) {
            dispatch(setAvatar(url));
            dispatch(
              showNotification({
                msg: 'Update successfully!',
                status: 'info',
              })
            );
          } else {
            dispatch(
              showNotification({
                msg: res.data,
                status: 'error',
              })
            );
          }
        } else {
          dispatch(
            showNotification({
              msg: res.data,
              status: 'error',
            })
          );
        }
        cropModal.closeModal();
        setLoading(false);
      }, 'image/jpeg');
    };
  };

  return (
    <>
      <Modal
        centered
        show={cropModal.isShowModal}
        onHide={cropModal.closeModal}
        className="crop-modal"
      >
        <Modal.Header closeButton {...(theme === 'dark' ? {closeVariant: 'white'} : {})}>
          <Modal.Title>Upload Avatar</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ImageCropper
            image={fileData}
            onCropDone={handleCropDone}
            loading={loading}
          />
        </Modal.Body>
      </Modal>
      <Button
        variant="secondary"
        className="text-uppercase"
        onClick={handleClickUpload}
        disabled={loading}
      >
        {loading && <Spinner size="sm" className="me-2" />}
        Upload
      </Button>
      <input
        type="file"
        hidden
        ref={inputFile}
        onChange={inputChange}
        accept="image/png, image/jpg, image/jpeg"
      />
    </>
  );
};

export default UpdateAvatar;
