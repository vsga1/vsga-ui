/* eslint-disable indent */
import React, { useState } from 'react';
import { useRouter } from 'next/router';
import Select, { Options, StylesConfig } from 'react-select';
import AsyncCreatableSelect from 'react-select/async-creatable';
import { Form, Row, Col, Button, Spinner } from 'react-bootstrap';
import chroma from 'chroma-js';
import createFormSchema from '@/common/utils/yupSchema/createRoomForm';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { getAPI, postAPI } from '@/common/utils/customAPI';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { ROOM } from '@/common/utils/constants';
import { Topic, TopicResponse } from '@/common/utils/interface';
import { setRoomInfo } from '@/store/slice/roomSlice';
import useTheme from '@/hooks/useTheme';

interface CreateFormData {
  name: string;
  description: string;
  capacity: number;
  password: string;
  topics: Array<{
    value: string;
    label: string;
  }>;
  room_type: string;
}

const CreateRoom = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    control,
    formState: { errors },
  } = useForm<CreateFormData>({
    resolver: yupResolver(createFormSchema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });
  const dispatch = useAppDispatch();
  const router = useRouter();
  const theme = useTheme();
  const [selectedTopics, setSelectedTopics] = useState<Topic[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [topicOption] = useState<Topic[]>([]);
  const [searchRes, setSearchRes] = useState<Topic[]>([]);

  const handleCreateTopic = async (inputValue: string) => {
    setIsLoading(true);
    const newOption = {
      name: inputValue,
      color: chroma.random().hex(),
    };
    const res = await postAPI('/v1/topic', newOption);
    if (res.code === 200) {
      setSelectedTopics((prev) => [
        ...prev,
        {
          label: newOption.name.toUpperCase(),
          value: newOption.name.toUpperCase(),
          color: newOption.color,
        },
      ]);
    } else {
      dispatch(
        showNotification({
          status: 'error',
          msg: res.data,
        })
      );
    }
    setIsLoading(false);
  };

  const onSubmit = handleSubmit(async (data, event) => {
    event?.preventDefault();
    setIsLoading(true);
    const submitData = {
      ...data,
      description:
        data.description ||
        'Join my study room to boost your focus & get stuff done together!',
      topics: selectedTopics.map((i) => i.value),
    };
    const res = await postAPI('/v1/room', submitData);
    if (res.code === 200) {
      dispatch(setRoomInfo(res.data));
      router.replace(`/room/${res.data.code}`);
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    setIsLoading(false);
  });

  const promiseOptions = async (inputValue: string) => {
    const url = inputValue
      ? `/v1/topic?name=${inputValue.toLocaleLowerCase()}`
      : '/v1/topic/suggest';
    const res = await getAPI(url);
    if (res.code === 200) {
      const optionsList = res.data.map(({ name, color }: TopicResponse) => ({
        label: name,
        value: name,
        color,
      }));
      setSearchRes(optionsList);
      return optionsList;
    }
  };

  const roomTypeStyles: StylesConfig<{ value: string }, false> = {
    control: (baseStyles, state) => ({
      ...baseStyles,
      borderColor: state.isFocused
        ? 'var(--color-secondary)'
        : 'var(--color-border-40)',
      outline: 0,
      boxShadow: 'unset',
      background: 'var(--color-background)',
      '&:hover': {                                           
        ...baseStyles,
        borderColor:'var(--color-secondary) !important',
        background: 'var(--color-background)'
      },
    }),
    menu: (base) => ({
      ...base,
      border:
        theme === 'dark' ? '1px solid rgba(var(--decimal-text), 0.5)' : '',
      background: 'var(--color-background)',
    }),
    option: (style, props) => ({
      ...style,
      ':hover': {
        ...style[':hover'],
        backgroundColor: props.isSelected
          ? 'var(--color-secondary)'
          : 'rgba(var(--decimal-secondary),0.28)',
      },
    }),
    singleValue: (base) => ({
      ...base,
      color: 'var(--color-text-primary)',
    }),
  };

  const topicStyles: StylesConfig<Topic, true> = {
    control: (styles, state) => ({
      ...styles,
      borderColor: state.isFocused
        ? 'var(--color-secondary)'
        : 'var(--color-border-40)',
      outline: 0,
      boxShadow: 'unset',
      background: 'var(--color-background)',
      '&:hover': {                                           
        ...styles,
        borderColor:'var(--color-secondary) !important',
        background: 'var(--color-background)'
      },
    }),
    menu: (base) => ({
      ...base,
      background: 'var(--color-background)',
    }),
    option: (styles, { data, isDisabled, isFocused, isSelected }) => {
      const color = chroma(data.color || '#000');
      return {
        ...styles,
        backgroundColor: isDisabled
          ? undefined
          : isSelected
          ? data.color
          : isFocused
          ? color.alpha(0.1).css()
          : undefined,
        color: isDisabled
          ? '#ccc'
          : isSelected
          ? chroma.contrast(color, 'white') > 2
            ? 'white'
            : 'black'
          : data.color,
        cursor: isDisabled ? 'not-allowed' : 'default',

        ':active': {
          ...styles[':active'],
          backgroundColor: !isDisabled
            ? isSelected
              ? data.color
              : color.alpha(0.3).css()
            : undefined,
        },
      };
    },
    multiValue: (styles, { data }) => {
      const color = chroma(data.color);
      return {
        ...styles,
        backgroundColor: color.alpha(0.1).css(),
      };
    },
    multiValueLabel: (styles, { data }) => ({
      ...styles,
      color: data.color,
    }),
    multiValueRemove: (styles, { data }) => ({
      ...styles,
      color: data.color,
      ':hover': {
        backgroundColor: data.color,
        color: 'white',
      },
    }),
  };

  const validOption = (input: string, selectedValue: Options<Topic>) => {
    if (input.length && input.length > 30) return false;
    if (searchRes.length) return false;
    if (selectedValue.length >= 5) return false;
    return true;
  };

  return (
    <Form className="form" onSubmit={onSubmit} noValidate>
      <Form.Group className="mb-2">
        <Form.Label>Name*</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter room's name"
          {...register('name', {
            onChange: () => {
              clearErrors('name');
            },
          })}
          autoFocus
          isInvalid={!!errors.name}
          disabled={isLoading}
        />
        <Form.Control.Feedback type="invalid" className="error">
          {errors.name?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group className="mb-2">
        <Form.Label>Description (Optional)</Form.Label>
        <Form.Control
          className="no-resize"
          as="textarea"
          rows={2}
          placeholder="Enter description"
          {...register('description', {
            onChange: () => {
              clearErrors('description');
            },
          })}
          isInvalid={!!errors.description}
          disabled={isLoading}
        />
        <Form.Control.Feedback type="invalid" className="error">
          {errors.description?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <Row>
        <Col md={6}>
          <Form.Group className="mb-2">
            <Form.Label>Type*</Form.Label>
            <Controller
              control={control}
              defaultValue={ROOM.ROOM_TYPE[0].value}
              name="room_type"
              render={({ field: { onChange, value } }) => (
                <Select
                  isSearchable={false}
                  theme={(theme) => ({
                    ...theme,
                    colors: {
                      ...theme.colors,
                      primary: '#3bacb6',
                    },
                  })}
                  styles={roomTypeStyles}
                  options={ROOM.ROOM_TYPE}
                  value={ROOM.ROOM_TYPE.find((c) => c.value === value)}
                  onChange={(val) => onChange(val?.value)}
                  isDisabled={isLoading}
                  isLoading={isLoading}
                />
              )}
            />
          </Form.Group>
        </Col>
        <Col md={6}>
          <Form.Group className="mb-2">
            <Form.Label>Capacity*</Form.Label>
            <Form.Control
              min={2}
              defaultValue={10}
              type="number"
              {...register('capacity', {
                onChange: () => {
                  clearErrors('capacity');
                },
              })}
              disabled={isLoading}
            />
            <div className="error">{errors.capacity?.message}</div>
          </Form.Group>
        </Col>
      </Row>
      <Form.Group className="mb-2">
        <Form.Label>Password (optional)</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter password"
          {...register('password', {
            onChange: () => {
              clearErrors('password');
            },
          })}
          isInvalid={!!errors.password}
          disabled={isLoading}
        />
        <Form.Control.Feedback type="invalid" className="error">
          {errors.password?.message}
        </Form.Control.Feedback>
      </Form.Group>
      <Form.Group className="mb-2">
        <Form.Label>Select topics</Form.Label>
        <AsyncCreatableSelect
          className="select-topic"
          onChange={(selectedOptions) => {
            if (selectedOptions.length <= 5) {
              setSelectedTopics([...selectedOptions]);
            }
          }}
          theme={(theme) => ({
            ...theme,
            colors: {
              ...theme.colors,
              primary: '#3bacb6',
            },
          })}
          closeMenuOnSelect={false}
          isMulti
          isValidNewOption={validOption}
          styles={topicStyles}
          defaultOptions
          isDisabled={isLoading}
          isLoading={isLoading}
          onCreateOption={handleCreateTopic}
          options={topicOption}
          value={selectedTopics}
          loadOptions={promiseOptions}
        />
      </Form.Group>
      <div className="d-flex justify-content-end mt-4">
        <Button
          variant="secondary"
          type="submit"
          className="submit text-uppercase"
          disabled={isLoading}
        >
          <Spinner
            animation="border"
            size="sm"
            className={!isLoading ? 'd-none' : 'me-2'}
          />
          Submit
        </Button>
      </div>
    </Form>
  );
};

export default CreateRoom;
