import React, { useState } from 'react';
import { putAPI } from '@/common/utils/customAPI';
import { Row, Col, Button, Form, Spinner } from 'react-bootstrap';
import { useAppDispatch } from '@/hooks';
import { showNotification } from '@/store/slice/appSlice';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import updatePasswordSchema from '@/common/utils/yupSchema/updatePassowordForm';

interface UpdatePasswordForm {
  current_password: string;
  new_password: string;
}

const Security = () => {
  const {
    register,
    handleSubmit,
    clearErrors,
    reset,
    setFocus,
    formState: { errors },
  } = useForm<UpdatePasswordForm>({
    resolver: yupResolver(updatePasswordSchema),
    mode: 'onSubmit',
    reValidateMode: 'onSubmit',
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const onSubmit = handleSubmit(async (data, event) => {
    event?.preventDefault();
    setIsLoading(true);
    const res = await putAPI('/v1/account/update-password', { ...data });
    if (res.code === 200) {
      dispatch(
        showNotification({
          msg: 'Update password successfully!',
          status: 'info',
        })
      );
      reset();
    } else {
      dispatch(
        showNotification({
          msg: res.data as string,
          status: 'error',
        })
      );
    }
    setIsLoading(false);
  });

  return (
    <div id="setting-profile">
      <Row>
        <Col md={6} lg={8} className="mb-3">
          <div className="setting-card">
            <div className="header">Update password</div>
            <div className="body">
              <Form className="form" onSubmit={onSubmit}>
                <Row className="mb-1">
                  <Col md={12} lg={6}>
                    <Form.Label>Current password*</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Enter current password"
                      {...register('current_password', {
                        onChange: () => {
                          clearErrors('current_password');
                        },
                      })}
                      isInvalid={!!errors.current_password}
                      onKeyDown={(e) => {
                        if (e.key === 'Enter') {
                          e.preventDefault();
                          setFocus('new_password');
                        }
                      }}
                      disabled={isLoading}
                    />
                    <Form.Control.Feedback type="invalid" className="error">
                      {errors.current_password?.message}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={12} lg={6}>
                    <Form.Label>New password*</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Enter new password"
                      {...register('new_password', {
                        onChange: () => {
                          clearErrors('new_password');
                        },
                      })}
                      isInvalid={!!errors.new_password}
                      disabled={isLoading}
                    />
                    <Form.Control.Feedback type="invalid" className="error">
                      {errors.new_password?.message}
                    </Form.Control.Feedback>
                  </Col>
                </Row>
                <div className="d-flex align-items-center justify-content-end">
                  <Button
                    variant="secondary"
                    className="text-uppercase"
                    disabled={isLoading}
                    type="submit"
                  >
                    <Spinner
                      animation="border"
                      size="sm"
                      className={!isLoading ? 'd-none' : 'me-2'}
                    />
                    Change
                  </Button>
                </div>
              </Form>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Security;
