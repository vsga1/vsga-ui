import chroma from 'chroma-js';
import React from 'react';
import { Badge } from 'react-bootstrap';

type Props = { color: string; children: React.ReactNode; name: string; tooltipClass?: string; };

const MyBadge = ({ color, children, name, tooltipClass }: Props) => {
  return (
    <Badge
      style={{
        backgroundColor: color,
        color: chroma.contrast(color, 'white') > 1.5 ? 'white' : 'black',
        maxWidth: '7.5rem'
      }}
      bg="custom"
      className={`mb-1 text-truncate ${tooltipClass ? tooltipClass : ''}`}
      data-tooltip-content={name}
    >
      {children}
    </Badge>
  );
};

export default MyBadge;
