import React, { useContext, useState } from 'react';
import { Button, Form, FormControl } from 'react-bootstrap';
import { MdOutlineSend } from 'react-icons/md';
import Select from 'react-select';
import ChatArea from './ChatArea';
import { Tooltip } from 'react-tooltip';
import 'react-tooltip/dist/react-tooltip.css';
import { SocketContext } from '@/context/socket';
import { useAppSelector } from '@/hooks';
import { selectUser } from '@/store/slice/userSlice';
import { selectAllMessage } from '@/store/slice/messageSlice';
import { SEND_MESSAGE } from '@/common/utils/constants/event';
import UserSelectItem, { UserSelectItemProps } from './UserSelectItem';
import { selectUsers } from '@/store/slice/roomSlice';
import { selectAppTheme } from '@/store/slice/appSlice';

const everyoneOption: UserSelectItemProps = {
  id: 'everyone',
  value: '',
  username: 'Everyone',
  avatar_img_url: '',
};

const Chat = () => {
  const [messageText, setMessageText] = useState<string>('');
  const socket = useContext(SocketContext);
  const user = useAppSelector(selectUser);
  const theme = useAppSelector(selectAppTheme);
  const messageList = useAppSelector(selectAllMessage);
  const usersInRoomExceptCurrentUser: UserSelectItemProps[] | undefined =
    useAppSelector(selectUsers)
      ?.map((user) => ({
        ...user,
        value: user.username,
      }))
      .filter((userInRoom) => userInRoom.username !== user.data.username);
  const userOptions = usersInRoomExceptCurrentUser
    ? [everyoneOption, ...usersInRoomExceptCurrentUser]
    : [everyoneOption];
  const [userSelected, setUserSelected] = useState<string>(
    userOptions[0].value
  );

  const handleSetMessageText = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setMessageText(value);
  };

  const handleSendMessage = (event: React.SyntheticEvent) => {
    event.preventDefault();
    if (!messageText) return;
    socket.emit(SEND_MESSAGE, { text: messageText, receiver: userSelected });
    setMessageText('');
  };

  return (
    <>
      <div className="p-3 shadow-sm">
        <Select
          isSearchable={false}
          options={userOptions}
          defaultValue={userOptions[0]}
          formatOptionLabel={UserSelectItem}
          onChange={(selected) => setUserSelected(selected?.value || '')}
          theme={(theme) => ({
            ...theme,
            colors: {
              ...theme.colors,
              primary: '#3bacb6',
            },
          })}
          styles={{
            control: (base) => ({
              ...base,
              cursor: 'pointer',
              background: 'var(--color-background)',
              '&:hover': {                                           
                ...base,
                borderColor:'var(--color-secondary) !important',
                background: 'var(--color-background)'
              },
            }),
            menu: (base) => ({
              ...base,
              border:
                theme === 'dark'
                  ? '1px solid rgba(var(--decimal-text), 0.5)'
                  : '',
              background: 'var(--color-background)',
            }),
            option: (style, props) => ({
              ...style,
              ':hover': {
                ...style[':hover'],
                backgroundColor: props.isSelected
                  ? 'var(--color-secondary)'
                  : 'rgba(var(--decimal-secondary),0.28)',
              },
            }),
            dropdownIndicator: (base) => ({
              ...base,
              ':hover': {
                color: 'rgba(var(--decimal-text), 0.5)',
              },
            }),
            singleValue: (base) => ({
              ...base,
              color: 'var(--color-text-primary)',
            }),
          }}
        />
      </div>
      <ChatArea messageList={messageList} />
      <Form
        className="w-100 border-top p-3 d-flex align-items-center gap-2"
        onSubmit={handleSendMessage}
      >
        <FormControl
          size="sm"
          type="text"
          placeholder="Chat something..."
          value={messageText}
          onChange={handleSetMessageText}
        ></FormControl>
        <Button
          data-tooltip-id="btn-feature-send"
          data-tooltip-content="Send"
          data-tooltip-place="top"
          className="circle"
          type="submit"
          disabled={!messageText}
        >
          <MdOutlineSend />
        </Button>
        <Tooltip id="btn-feature-send" />
      </Form>
    </>
  );
};

export default Chat;
