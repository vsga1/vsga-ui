import { Message } from '@/store/slice/messageSlice';
import React from 'react';
import MessageList from '../Message/MessageList';

interface ChatAreaProps {
  messageList: Message[];
}

const ChatArea: React.FC<ChatAreaProps> = ({ messageList }) => {
  return (
    <div className="p-3 overflow-auto flex-grow-1">
      <MessageList messageList={messageList} />
    </div>
  )
}

export default ChatArea;
