import { DEFAULT_AVATAR_PATH } from '@/common/utils/constants/path';
import React from 'react';

export interface UserSelectItemProps {
  id: string
  value: string;
  username: string;
  avatar_img_url: string;
}

const UserSelectItem = ({ value, username, avatar_img_url }: UserSelectItemProps) => {
  return (
    <div>
      <div className="d-flex align-items-center">
        {
          value && (
            <div className="avatar me-2 rounded-circle overflow-hidden">
              <img
                src={avatar_img_url || DEFAULT_AVATAR_PATH}
                alt="avatar"
                width="28px"
                height="28px"
              />
            </div>
          )
        }
        {username}
      </div>
    </div>
  )
}

export default UserSelectItem;
