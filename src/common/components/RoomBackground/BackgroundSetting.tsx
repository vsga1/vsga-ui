import React, { useEffect, useState, KeyboardEvent } from 'react';
import { GoUnmute, GoMute } from 'react-icons/go';
import { Form } from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from '@/hooks';
import {
  selectBackground,
  setBackgroundSrc,
  setBackgroundVol,
} from '@/store/slice/appSlice';
import youtubeParser from '@/common/utils/youtubeParse';
import { RoomDefaultBackgrounds } from '@/common/utils/constants/room';

const BackgroundSetting = () => {
  const dispatch = useAppDispatch();
  const background = useAppSelector(selectBackground);
  const [ytbLink, setYtbLink] = useState('');
  const [vol, setVol] = useState(background.volume);
  const [isMute, setIsMute] = useState(vol > 0);

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      setIsMute(vol === 0);
      dispatch(setBackgroundVol(vol));
    }, 200);

    return () => clearTimeout(delayDebounceFn);
  }, [dispatch, vol]);

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      const vidId = youtubeParser(ytbLink);
      if (!vidId) return;
      dispatch(setBackgroundSrc(vidId));
    }, 200);

    return () => clearTimeout(delayDebounceFn);
  }, [dispatch, ytbLink]);

  const toggleMute = () => {
    dispatch(setBackgroundVol(isMute ? vol : 0));
    setIsMute((prev) => !prev);
  };

  const handleKeydown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      const vidId = youtubeParser(ytbLink);
      if (!vidId) return;
      dispatch(setBackgroundSrc(vidId));
    }
  };

  return (
    <div className="bg-setting">
      <div className="option-list">
        {RoomDefaultBackgrounds.map((item, index) => (
          <div
            key={index}
            className={`option ${item === background.src && 'active'}`}
            onClick={() => dispatch(setBackgroundSrc(item))}
          >
            <img src={`https://img.youtube.com/vi/${item}/0.jpg`} alt="" />
          </div>
        ))}
      </div>
      <div className="custom-link">
        <Form.Group className="my-3">
          <Form.Label>Youtube Video</Form.Label>
          <Form.Control
            type="text"
            placeholder="Paste your Youtube link here"
            value={ytbLink}
            onChange={(e) => setYtbLink(e.target.value)}
            onKeyDown={handleKeydown}
          />
        </Form.Group>
      </div>
      <div className="volume">
        <Form.Label>Original video sound</Form.Label>
        <div className="d-flex align-items-center">
          <div className="mute" onClick={toggleMute}>
            {!isMute ? <GoUnmute /> : <GoMute />}
          </div>
          <Form.Range
            value={vol}
            max={100}
            min={0}
            step={1}
            onChange={(e) => setVol(Number.parseInt(e.target.value))}
          />
        </div>
      </div>
    </div>
  );
};

export default BackgroundSetting;
