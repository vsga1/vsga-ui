import { useAppSelector } from '@/hooks';
import { selectBackground } from '@/store/slice/appSlice';
import React, { useEffect, useState } from 'react';
import Youtube, { YouTubePlayer, YouTubeProps } from 'react-youtube';
import { useWindowSize } from 'usehooks-ts';

const RoomBackground = () => {
  const background = useAppSelector(selectBackground);
  const [player, setPlayer] = useState<YouTubePlayer>();
  const { height, width } = useWindowSize();
  const scaleRatio =
    1 < width / height && width / height < 1.8 ? width / height : 1;
    
  const opts: YouTubeProps['opts'] = {
    playerVars: {
      autoplay: 1,
      controls: 0,
      loop: 1,
    },
  };

  useEffect(() => {
    if (!player) return;
    player.setVolume(background.volume);
    player.playVideo();
  }, [background, player]);

  useEffect(() => {
    if (!player) return;
    player.seekTo(0);
  }, [background.src, player]);

  return (
    <Youtube
      style={{ scale: scaleRatio.toString() }}
      videoId={background.src}
      className="room-background"
      opts={opts}
      onReady={(e) => {
        setPlayer(e.target);
        e.target.setVolume(background.volume);
      }}
      onEnd={(e) => e.target.playVideo()}
    />
  );
};

export default RoomBackground;
