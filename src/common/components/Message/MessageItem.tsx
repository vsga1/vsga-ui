import { ChatType } from '@/common/utils/enums';
import { useAppSelector } from '@/hooks';
import { selectAppTheme } from '@/store/slice/appSlice';
import { Message } from '@/store/slice/messageSlice';
import { selectUser } from '@/store/slice/userSlice';
import React from 'react';

interface MessageProps {
  message: Message;
}

const MessageItem: React.FC<MessageProps> = ({ message }) => {
  const theme = useAppSelector(selectAppTheme);
  const user = useAppSelector(selectUser);
  const isNotCurrentUser = (username: string) => username !== user.data.username;

  return (
    <div className="mt-3 d-flex flex-column">
      <div className="d-flex justify-content-between gap-3">
        <p className="fs-6 mb-2 text-secondary me-2">
          <span className="text-primary">
            {isNotCurrentUser(message.sender) ? message.sender : 'me'}
          </span>
          {' to '}
          <span className="text-primary">
            {isNotCurrentUser(message.receiver) ? message.receiver : 'me'}
          </span>
          {':'}
          {message.type === ChatType.DIRECTIVE && (
            <span className="text-danger">
              {' (private)'}
            </span>
          )}
        </p>
        <p className="fs-6 text-secondary">{message.sentAt}</p>
      </div>
      <p 
        className={`px-2 py-1 border rounded-1 text-break align-self-start ${message.type === ChatType.ALL ? 
          'bg-primary text-light border-primary' : 
          (theme === 'light' ? 'bg-light text-dark' : 'text-light border-primary')}`}
      >
        {message.text}
      </p>
    </div>
  )
}

export default MessageItem;
