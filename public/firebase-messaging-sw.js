/* eslint-disable no-undef */
importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js');
importScripts(
  'https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js'
);

// We will replace this variable to our app.
firebase.initializeApp({
  apiKey: 'AIzaSyAcA1HqPm89aK4RIRlXiJxf9x4RtkbZTwU',
  authDomain: 'vstudy-380113.firebaseapp.com',
  projectId: 'vstudy-380113',
  storageBucket: 'vstudy-380113.appspot.com',
  messagingSenderId: '270261685124',
  appId: '1:270261685124:web:39c4f6eb0237a19015abd1',
  measurementId: 'G-6NR4VW6RRJ'
});

let messaging = null;
if (firebase.messaging.isSupported()) {
  messaging = firebase.messaging();
};

const channel = new BroadcastChannel('notifications');
if (messaging) {
  messaging.onBackgroundMessage(function (payload) {
    channel.postMessage(payload);
  });
}
